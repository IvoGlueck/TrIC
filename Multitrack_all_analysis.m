%% Runs entire TrIC analysis on multiple tracks 
function Multitrack_all_analysis(hObject, eventdata, handles, varargin)
% hObject    handle to Multitrack_all_analysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Loads global variables

global usersvalues
global movies

%% START analysis loop

number_of_tracks = int2str(length(usersvalues.track_data));
for current_track_number = 1:length(usersvalues.track_data)
    current_track_data = usersvalues.track_data{current_track_number,1};
    
    switch nargin 
    
        case 3
        
            %Single_track_all_analysis_Callback(current_track_data, hObject, eventdata, handles, current_track_number, number_of_tracks)
            Single_track_all_analysis(current_track_data, hObject, eventdata, handles, current_track_number, number_of_tracks)
            
        case 5
            
            current_cell_number = varargin{1};
            
            number_of_cells = varargin{2};
            
            %Single_track_all_analysis_Callback(current_track_data, hObject, eventdata, handles, current_track_number, number_of_tracks, current_cell_number, number_of_cells)
            Single_track_all_analysis(current_track_data, hObject, eventdata, handles, current_track_number, number_of_tracks, current_cell_number, number_of_cells)
            
    end
    
end    

%% Exports full size movie with all tracks encircled 

if usersvalues.twoD == 1 
    
    full_RGB_movie = RGB_converter_stack (movies.TwoD_movie_tracked, 1, 512, 1, 512, 1, length (movies.TwoD_movie_tracked(1,1,:)));
    
end


if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1

    full_RGB_movie = RGB_converter_stack (movies.proj_movie_tracked, 1, 512, 1, 512, 1, length (movies.proj_movie_tracked(1,1,:)));

end

Encircle_all_tracks_incl_numbers(full_RGB_movie,usersvalues.track_data,usersvalues.result_location, 'Movie_all_tracks', 255, 0, 0);

%% Completion message + clearing of variables from workspace

switch nargin
    
    case 3
        
        disp('Multitrack analysis completed');
        clear;
    
    case 5
    
        current_cell_number_str = num2str(current_cell_number);
        
        number_of_cells_str = num2str(current_cell_number);
        
        disp(strcat({'Multitrack analysis of cell '}, current_cell_number_str, {'out of '}, number_of_cells_str, {' completed'}));

end