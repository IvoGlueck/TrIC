function [Max,Pos,Maxrand, indoutofZ] = CC3D_trans(non_proj_movie_tracked,non_proj_movie_untracked,xpix,ypix,Z,Nz,frame_init,frame_end,transf,PFS_status)
%New 3D cross-correlation colocalization" analysis

%Aurelie's comments:
%calculates the 3D cross-correlation between each 3D frame of two movies
%restricted to the area around the tracked particle
% Filename2 is the original movie (16bits) in the channel not used for
% TDtracking.
% Nz is the number of Z slices for each frame
% xpix,ypix result of 2D tracking in pixels, Z result of Ztracking in
% pixels
%Aurelie Dupont

%Ivo's comments:
%channel 1: tracked channel 
%channel 2: untracked channel 

%Inputs:
    %non_proj_movie_tracked: 3D array containing the tracked non-projected movie
    %non_proj_movie_untracked: 3D array containing the untracked non-projected movie
    %xpix: matrix with subpixel accurate x values of track
    %ypix: matrix with subpixel accurate y values of track
    %Z: matrix with substack accurate z values of track
    %Nz: number of planes of each stack
    %frame_init: index of the frame of the projected movie for which tracking was started
    %frame_end: index of the frame of the projected movie for which tracking
    %was ended
    %transf:structure containing info about the transformation matrix 
    %PFS_status: contains the info if measurements have been done with or without perfect focus system (at Andor SD)
    
%Outputs:
%1. Max is a vector cotaining the maximum value the cross-correlation between 
    %the stacks of channel 1 and channel 2 
    %Max contains one value for each stack 

%2. Pos is a vector containing 
    %A) the subpixel accurate x (Pos1, Pos(1)) and y (Pos2, Pos(2)) coordinates of the
    %maximum of the cross correlation between the tracked and the untracked
    %non-projected movie (done for each stack for the plane containg the
    %maximum => one Pos1 and Pos2 value for each stack)
    %B) the substack accurate Z position (Pos(3)) of the cross-correlation between the tracked and the untracked
    %non-projected movie (one value for each stack)  
    
%3. Maxrand is a vector cotaining the maximum value the cross-correlation between 
    %the stacks of channel 1 and the randomized channel 2 (CHECK WHATS EXACTLY RANDOMIZED) 
    %Maxrand contains one value for each stack 

%indoutofZ: contains the indices of the frames of the projected movie for which the maximum of the cross-correlation could not be fitted in Z 

%Aurelie's comments:
%close all;

%info = imfinfo(Filename1);
%"info" is a structure with 37 fields each containing certain info about the 
%file containing the tracked not projected movie

W = length(non_proj_movie_tracked(1,:,1));
%W = info.Width;
%saves the info in the field "Width" of the variable "info" in "W". Width of
%the image in pixels (for Aurelie's images with both channels on single camera =256)

H = length(non_proj_movie_tracked(:,1,1));
%H = info.Height;
%saves the info in the field "Height" of the variable "info" in "H". Height of
%the image in pixels (for Aurelie's images with both channels on single camera=512)

L = frame_end - frame_init +1; %L: number of frames of the projected movie for which tracking was
%performed; length of current track

Z=round(Z); %rounds subplane accurate Z values to the nearest integer

dpix = 10; %half size of the area of interest (xy), default 10
%dpixZ = 3;

S=2*dpix+1; %dimension of the area of interest in pixel

%movie1 = uint16(zeros(S,S,Nz));
%movie2 = uint16(zeros(S,S,Nz));
%uint16" converts into unsigned integers of type "uint16" 
%creates array of S-by-S-by-Nz dimensions; Nz matrices of S-by-S dimensions

xpix=round(xpix);ypix=round(ypix);
%rounds subpixel accurate x and y coordinates of the track to the nearest
%integer

Pos=cell(L,3); %initializes an array "Pos" with L rows and 3 columns

if PFS_status == 1
   Nz = Nz-1; %if measurements have been performed with the perfect focus system at the Andor SD the first plane of every stack has been removed during image readin
end

if length(transf.tdata) == 10 %if there are 10 coefficients saved for mapping the polynomial degree used for mapping was 3
    D=3;
elseif length(transf.tdata) == 6 %if there are 6 coefficients saved for mapping the polynomial degree used for mapping was 2
    D=2;
end

[x2pix,y2pix] = Tran1to2(xpix,ypix,transf,D);
%Inputs:
%xpix and ypix: coordinates with subpixel resolution of track
%transf: structure containing info about the transformation matrix
%D=length of tranf.tdata CHANGE AFTER UNDERSTANDING TRANSF

%Outputs:
%x2pix, y2pix: matrices containing coordinates of the track in the not 
%tracked movie for every frame 
%(CHECK AGAIN AFTER UNDERSTANDING TRANF)

x2pix=round(x2pix); 
y2pix=round(y2pix); 
%rounds the x and y coordinates in the not tracked movie to the nearest
%integer

outofZ=zeros(L,1); %preallocates a vector of length L

disp('Colocalization analysis ongoing');

%if waitbar should be activated activate the comments below
%canceled= 0;
%"canceled is set to "1" if analysis has been canceled
%NewTDCCwaitbar = waitbar(0, '1', 'Name', 'New 3D cross-correlation colocalization analysis in progress ...', 'CreateCancelBtn', 'setappdata(gcbf,''Cancel'',1)') 
%initializes a waitbar with "Cancel" button 

p = ProgressBar(L);
for i= 1:L % runs over the tracked frames of the projected movie
%parfor i= 1:L % runs over the tracked frames of the projected movie
        %(i/L)*100 %value is shown in the command window, iteration progress in percent 
    
        %PixRegx=[xpix(i)-dpix,xpix(i)+dpix];
        %creates an array of type double with 2 elements: 
        %1. x coordinate of frame i - dpix, 
        %2. x coordinate of frame i + dpix
        %specifies a region around the pixel indentified by manual tracking 
        %PixRegy=[ypix(i)-dpix,ypix(i)+dpix];
        %does the same for y 
        
        
    if (xpix(i)-dpix<=0) | (ypix(i)-dpix<=0) | (xpix(i)+dpix>W) | (ypix(i)+dpix>H) %tracked channel
        Pos1=-20;Pos2=-20;PosZsub=-1;Max(i)=0;Maxrand(i)=0; %POSZSUB AND MAXRAND NOT INCLUDED IN THE OLD VERSION OF CC ANALYSIS
        %if the particle is closer to the edge of the image than half the
        %fitting window in x and y the above stated  values are returned
        %WHY WERE THESE VALUES CHOSEN? CHECK!!
    elseif (x2pix(i)<=dpix) | (y2pix(i)<=dpix) | (x2pix(i)>W-dpix) | (y2pix(i)>H-dpix) %untracked channel
        Pos1=-20;Pos2=-20;PosZsub=-1;Max(i)=0;Maxrand(i)=0;  %POSZSUB AND MAXRAND NOT INCLUDED IN THE OLD VERSION OF CC ANALYSIS
        %if the particle is closer to the edge of the image than half the
        %fitting window in x and y the above stated  values are returned
        %WHY WERE THESE VALUES CHOSEN?
    else
        %Aurelie's stuff:
        %frameTemp = frame_init+i-2;
        %nypixTemp = ypix(i)-dpix;
        %pypixTemp = ypix(i)+dpix;
        %nxpixTemp = xpix(i)-dpix;
        %pxpixTemp = xpix(i)+dpix;
        movie1 = uint16(zeros(S,S,Nz));
        movie2 = uint16(zeros(S,S,Nz));
        for frame=1:Nz %iterates over the frames of each stack 
            
            %channel 1
            %Aurelie's comment: movie1(:,:,j)=imread(Filename1,'Index',(frameTemp).*Nz+j,'PixelRegion',{[nypixTemp,pypixTemp],[nxpixTemp,pxpixTemp]}); %DIFFERENCE TO OLD CC3D: reads in all frames of the unprojected movie 
            %movie1(:,:,frame) = non_proj_movie_tracked (PixRegy(1):PixRegy(2), PixRegx(1):PixRegx(2),(frame_init+i-2).*Nz+frame);
            movie1(:,:,frame) = non_proj_movie_tracked ((ypix(i)-dpix):(ypix(i)+dpix), (xpix(i)-dpix):(xpix(i)+dpix),(frame_init+i-2).*Nz+frame);
            %movie1(:,:,frame)=imread(Filename1,'Index',(frame_init+i-2).*Nz+j,'PixelRegion',{[ypix(i)-dpix,ypix(i)+dpix],[xpix(i)-dpix,xpix(i)+dpix]}); %DIFFERENCE TO OLD CC3D: reads in all frames of the unprojected movie 
           
            %saves the intensities of the pixels within the area of
            %interest around the rounded subpixel accurate x and y
            %coordinates of the track in 16-bit greyscale of each frame of
            %the non-projected movie 
            
            %Aurelie's comment: to export movie 1
%           File=strcat('movie1_',num2str(frame),'.tif');
%           imwrite(movie1(:,:,frame),File, 'tif','Compression','none');
            
            %channel 2
            movie2(:,:,frame) = non_proj_movie_untracked((y2pix(i)-dpix):(y2pix(i)+dpix), (x2pix(i)-dpix):(x2pix(i)+dpix),(frame_init+i-2).*Nz+frame);
            %movie2(:,:,frame) = non_proj_movie_untracked(PixReg2y(1):PixReg2y(2), PixReg2x(1):PixReg2x(2),(frame_init+i-2).*Nz+frame);
            %movie2(:,:,frame)=imread(Filename2,'Index',(frame_init+i-2).*Nz+j,'PixelRegion',{[y2pix(i)-dpix,y2pix(i)+dpix],[x2pix(i)-dpix,x2pix(i)+dpix]}); %DIFFERENCE TO OLD CC3D: reads in all frames of the unprojected movie 
            
            %Aurelie's comments: to export movie 2  
%           File=strcat('movie2_',num2str(frame),'.tif');
%           imwrite(movie2(:,:,frame),File, 'tif','Compression','none');
        end
         Si=size(movie2); %returns the size of each dimension of movie2, 
         %row vector with entries: number of pixels in x number of pixels in y, 
         %number of planes per z-stack
 
         %to randomize the second file
          movie2rand=movie2(randperm(Si(1)),randperm(Si(2)),randperm(Si(3))); 
          %intensity values of all pixels within the area of interest of one stack randomized  
          
          %Aurelie's comments: to export randomized movie2
          %for frame=1:Nz
          %    File=strcat('movie2rand_',num2str(frame),'.tif');
          %    imwrite(movie2rand(:,:,frame),File, 'tif','Compression','none');
          %end;
          
          CCCrand = CC3D(movie1,movie2rand)./(Si(1)*Si(2).*Si(3));
          %CCCrand: cross-correlation of channel 1 with randomized channel 2
          %array with the dimensions: 2dpix+1-by-2dpix+1-by-number of planes per
          %stack)
          %the results of the cross-correlation are divided by the total
          %number of pixels (by-the-way:
          %(Si(1)*Si(2).*Si(3))=(Si(1)*Si(2)*Si(3))) 
          
          %Aurelie's comments: to export results of 3D cross-correlation
 %          for frame=1:Nz
 %              File=strcat('CCCrand_',num2str(frame),'.tif');
 %              imwrite(CCCrand(:,:,frame),File, 'tif','Compression','none');
 %          end;
          Maxrand(i)=max(max(max(CCCrand)));
          %maximum value the cross-correlation between stack i of channel 1 and randomized stack i of channel 2 
          %Maxrand contains one value for each stack 
         
          CCC = CC3D(movie1,movie2)./(Si(1)*Si(2).*Si(3)); %%DIFFERENCE TO OLD CC3D ANALYSIS: entire stacks are correlated here; calculation itsself is completely identical 
          %CCC: cross-correlation of channel 1 with channel 2
          %array with the dimensions: 2dipix+1-by-2dpix+1-by-number of planes per
          %stack)
          %the results of the cross-correlation are divided by the total
          %number of pixels of one stack (by-the-way: (Si(1)*Si(2).*Si(3))=(Si(1)*Si(2)*Si(3))
 
          %Aurelie's comments: to export results of 3D cross-correlation         
 %         for frame=1:Nz
 %             File=strcat('CCC_',num2str(frame),'.tif');
 %             imwrite(CCC(:,:,frame),File, 'tif','Compression','none');
 %         end;
         
         %Ivo's comments: IN CONTRAST TO THE OLD CC3D the result of the
         %cross correlation is a stack, in the following the plane
         %containing the maximum is found and fitted with a Gaussian to find
         %the subpixel accurate x-/y-coordinates of the maximum of the
         %cross-correlation
          Max(i)=max(max(max(CCC)));
          %maximum value the cross-correlation between stack i of channel 1 and stack i of channel 2 
          %Max contains one value for each stack 
         
 
         %find the position of the CC maximum
         M=find(CCC(:)==max(max(max(CCC(:)))));
         %CCC(:) writes all entries of CCC in a vector containing all entries below each other
         %=> M is a single number giving the index of the maximum entry in
         %this vector
         %finds the indices of the maximum value in CCC (maximum values of
         %the cross correlation be
         
         ZM=floor(M/S^2)+1;
         %ZM=1;
         %ZM is the index of the plane in which the maximum of the
         %cross-correlation is found (WHY +1?)
         %S contains the number of pixels in the area of interest 
         %floor(X) rounds each element of X to the nearest integer less than
         %or equal to that element. 
         [RowM,ColM]=find(CCC(:,:,ZM)==max(max(CCC(:,:,ZM))));
         %finds the indices/x-y coordinates of the maximum value within the plane identified above 
         %RowM: in which row
         %ColM: in which column
         
         %Refine x and y
         [Pos1,Pos2]=Gaussian2Di(CCC(:,:,ZM));
         %fits the plane containing the maximum of the cross correlation
         %with a 2D gaussian and returns the subpixel position of the
         %maximum of the gaussian => subpixel accurate x-y coordinates of
         %the maximum of the cross correlation
         
         erPos=3; %error of position: allowed deviation of the subpixel accurate xy position from the pixel accurate xy position
         if abs(Pos1-ColM)>erPos 
             Pos1=ColM;
         end
         if abs(Pos2-RowM)>erPos
             Pos2=RowM;
             
         end
         %if the fitted xy coordinates differ more than 3 pixels from
             %the non-fitted xy coordinates the non fitted xy coordinates
             %are taken as the final position
         
 %Refine Z %CALCULATION OF THE SUBSTACK ACCURATE Z POSITION IS MISSING IN
 %THE OLD CC3D VERSION 
         dpix2=3;
         roi = zeros(min(RowM+dpix2,S)-max(1,RowM-dpix2), (min(ColM+dpix2,S)- max(1,ColM-dpix2))); %preallocates the variables used in the for-loop, needed for PARFOR 
         Int=[];
         
         for frame=1:Nz %runs over the planes of a stack         
             %measure intensity in a smaller area around the max
             roi=CCC(max(1,RowM-dpix2):min(RowM+dpix2,S),max(1,ColM-dpix2):min(ColM+dpix2,S),frame); 
             %max(1,RowM-dpix2): returns 1 if RowM is 4 or smaller, if not
             %it returns RowM-3
             %min(RowM+dpix2,S): returns S=5 if RowM is 2 or smaller, if not
             %it returns RowM+3 
             %e.g.: for S== 10 and first plane and RoM==6 and CoM==5 -> CCC(3:9,2:8,1)
             %roi contains the cross-correlation values of the pixels within
             %a smaller area (maximal 7x7) around the indices of the maximum value of the cross-correlation for each plane of a stack
             Int(frame) = mean(mean(roi));  
             %measures the mean intensity within this smaller area for each
             %plane of the stack
         end
         %Aurelie's comments:
         %plot(Int-min(Int),'.-');pause;
         
         Int0=Int-min(Int); 
         %from the avarage intensities the minimal avarage intensity is
         %substracted WHY? -> done to correct for intensity differences
         %between the channels 
         dpz=3; % default 2, fit over 2*dpz points, defines fit window in z for cross-correlation maximum
         PosZ=ZM;
         %PosZ is the Z position of the correlation maximum of a stack with
         %stack accuracy/before the fit
        i0=PosZ-dpz;
        ii=PosZ+dpz;
        %sets the window in z over which to fit
        %outofZ=zeros(L);
        if (PosZ+dpz <= Nz)&(PosZ-dpz>=1)
            %checks whether the planes corresponding to the indices i0 and ii
            %exist for the stacks taken
            PosZsub = fitwithgaussian([i0:ii],Int0(i0:ii),PosZ);
            %Inputs are: 
            %A vector containing the indices of the planes inside the window in z, 
            %the values stored in Int0 for these indices 
            %the Z position of the correlation maximum of a stack with
            %stack accuracy/before the fit

            %Outputs are:
            %substack accurate z position of the cross-correlation maximum is stored in the variable Zsub 
            %=> Zsub contains one value for each frame of the projected movie 
            %-> Z position of particle with substack accuracy
        
        elseif PosZ+dpz > Nz
            outofZ(i,1) = 1; %if Z window for fit exceeds stack size the Z position cannot be fitted; at the index of those planes the vector 'outofZ' is set to 1
            PosZsub = PosZ; %and the plane accurate Z position of the maximum is saved instead
            
        elseif PosZ-dpz<1
            outofZ(i,1) = 1; %if Z window for fit exceeds stack size the Z position cannot be fitted; at the index of those planes the vector 'outofZ' is set to 1
            PosZsub = PosZ; %and the plane accurate Z position of the maximum is saved instead
        end
        %if the window in Z exceeds the stack than the stack accurate
        %non-fitted z position is taken as the final
        Pos1=Pos1-dpix-1;Pos2=Pos2-dpix-1;
        %scaling back from the the area of interest in x and y to the indices of the entire image
        PosZsub=PosZsub-(Nz+1)/2;
        %scaling back from the the area of interest in z to the indices of
        %the entire stack 
    end
    
    Pos(i,:)={Pos1,Pos2,PosZsub};
    
    %Pos=[Pos;Pos1,Pos2,PosZsub];
    %Pos is a vector containing 
    %A) the subpixel accurate x (Pos1, Pos(1)) and y (Pos2, Pos(2)) coordinates of the
    %maximum of the cross correlation between the tracked and the untracked
    %non-projected movie (done for each stack for the plane containing the
    %maximum => one Pos1 and Pos2 value for each stack)
    %B) the substack accurate Z position (Pos(3)) of the cross-correlation between the tracked and the untracked
    %non-projected movie (one value for each stack)  
    
 %   in case waitbar window is used:  
 %   waitbar(i/L, NewTDCCwaitbar, [num2str((i/L)*100, 2) ' %']) %updates the waitbar
 %   if getappdata(NewTDCCwaitbar, 'Cancel') %if "Cancel" button has been pressed 
 %      fprintf ('Analysis has been canceled during new 3D cross-correlation colocalization analysis'); 
 %      canceled = 1; %"canceled is set to "1" if analysis has been canceled 
 %      delete(NewTDCCwaitbar)
 %      Pos=100000; %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
 %      Max=100000; %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
 %      Maxrand=100000000; %if Maxrand is 100000000 this means that analysis has been canceled during New 3D cross-correlation colocalization analysis; needed to stop "TRAC" function
 %      return %cancel the function here 
 %   end
 p.progress;
end
p.stop;

%if waitbar without parfor is used
%if (canceled == 0) %variable showing whether analysis has been canceled  
%   fprintf ('New 3D cross-correlation colocalization analysis finished');
%   delete(NewTDCCwaitbar)
%end

Max=Max';Maxrand=Maxrand'; %flips arrays 
Pos=cell2mat(Pos);

indoutofZ = find(outofZ==1); %indoutofZ: contains the indices of the frames of the projected movie for which the maximum of the cross-correlation could not be fitted in Z

% if frames for which Z position of cross-correlation maximum could not be
% fitted should be printed in the command window:
%loutofZ = length(indoutofZ);
%for h=1:loutofZ
%    statement=char(strcat(statement, ' ', num2str(indoutofZ(h)),' ', ','))
%end
%statement2=strcat('Z position of CCmax could not be fitted for frame:', statement);
%disp(statement2);
%=================================================================
%=================================================================
function [X2,Y2] = Tran1to2(X1,Y1,transf,D)

    
    %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    corrx = 0; %correction factor of x coordinate; insert when working on cropped movies (only ROI), set to 0 when working on uncropped movies 
    corry = 0; %correction factor of y coordinate; insert when working on cropped movies (only ROI), set to 0 when working on uncropped movies
    %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    
    
%transform track to fit in channel 2
L=length(X1);
for i=1:L
    if D==2
        A=[1,X1(i),Y1(i),X1(i).*Y1(i),X1(i).*X1(i),Y1(i).*Y1(i)];
    elseif D==3
        A=[1,(X1(i)+corrx),(Y1(i)+corry),(X1(i)+corrx).*(Y1(i)+corry),(X1(i)+corrx).^2,(Y1(i)+corry).^2,(Y1(i)+corry).*(X1(i)+corrx).^2,(X1(i)+corrx).*(Y1(i)+corry).^2,(X1(i)+corrx).^3,(Y1(i)+corry).^3];
        %A=[1,X1(i),Y1(i),X1(i).*Y1(i),X1(i).^2,Y1(i).^2,Y1(i).*X1(i).^2,X1(i).*Y1(i).^2,X1(i).^3,Y1(i).^3];
    end
    u=A*transf.tdata;
    X2(i)=(u(1)-corrx);Y2(i)=(u(2)-corry);
    %X2(i)=u(1);Y2(i)=u(2);
end
X2=X2';Y2=Y2';

%=================================================================
%=================================================================
function [CC] = CC3D(stack1,stack2)

% cross-correlation of two 3D stacks

%Inputs:
%stack1 and stack2 are the images of one stack of the non-projected movie
%an array with the dimensions: 2dipix+1-by-2dpix+1-by-number of planes per
%stack (only the grey scale values of the pixels with the area of interest)

%Outputs:
%CC are the results of the cross-correlation of the images of the stack 
%(array with the dimensions: 2dipix+1-by-2dpix+1-by-number of planes per
%stack)

stack1=double(stack1);
stack2=double(stack2);

%scaling (WHY IS THIS DONE??)
stack1=stack1-mean(mean(mean(stack1)));
stack2=stack2-mean(mean(mean(stack2)));
%mean intensity is substracted from all pxiel values 

%Aurelie's comments:
% frame1=frame1./max(max(frame1));
% frame2=frame2./max(max(frame2));


STDF1=std(reshape(stack1,1,[]));
STDF2=std(reshape(stack2,1,[]));
%reshape(stack1,1,[]) all entries of a frame are written into one
%column, 21-by-21 array into 1-by-441 vector
%the standard deviation of all grey scale values is taken 

stack1=stack1./STDF1;
stack2=stack2./STDF2;
%the grey scale values from which the mean value over all pixels was
%substracted is devided by the standard deviation of all pixel values 

IM1=fftn(stack1);
IM2=fftn(stack2);
%calculates the fast fourier transform of the greyscale values

CC=abs(real(fftshift(ifftn(IM1.*conj(IM2)))));
%conj(X): returns the complex conjugate of the elements of X
%ifft(X): calculates the inverse fourier transform of X
%fftshift: shifts the zero-frequency component to the center of the array
%real (X): returns the real part of the elements of the complex array X
%abs(X): returns the absolute value of the element in array X


%====================================================================
%====================================================================
function [ax,ay]=Gaussian2Di(frame)

%to fit a frame with a 2D gaussian
%returns [x,y] coordinates of the maximum

movie=frame-mean(mean(frame));
mm=max(max(movie(:,:)));
[mrow mcol]= find(movie(:,:)==mm,1);

%[n,m]=size(movie(:,:));%assumes that I is a nxm matrix
% ws=10; %Half-window size
%  I=double(movie(my-ws:my+ws,mx-ws:mx+ws));
I=double(movie);
I=I-mean(mean(I));
[n,m]=size(I);
[X,Y]=meshgrid(1:n,1:m);%your x-y coordinates
x(:,1)=X(:); % x= first column
x(:,2)=Y(:); % y= second column

f=I(:);          % your data f(x,y) (in column vector)


mI=max(max(I(:,:)));
[mrowf mcolf]= find(I(:,:)==mI,1);



fun = @(c,x) c(1)+c(2)*exp(-((x(:,1)-c(3))/c(4)).^2-((x(:,2)-c(5))/c(6)).^2);
c0=[0 mI mcolf 5 mrowf 5];%start-guess here

options=optimset('TolX',1e-3,'MaxIter',100,'MaxTime',.3,'Display','off');


%fit
[cc, a , b , c, d]=lsqcurvefit(fun,c0,x,f,[],[],options);
d;
% ax=cc(3)+my-(ws+2)
% ay=cc(5)+mx-(ws+2)
ax=cc(3);ay=cc(5);

% Ifit=fun(cc,x); %your fitted gaussian in vector
% Ifit=reshape(Ifit,[n m]);%gaussian reshaped as matrix
% cc=round(cc);
% 
% 
% %to plot the fit in a window of 40x40 pixels
% 
% figure
% imagesc([Ifit I])
% drawnow
% title(['Sigma: ' num2str(cc(4)) ' / ' num2str(cc(6))]);
% axis image
% axis off
% pause;
%=============================================================
%=============================================================
function [stackb]=zoom(stack,xpix,ypix,dpix)

%to zoom around a particle, half-size of the box:dpix
S=size(stack);
xpix=round(xpix);
ypix=round(ypix);
if (xpix-dpix<=0) | (ypix-dpix<=0) | (xpix+dpix>S(2)) | (ypix+dpix>S(1))
    stackb=0;
else
    stackb=stack(ypix-dpix:ypix+dpix,xpix-dpix:xpix+dpix);
end

%=======================================================================
% fit the intensity as a function of Z with a gaussian
function m = fitwithgaussian(X,Y,Z)
%Inputs are:
% 1. X: vector containing the indices of the planes within the window in Z.
% Meaning the indices of the planes to which fitting has to be performed 

%2. Y: data to which fit is performed

%3. Z: index of plane containing the maximum (before fit)

sx = size(X);
if sx(1) < sx(2)    %ISN'T this always the case??
    X = X';         %turns row vector into column vector
end

sy = size(Y);
if sy(1) < sy(2)
    Y = Y';         %turns row vector into column vector 
end

s = fitoptions('Method','NonlinearLeastSquares',... %fitting method DETAILS
    'Lower',[0,0,0],...         %lower bounds of coefficients to be fitted
    'Upper',[100,50,10],...     %upper bounds of coefficients to be fitted
    'Startpoint',[0.5,Z,1],...  %initial values for coefficients 
    'MaxIter',100);             %maximum number of interations allowed for the fit

f = fittype('A.*exp(-(x-m)^2./(2.*s^2))','options',s);
%defines the function with which to fit => 1D Gaussian
    %A: height/amplitude of the curve's peak
    %m: position of the center of the peak
    %s: standard deviation, controls the width of the peak
    %pairs: 'Name', Value, here: fit option saved in s
    %gives the fit options

%Aurelie's comments: 
%options = fitoptions;
%options.StartPoint = [1 3 5];

Fres = fit(X,Y,f); %DETAILS HERE!!
%creates the fit to the data in X and Y with the model specified by f
%(the fittype) 
%X: A vector containing the indices from Z(i)-dp until Z(i)+dp, 
%Y: the values stored in Int0 for these indices 
coeffvals = coeffvalues(Fres);
%retuns the coefficients of the "Fres" fit
A = coeffvals(1);   %A: height/amplitude of the curve's peak
m = coeffvals(2);   %m: position of the center of the peak
s = coeffvals(3);   %s: standard deviation, controls the width of the peak 
% saves the coefficients in A, m and s
% m is returned by the function
    
%     figure;plot(X,Y,'.')
%     hold on;
%     plot(Fres,'r')
%     pause;

