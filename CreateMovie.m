function movie = CreateMovie(RGB_stack, sub_x_coordinates, sub_y_coordinates, min_x_index, min_y_index, saving_destination, moviename, value_red, value_green, value_blue)
%Encircles particle in RGB movie and saves resulting movie as TIFF and AVI
%file

%Inputs:
%RGB_stack: contains RGB-converted movie in an array (H,W,3,L)
%sub_x_coordinates: subpixel accurate x-coordinates of track (relative to
%512x512 full frame)
%sub_y_coordinates: subpixel accurate y-coordinates of track (relative to
%512x512 full frame)
%min_x_index: lowest index in X used when exporting RGB stack 
%max_x_index: highest index in X used when exporting RGB stack 
%min_y_index: lowest index in Y used when exporting RGB stack 
%max_y_index: highest index in Y used when exporting RGB stack 
%saving_destination: path to folder in which to save the movie 
%moviename: name of file in which movie is saved 
%value_red: value red for circle colour 
%value_green: value green for circle colour 
%value_blue: value blue for circle colour 

%info = imfinfo(Filename);

L = length(RGB_stack(1,1,1,:));
%L=length(Traj(:,1));
%L = length(info);
%color=[1:3];
R =7;
W = length(RGB_stack(1,:,1,1));
%W = info.Width;

H = length(RGB_stack(:,1,1,1));
%H = info.Height;

movie = zeros(H,W,3,L);
%movie = uint8(zeros(H,W,3,L)); 

% for frame=1:L
%     % Read each frame into the appropriate frame in memory.
%     movie(:,:,:,frame) = imread(Filename,frame);
%     %imshow(movie(:,:,:,frame))
%     %axis('image');
% end

    %frame =Traj(:,1); 
    x = round((sub_x_coordinates(:)-min_x_index)+1);
    %x = round(Traj(:,2));
    y = round((sub_y_coordinates(:)-min_y_index)+1);
    %L=length(x);%length of this track
    dt = linspace(0,2*pi,45);
    %dt = 0:0.1/500:2*pi;
    x_circle = R.* cos(dt);
    y_circle = R.* sin(dt);

disp ('Insertion of circle ongoing'); %this statement is shown in the command window

p = ProgressBar(L);
    for k=1:L
        movie(:,:,:,k) = RGB_stack(:,:,:,k);
        %movie(:,:,:,k) = imread(Filename,frame(k));
        %viscircles(centersBright, radiiBright,'Color','b');
        %rectangle ('Position', [x(k)-10, y(k)-10, 20, 20])
        
        x_offs = x_circle + x(k); %THE ERROR IS HERE 
        y_offs = y_circle + y(k);
        
        %x_circle= round(x(k) + R.*cos(dt));
        %y_circle= round(y(k) + R.*sin(dt));
        for i=1:length(dt)
            
            % checks whether pixels of the circle exceed the dimensions of the input stack, if so these are not plotted
            if round(y_offs(i))>0 && round(y_offs(i))<=H && round(x_offs(i))>0 && round(x_offs(i))<=W 
            
                movie(round(y_offs(i)),round(x_offs(i)),1,k) = value_red;
                movie(round(y_offs(i)),round(x_offs(i)),2,k) = value_green;
                movie(round(y_offs(i)),round(x_offs(i)),3,k) = value_blue;
            
            end
            
        end
        
p.progress;   
    end
p.stop;
    
%mov = immovie(movie,map);

%saves the movie 
disp ('Saving of movie ongoing'); %this statement is shown in the command window

%as TIFF file
outputFileName = strcat(saving_destination, '\', moviename, '.tif'); %contains the full path to the file containing the movie

p = ProgressBar(L);
for n=1:length(movie(1, 1, 1, :)) %iterates over the stack containing the encircled particle
    imwrite(movie(:, :, :, n), outputFileName, 'tif', 'WriteMode', 'append',  'Compression','none'); %writes all corrected frames into a single tiff file and saves it in the specified folder
p.progress;    
end
p.stop;

%as AVI file
 mov = immovie(movie);
 results_location = strcat(saving_destination, '\', moviename, '.avi');
 uncompressedVideo = VideoWriter(results_location, 'Uncompressed AVI');
 uncompressedVideo.FrameRate = 15;  % Default 30
 open(uncompressedVideo);
 writeVideo(uncompressedVideo, mov);
 
%movie2avi(mov, [results_location, moviename], 'compression', 'None', 'fps', 23);
%implay(mov); %activate if you want to play movie in matlab
%=========================================================================
