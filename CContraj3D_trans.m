function [Max,Pos] = CContraj3D_trans(non_proj_movie_tracked,non_proj_movie_untracked,xpix,ypix,Z,Nz,frame_init,frame_end,transf,PFS_status)

    %%NOT YET UPDATED TO FULL FRAME
    
    
%Aurelie's comments:
%calculates the cross-correlation between each frame of two movies
%restricted to the area around the tracked particle
%
% From TDtracking fit: Z and stack1
% Filename is the original movie (16bits) in the channel not used for
% TDtracking.
% Nz is the number of Z slices for each frame
% xpix,ypix result of 2D tracking in pixels
%Aurelie Dupont

%Inputs:
%Filename1: path to the tracked non-projected movie
%Filename2: path to the untracked non-projected movie
%xpix and ypix: coordinates with subpixel resolution
%Z: z coordinates with substack resolution/results of fit (contains one
%value for each frame of the projected movie
%Nz: number of Z slices entered in the GUI
%frame_init: index of the frame of the projected movie for which tracking was started
%frame_end: index of the frame of the projected movie for which tracking
%was ended
%transf:structure containing info about the transformation matrix (CHANGE AFTER UNDERSTANDING TRANSF
%)

%Outputs:
% Max: array containing the value of the maximum of the cross-correlation between
% each frame of Filename1 and Filname2 (TAKING 3D info into
% account)
% Pos: array containing the subpixel accurate x and y coordinates of the maximum of the cross 
% correlation for each frame of the projected movie/for each stack   

info = imfinfo(Filename1);
%"info" is a structure with 37 fields each containing certain info about the 
%file containing the tracked projected movie
W = info.Width;
%saves the info in the field "Width" of the variable "info" in "W". Width of
%the image in pixels (for Aurelie's images with both channels on single camera =256)
H = info.Height;
%saves the info in the field "Height" of the variable "info" in "H". Height of
%the image in pixels (for Aurelie's images with both channels on single camera=512)
L = frame_end - frame_init +1;
%L=the number of frames of the projected movie
Z=round(Z);
%rounds the z coordinates with substack resolution to the nearest integer
dpix = 10; %half size of the area of interest, default 10
S=2*dpix+1; %size of the area of interest
movie1 = uint16(zeros(S,S)); 
movie2 = uint16(zeros(S,S));
%creates a matrix of zeros which has the dimensions of the area of interest 
%"uint16" converts into unsigned integers of type "uint16" 
xpix=round(xpix);ypix=round(ypix);
%rounds the subpixel x and y coordinates to the nearest integer
Pos=[];
%creates an array

if PFS_status == 1
   Nz = Nz-1; %if measurements have been performed with the perfect focus system at the Andor SD the first plane of every stack has been removed during image readin
end

if length(transf.tdata) == 10
    D=3;
elseif length(transf.tdata)==6
    D=2;
end
%returns the length of the largest dimension of the array
%"transf.tdata" CHECK AGAIN AFTER UNDERSTANDING TRANSF

[x2pix,y2pix] = Tran1to2(xpix,ypix,transf,D);
%Inputs:
%xpix and ypix: coordinates with subpixel resolution of track
%transf: structure containing info about the transformation matrix
%D=length of tranf.tdata CHANGE AFTER UNDERSTANDING TRANSF

%Outputs:
%x2pix, y2pix: matrices containing coordinates of the track in the not tracked projected movie for every frame 
%(CHECK AGAIN AFTER UNDERSTANDING TRANF)


'Colocalization analysis ongoing'

canceled= 0;
%"canceled is set to "1" if analysis has been canceled
TDCCwaitbar = waitbar(0, '1', 'Name', '3D cross-correlation colocalization analysis in progress ...', 'CreateCancelBtn', 'setappdata(gcbf,''Cancel'',1)')

for i= 1:L;
    %"L" contains the number of tracked frames; for-loop runs through all tracked frames  
    (i/L)*100
    %this value is shown in the command window for each iteration in the
    %for-loop; percentage of completion 
    if (xpix(i)<=dpix) | (ypix(i)<=dpix) | (xpix(i)>W-dpix) | (ypix(i)>H-dpix) %channel 1
        Pos1=-20;Pos2=-20;Max(i)=0;
        %if the particle is closer to the edge of the image than half the
        %fitting window in x and y the above stated  values are returned
        %WHY WERE THESE VALUES CHOSEN?
    elseif (x2pix(i)<=dpix) | (y2pix(i)<=dpix) | (x2pix(i)>W-dpix) | (y2pix(i)>H-dpix) %channel 2
        Pos1=-20;Pos2=-20;Max(i)=0;
        %if the particle is closer to the edge of the image than half the
        %fitting window in x and y the above stated  values are returned
        %WHY WERE THESE VALUES CHOSEN?
    else
        PixRegx=[xpix(i)-dpix,xpix(i)+dpix];    %ROI NOT SAVED IN A SEPERATE VARIABLE FOR CC3D_TRANS
        %creates an array of type double with 2 elements: 
        %1. x coordinate of frame (i) - dpix, 
        %2. x coordinate of frame (i) + dpix
        %specifies a region around the pixel received after rounding the subpixel accurate x/y coordinates received after 2D tracking (subpixel resolution) 
        PixRegy=[ypix(i)-dpix,ypix(i)+dpix];    %ROI NOT SAVED IN A SEPERATE VARIABLE FOR CC3D_TRANS    
        %does the same for y 
        
        PixRegx2=[round(x2pix(i))-dpix,round(x2pix(i))+dpix];   %ROI NOT SAVED IN A SEPERATE VARIABLE FOR CC3D_TRANS
        PixRegy2=[round(y2pix(i))-dpix,round(y2pix(i))+dpix];   %ROI NOT SAVED IN A SEPERATE VARIABLE FOR CC3D_TRANS
        %the same as done for channel 1 before
        %specifies a region around the pixel received after rounding the subpixel accurate x/y coordinates received after 2D tracking (subpixel resolution) 
        %rounds the values to the next integer
        
        
        movie1 = imread(Filename1,(frame_init+i-2).*Nz+Z(i),'PixelRegion',{PixRegy,PixRegx}); %tracked channel; DIFFERENCE TO NEW CC3D: reads in only the frames of the non-projected which are closest to the substack accurate Z position of the particle 
        movie2 = imread(Filename2,(frame_init+i-2).*Nz+Z(i),'PixelRegion',{PixRegy2,PixRegx2}); %untracked channel; DIFFERENCE TO NEW CC3D: reads in only the frames of the non-projected which are closest to the substack accurate Z position of the particle  
        %"Filename1" contains the path to the tracked non-projected movie
        %"Filename2" contains the path to the untracked non-projected movie
        %"frame_init+i-2" goes through the numbers of the frames of the
        %tracked projected movie NOT SURE ABOUT -2 HERE!!
        %"(frame_init+i-2).*Nz+Z(i)" is the image of the non-projected
        %movie the closest to the substack Z position of the tracked particle, i runs over the stacks, done for the stacks  
        
        %"reads in only a region of each frame of the non-projected movie; region is
        %specified by the boundaries set in "PixRegy /2" and "PixRegx /2" => 10x10 region around 
        %the pixel identified by manual tracking
        
        if (movie1==0 | movie2==0) %returns an array of zeros with the same 
            %dimensions as movie1 where positions at which movie1 is zero are set to 1
            Pos1=-20;Pos2=-20;Max(i)=0;
            % for those positions Pos1, Po2 and Max(i) are set as stated above 
        else
            CCC = CCi(movie1,movie2)./(length(movie1).^2); %DIFFERENCE TO NEW CC3D ANALYSIS: only single frames are correlated here; calculation itsself is completely identical
            %movie1 and movie2 contain the image of stack i the closest to 
            %the substack Z position of the particle in stack i in both
            %channels, respectively
            %the cross-correlation between these images is calculated 
            %the pixel values are divided by the total number of pixels of one frame 
            
            [Pos1,Pos2]=Gaussian2Di(CCC);
            %Pos1: x coordinate of maximum of Gaussian fit to cross correlation results
            %Pos2: y coordinate of maximum of Gaussian fit to cross correlation results
            %THESE POSITIONS ARE CONTROLLED IN THE NEW CC3D ANALYSIS
            
            Max(i)=max(max(CCC));
            %Max(i) contains the maximum value of the image cross-correlation for
            %each frame of the projected movies
            
            %Aurelie's comments:
            %     [Pos1m,Pos2m]=find(CCC==Max(i));
            %     if (Pos1>10 | Pos1<-10)
            %         Pos1=Pos1m;
            %     end
            %     if (Pos2>10 | Pos2<-10)
            %         Pos2=Pos2m;
            %     end
            
            Pos1=Pos1-dpix-1;Pos2=Pos2-dpix-1;
            %scaling back from the the area of interest in x and y to the indices of the entire image
            
            %IN THE NEW CC3D THE SUBSTACK ACCURATE Z POSITION IS CALCULATED
            %IN THE FOLLOWING
        end
    end
    Pos=[Pos;Pos1,Pos2];
    %for each iteration Pos1 and Pos2 are saved in Pos below each other
    %the vector contains the x and y coordinates (subpixel accuracy) of the maximum of the fit
    %for each frame
    
    waitbar(i/L, TDCCwaitbar, [num2str((i/L)*100, 2) ' %'])
    if getappdata(TDCCwaitbar, 'Cancel') %if "Cancel" button has been pressed 
       fprintf ('Analysis has been canceled during 3D cross-correlation colocalization analysis'); 
       canceled = 1; %"canceled is set to "1" if analysis has been canceled 
       delete(TDCCwaitbar)
       Max=100000; %if Max is 100000 this means that analysis has been canceled during 3D cross-correlation colocalization analysis; needed to stop "TRAC" function
       Pos=100000; %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
       return %function is left        
    end
end
    if (canceled == 0) %variable showing whether analysis has been canceled  
       fprintf ('3D cross-correlation colocalization analysis finished');
       delete(TDCCwaitbar)           
    end
Max=Max'; %switches a row vetcor into a column vector


%=================================================================
%=================================================================
function [X2,Y2] = Tran1to2(X1,Y1,transf,D)

%transform track to fit in channel 2
L=length(X1);
for i=1:L
    if D==2
        A=[1,X1(i),Y1(i),X1(i).*Y1(i),X1(i).*X1(i),Y1(i).*Y1(i)];
    elseif D==3
        A=[1,X1(i),Y1(i),X1(i).*Y1(i),X1(i).^2,Y1(i).^2,Y1(i).*X1(i).^2,X1(i).*Y1(i).^2,X1(i).^3,Y1(i).^3];
    end
    u=A*transf.tdata;
    X2(i)=u(1);Y2(i)=u(2);
end
X2=X2';Y2=Y2';

%=================================================================
%=================================================================
function [CC] = CCi(frame1,frame2) 

% cross-correlation of two images
%

frame1=double(frame1);
frame2=double(frame2);

%scaling
frame1=frame1-mean(mean(frame1));
frame2=frame2-mean(mean(frame2));
% frame1=frame1./max(max(frame1));
% frame2=frame2./max(max(frame2));
STDF1=std(reshape(frame1,1,[]));
STDF2=std(reshape(frame2,1,[]));
frame1=frame1./STDF1;
frame2=frame2./STDF2;


IM1=fft2(frame1);
IM2=fft2(frame2);

 CC=abs(real(fftshift(ifft2(IM1.*conj(IM2)))));

        
%===============================================================
%===============================================================
%%===============================================================%%
function [CCfull] = windowCCi(frame1,frame2)

% cross-correlation in a small window of size mxn
%

frame1=double(frame1);
frame2=double(frame2);

frame1=frame1-mean(mean(frame1));
frame2=frame2-mean(mean(frame2));
frame1=frame1./max(max(frame1));
frame2=frame2./max(max(frame2));

S=size(frame1);
m = 4;
n = 4;

mmax = S(1)./m;
nmax = S(2)./n;

CCfull=[];

for k = 1:mmax
    CCline=[];
    for j = 1:nmax
        IM1=fft2(frame1((k-1).*m+1:k.*m,(j-1).*n+1:j.*n));
        IM2=fft2(frame2((k-1).*m+1:k.*m,(j-1).*n+1:j.*n));
        
        CC=abs(real(fftshift(ifft2(IM1.*conj(IM2)))));
        
        CCline=[CCline CC];
    end
    CCfull = [CCfull;CCline];
end


%====================================================================
%====================================================================
function [ax,ay]=Gaussian2Di(frame)

%to fit a frame with a 2D gaussian
%returns [x,y] coordinates of the maximum 

movie=frame-mean(mean(frame));
mm=max(max(movie(:,:)));
[my mx]= find(movie(:,:)==mm,1);

%[n,m]=size(movie(:,:));%assumes that I is a nxm matrix
% ws=10; %Half-window size
%  I=double(movie(my-ws:my+ws,mx-ws:mx+ws));
I=double(movie);
I=I-mean(mean(I));
[n,m]=size(I);
[X,Y]=meshgrid(1:n,1:m);%your x-y coordinates
x(:,1)=X(:); % x= first column
x(:,2)=Y(:); % y= second column

f=I(:);          % your data f(x,y) (in column vector)

% imshow(I)
mI=max(max(I(:,:)));
[myf mxf]= find(I(:,:)==mI,1);



fun = @(c,x) c(1)+c(2)*exp(-((x(:,1)-c(3))/c(4)).^2-((x(:,2)-c(5))/c(6)).^2);
c0=[0 mI mxf 5 myf 5];%start-guess here

options=optimset('TolX',1e-3,'MaxIter',100,'MaxTime',.3,'Display','off');


%fit
[cc, a , b , c, d]=lsqcurvefit(fun,c0,x,f,[],[],options);
d;
% ax=cc(3)+my-(ws+2)
% ay=cc(5)+mx-(ws+2)
ax=cc(3);ay=cc(5);

% Ifit=fun(cc,x); %your fitted gaussian in vector
% Ifit=reshape(Ifit,[n m]);%gaussian reshaped as matrix
% cc=round(cc);


%to plot the fit in a window of 40x40 pixels

% figure
% imagesc([Ifit I])
% drawnow
% title(['Sigma: ' num2str(cc(4)) ' / ' num2str(cc(6))]);
% axis image
% axis off

%=============================================================
%=============================================================
     function [stackb]=zoom(stack,xpix,ypix,dpix)
         
         %to zoom around a particle, half-size of the box:dpix
         S=size(stack);
         xpix=round(xpix);
         ypix=round(ypix);
         if (xpix-dpix<=0) | (ypix-dpix<=0)| (xpix+dpix>S(2)) | (ypix+dpix>S(1))
             stackb=0;
         else
             stackb=stack(ypix-dpix:ypix+dpix,xpix-dpix:xpix+dpix);
         end
        
         
