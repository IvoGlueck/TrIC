function movie = Encircle_all_tracks(TwoDmovie,tracks,saving_destination, moviename, value_red, value_green, value_blue)

%Inputs:
%TwoDmovie: (Projected) movie (RGB; tiff stack) on to which the tracks have to be plotted

%tracks: cell array containing the tracks; each cell contains 1 track 
        %(matrix with three coloumns: 1: frame, 2: x coordinate, 3: y coordinate)
        %ATTENTION: if tracks are generate in Fiji the indexing of the frames
        %starts at 0; if generate in matlab the indexing starts at 1 (see below)
%saving destination: path to the folder where the resulting movie should be
                    %saved (excluding the name of the file containing the movie)

%moviename: name of the file containing the movie with the encircled tracks

%value_red: red RGB value of circle 

%value_green: green RGB value of circle

%value_blue: blue RGB value of circle


%Outputs:
%movie containing the encircled tracks in the tiff as well as avi format

%--------------------------------------------------------------------------

%Specifies the radius of the circles (adjust if needed) 
R = 7;

%Preallocates the variable in which the movie is loaded  
W = length(TwoDmovie(1,:,1));
%W = info.Width;
H = length(TwoDmovie(:,1,1));
%H = info.Height;
length_of_movie = length(TwoDmovie(1,1,1,:));
%info = imfinfo(Filename);
%length_of_movie=length(info); %number of frames of movie
%L = length(info);
movie = zeros(H,W,3,length_of_movie); 
%movie = unit8(zeros(H,W,3,length_of_movie)); 

number_of_tracks = length(tracks);
%number_of_tracks=numel(wavelet_tracks(1,:)); %saves the number of tracks 
%length_of_tracks=length(wavelet_tracks{:,1}); %takes the length of the first track for all; change later on


% for frame=1:L
%     % Read each frame into the appropriate frame in memory.
%     movie(:,:,:,frame) = imread(Filename,frame);
%     %imshow(movie(:,:,:,frame))
%     %axis('image');
% end

    %frame =Traj(:,1); 
    %x = round(Traj(:,2));
    %y = round(Traj(:,3));
    %L=length(x);%length of this track
    dt = linspace(0,2*pi,45);
    %dt = 0:0.1/500:2*pi;
    
    %contains the x and y coordinates of the circle, respectively
    x_circle = R.* cos(dt);
    y_circle = R.* sin(dt);
    
    %Iterates over the frames of the 2D movie
    for frame=1:length_of_movie        
      
        %Loads frame of 2D movie
        movie(:,:,:,frame) = TwoDmovie(:,:,:,frame);
        %movie(:,:,:,frame) = imread(Filename,frame);    
        %imshow(movie(:,:,:,frame))
        %imagesc(movie(:,:,:,frame))
        
        %Iterates over tracks 
        for track_number=1:number_of_tracks
            
            %Loads current track
            current_track = tracks{track_number,1};
            %coordinates=wavelet_tracks{1,track_number};
            
            %Checks whether the current track has a coordinate for the current frame
            if length(current_track)>=frame && current_track(frame,1) == frame-1 
            
                
                %Rounds the x and y coordinates of the current frame of the current frame
                x=round(current_track(frame,2));
                y=round(current_track(frame,3));
                
                %Calculates the coordinates of the circle around the current coordinate of the circle 
                x_offs = x_circle + x;
                y_offs = y_circle + y;
                %x_offs_r = round(x_offs);
                %y_offs_r = round(y_offs);
                
                %Checks whether the coordinate of the track and the pixel values of the sourounding circle are not outside the image (can among other reasons happen if coordinates are at edge and rounded)
                if x <= W && y <= H && x~=0 && y~=0 && all(x_offs <= W) && all(y_offs <= H) && all(x_offs >= 0) && all(y_offs >= 0) 
                    
                    %Sets the pixel with the coordinate to the specified color 
                    movie(y,x,1,frame)= value_red;
                    movie(y,x,2,frame)= value_green;
                    movie(y,x,3,frame)= value_blue;
                    
                    %position = [x+7 y+7]; %inserted
                    %value = track_number; %inserted
                    
                    %movie(:,:,:,frame) = insertText(movie(:,:,:,frame),position,value,'AnchorPoint','LeftBottom'); %inserted
                    
                    %figure
                    %imshow(movie),title('Numeric values');
                    
                    %Rounds the coordinates of the sourounding circle 
                    for i=1:length(dt)
                        y_offs_r = round(y_offs(i));
                        x_offs_r = round(x_offs(i));
                        
                        %Encircles the coordinate of the track 
                        if x_offs_r~=0 && y_offs_r~=0
                           movie(round(y_offs_r),round(x_offs_r),1,frame) = value_red;
                           movie(round(y_offs_r),round(x_offs_r),2,frame) = value_green;
                           movie(round(y_offs_r),round(x_offs_r),3,frame) = value_blue;
                        end
                        
                    end
                    
               end
                        
            end
            
        end
    
    end
    
%saves the movie 
disp ('Saving of movie ongoing'); %this statement is shown in the command window

%as TIFF file
outputFileName = strcat(saving_destination, '\', moviename, '.tif'); %contains the full path to the file containing the movie

p = ProgressBar(length(movie(1, 1, 1, :)));
for n=1:length(movie(1, 1, 1, :)) %iterates over the stack containing the encircled particle
    imwrite(movie(:, :, :, n), outputFileName, 'tif', 'WriteMode', 'append',  'Compression','none'); %writes all corrected frames into a single tiff file and saves it in the specified folder
p.progress;    
end
p.stop;

%as AVI file
 mov = immovie(movie);
 results_location = strcat(saving_destination, '\', moviename, '.avi');
 uncompressedVideo = VideoWriter(results_location, 'Uncompressed AVI');
 uncompressedVideo.FrameRate = 15;  % Default 30
 open(uncompressedVideo);
 writeVideo(uncompressedVideo, mov);
    
    
    
    
    
    
    
    
    
    %movie(:,:,:,k) = imread(Filename,frame(k));
        %viscircles(centersBright, radiiBright,'Color','b');
        %rectangle ('Position', [x(k)-10, y(k)-10, 20, 20])
        
        %x_offs = x_circle + x(k);
        %y_offs = y_circle + y(k);
        
        %x_circle= round(x(k) + R.*cos(dt));
        %y_circle= round(y(k) + R.*sin(dt));
        %for i=1:length(dt)
         %   movie(round(y_offs(i)),round(x_offs(i)),1,k) = value_red;
          %  movie(round(y_offs(i)),round(x_offs(i)),2,k) = value_green;
           % movie(round(y_offs(i)),round(x_offs(i)),3,k) = value_blue;
        %end
    %end
    
    
%mov = immovie(movie,map);


 %mov = immovie(movie);
%results_location = strcat(saving_destination, '\');
%movie2avi(mov, [results_location, moviename], 'compression', 'None', 'fps', 23);
%implay(mov); %activate if you want to play movie in matlab
%=========================================================================