function track_name_msg = Get_track_name_msg (varargin)

%% loads global variables

global Filenames
global usersvalues

% Gets root name
root_track_name = Filenames.Track.name(1:end-4); % Name of file in which the track(s) is/are saved 

root_cell_name = usersvalues.result_location; % Name of the folder in which results for cell are saved 

switch nargin 
    
    case 0 % Single track analysis
    
        track_name_msg = char(strcat({'Track '}, root_track_name, {' is analysed'}));        
        
    case 2 % Multitrack analysis  
        
        track_number = int2str(varargin{1});

        number_of_tracks = varargin{2};

        track_name_msg = char(strcat(root_track_name, {': '}, {'Track '}, track_number, {' out of '}, number_of_tracks, {' is analysed'}));
        
    case 4 % Multicell Multitrack analysis 
        
        track_number = int2str(varargin{1});

        number_of_tracks = varargin{2};

        cell_number = int2str(varargin{3});

        number_of_cells = int2str(varargin{4});

        track_name_msg = char(strcat({'Cell '}, root_track_name, {' (Cell '}, cell_number, {' out of '}, number_of_cells, {') is analysed. Track '}, track_number, {' out of '}, number_of_tracks, {' tracks is analysed'}));

end