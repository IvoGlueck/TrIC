function [tracks] = load_multitrack_file (multitrack_file_name, multitrack_file_path)
%function [pixel_tracks] = load_multitrack_file (multitrack_TrackMate_file)
% returns the tracks in pixel

multitrack_file_full_path=char(strcat(multitrack_file_path,multitrack_file_name));


[tracks] = importTrackMateTracks(multitrack_file_full_path, true);

%number_of_tracks = length (tracks);
%pixel_tracks=cell(number_of_tracks,1);

%for L=1:number_of_tracks
%    pixel_tracks (L,1) = {zeros(length (tracks{L,1}(:,1)),3)};
%    current_track = tracks{L,1};
%    pixel_tracks {L,1}(:,1) = current_track (:,1);
%    pixel_tracks {L,1}(:,2) = round(current_track (:,2)./141);
%    pixel_tracks {L,1}(:,3) = round(current_track (:,3)./141);
%end