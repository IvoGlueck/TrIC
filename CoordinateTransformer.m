function [XsubNotTracked, YsubNotTracked] = CoordinateTransformer (XsubTracked, YsubTracked, Transfpath)
    
    load(char(strcat(Transfpath.path, Transfpath.name)));
    
    L = length(XsubTracked);
    
    if length(transf12.tdata) == 10 %if there are 10 coefficients saved for mapping the polynomial degree used for mapping was 3
        D=3;
    elseif length(transf12.tdata) == 6 %if there are 6 coefficients saved for mapping the polynomial degree used for mapping was 2
        D=2;
    end
    
    % insert correction if not operating on full frame but a samller ROI 
    corrx = 0;
    corry = 0; 
   
    
    %preallocates the vectors
    XsubNotTracked = zeros(length(XsubTracked), 1);
    YsubNotTracked = zeros(length(YsubTracked), 1);
    
    
for i=1:L %iterates over all frames of the projected movie for which tracking was performed 
    if D==2
        A=[1,XsubTracked(i),YsubTracked(i),XsubTracked(i).*YsubTracked(i),XsubTracked(i).*XsubTracked(i),YsubTracked(i).*YsubTracked(i)];
        %A: vector with 6 entries; results of different operations on the
        %rounded subpixel x und y values
        %A(1): 1
        %A(2): rounded subpixel x value
        %A(3): rounded subpixel y value
        %A(4): product: rounded subpixel x value times rounded subpixel y value
        %A(5): rounded subpixel x value^2
        %A(6): rounded subpixel y value^2
    elseif D==3
        A=[1,XsubTracked(i)+corrx,YsubTracked(i)+corry,(XsubTracked(i)+corrx).*(YsubTracked(i)+corry),(XsubTracked(i)+corrx).^2,(YsubTracked(i)+corry).^2,(YsubTracked(i)+corry).*(XsubTracked(i)+corrx).^2,(XsubTracked(i)+corrx).*(YsubTracked(i)+corry).^2,(XsubTracked(i)+corrx).^3,(YsubTracked(i)+corry).^3];
        %A=[1,x1(i),y1(i),x1(i).*y1(i),x1(i).^2,y1(i).^2,y1(i).*x1(i).^2,x1(i).*y1(i).^2,x1(i).^3,y1(i).^3];
        %A: vector with 10 entries; results of different operations on the
        %rounded subpixel x und y values
        %A(1): 1
        %A(2): rounded subpixel x value
        %A(3): rounded subpixel y value
        %A(4): product: rounded subpixel x value times rounded subpixel y value
        %A(5): rounded subpixel x value^2
        %A(6): rounded subpixel y value^2
        %A(7): rounded subpixel y value times rounded subpixel x value^2
        %A(8): rounded subpixel x value times rounded subpixel x value^2
        %A(9): rounded subpixel x value^3
        %A(10): rounded subpixel y value^3
    end
    %Aurelie's comment:
    %u=A*transf.tdata;
    u=A*transf12.tdata; %A and tranf.tdata have the same dimensions and are multiplied
    XsubNotTracked(i)=(u(1)-corrx); YsubNotTracked(i)=(u(2)-corry); %these are the coordinates of the track in the not tracked movie (CHECK AGAIN AFTER UNDERSTANDING TRANF)
    %X2sub(i)=u(1);Y2sub(i)=u(2); 
end
XsubNotTracked=XsubNotTracked';YsubNotTracked=YsubNotTracked'; % flips the vectors

