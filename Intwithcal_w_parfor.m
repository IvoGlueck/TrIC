function [Int1_max,Int1_cor,Int2_max,Int2_cor] = Intwithcal(x1,y1,Filename1,Filename2,transf,frame_init)

%takes track done on Filename1: x1,y1
%takes the output of the calibration: transf
%Measure intensity in channel 1 by fitting with 2D gaussian 
%and substracting background
%Transform track with calibration information
%Measure intensity in channel 2
%Please give polynomial degree used for calibration:D
%Aurelie Dupont

%close all;

%Inputs:
%x1: matrix with subpixel accurate x values of track
%y1: matrix with subpixel accurate y values of track
%Filename1: path to the tracked projected movie
%Filename2: path to the untracked projected movie
%transf: structure containing info about the transformation matrix 
%frame_init: index of the frame of the projected movie for which tracking was started

%Outputs:
%Int1_max: vector containing the intensity value of the pixel with the maximum intensity for each frame of
%the projected movie of channel 1
%Int1_cor: vector containing the background corrected intensity
%of the tracked particle for each frame of channel 1
%Int2_max: vector containing the intensity value of the pixel with the maximum intensity for each frame of
%the projected movie of channel 2
%Int2_cor: vector containing the background corrected intensity
%of the tracked particle for each frame of channel 2


L=length(x1);
%L: number of steps for which particle was tracked 
dpix=4; % half size of the window 

if length(transf.tdata) == 10
    D=3;
elseif length(transf.tdata)==6
    D=2;
end
%returns the length of the largest dimension of the array
    %"transf.tdata"


%%%%%%%%PART 1%%%%%%%%%%%%%%
%measure intensity in first channel
info = imfinfo(Filename1);
W = info.Width;
%saves the info in the field "Width" of the variable "info" in "W". Width of
%the image in pixels (e.g. =512) 
H = info.Height;
%saves the info in the field "Height" of the variable "info" in "H". Height of
%the image in pixels (e.g. =256)

%stack = uint16(zeros(2*dpix+1,2*dpix+1));
%creates a matrix of zeros which has the dimensions of the area of interest 
%"uint16" converts into unsigned integers of type "uint16" 
x1r=round(x1);y1r=round(y1);
%rounds the subpixel x and y coordinates to the nearest integer


'Measuring intensity in channel 1'

Int1_cor = double(zeros(L,L));
%creates an array
%canceled= 0;
%"canceled is set to "1" if analysis has been canceled
%IntOwaitbar = waitbar(0, '1', 'Name', 'Measuring intensity in channel 1 ...', 'CreateCancelBtn', 'setappdata(gcbf,''Cancel'',1)') 
%initializes a waitbar with "Cancel" button 
PigRegx = double(zeros(1,2));
PigRegx = double(zeros(1,2));
Int1_max = double(zeros(1,L));
Int1_sumcor = double(zeros(1,L));
circle = uint16(zeros(2, dpix));
Peak = uint16(zeros(2, dpix));
BackG = uint16(zeros(2, dpix));
Size_peak = double(zeros(1,1));
Size_bkg = double(zeros(1,1));
Int1_peak = double(zeros(1,1));
Int1_sumbkg = double(zeros(1,L));
parfor i= 1:L
    %i runs over all frames of the projected movie
    PixRegx=[x1r(i)-dpix,x1r(i)+dpix];
    %creates an array of type double with 2 elements: 
    %1. rounded subpixel x coordinate of frame i - dpix, 
    %2. rounded subpixel x coordinate of frame i + dpix
        
    PixRegy=[y1r(i)-dpix,y1r(i)+dpix];
    %does the same for y   
    %specifies a region around the pixel indentified by manual tracking
    
    
    stack = uint16(zeros(2*dpix+1,2*dpix+1));
    %creates a matrix of zeros which has the dimensions of the area of interest 
    %"uint16" converts into unsigned integers of type "uint16" 

    if (x1r(i)<=dpix) | (y1r(i)<=dpix) | (x1r(i)>W-dpix) | (y1r(i)>H-dpix)
    stack(:,:)=0;
    %pipe "|" means "OR"
    %if the particle is closer to the edge of the image than half the
    %fitting window in x and y no fitting can be performed and the 9x9
    %matrix is filled with zeros for these positions
    else
        stack(:,:) = imread(Filename1,frame_init+i-1,'PixelRegion',{PixRegy,PixRegx});
        %saves the intensities of the pixels within the area of
        %interest around the rounded subpixel accurate x and y
        %coordinates of the track in 16-bit greyscale of frame i of
        %the tracked projected movie 
   
    %Aurelie's comments:    
    % stack(:,:) = imread(Filename1,frame_init+i-1);
    %stackb=double(zoom(stack,x1r(i),y1r(i),dpix));
        %imagesc(stackb);title('channel 1');pause;
    end
    Int1_max(i)=max(max(double(stack)));
    %converts the intensity values in "stack" into double precision values
    %and stores the maximum intensity in "Int1_max(i) => Int1_max is a
    %vector containing the intensity value of the pixel with the maximum intensity for each frame of
    %the projected movie of channel 1
    
    %Measure and substrack background
    dzoom=2;
    circle=circle_d(dzoom,dpix);%r:radius dpix:half-size of the box; 0 in the peak
    Peak=(1-circle).*stack;
    %1-circle: matrix of size circle filled with ones from which circle is
    %substracted => area of circle is filled with ones surrounded by zeros
    %Peak: only intensity values within a circle of 2 pixels around the
    %subpixel accurate x and y coordinates are kept
    BackG=circle.*stack;
    %only intensity values of pixels outside the circle are kept 
    Size_peak=sum(sum(1-circle));
    %sums up the columns and then those values (the large the area of circle within the box the
    %larger this value => contains the number of pixels forming the circle
    Size_bkg=sum(sum(circle));
    %sums up the columns and then those values (the large the area of the background with the box the
    %larger this value => contains the number of pixels forming the
    %background
    Int1_peak(i)=sum(sum(Peak))./Size_peak;
    %Average intensity of pixels within the circle 
    Int1_sumbkg = double(zeros(1,L));
    Int1_sumbkg(i)=sum(sum(BackG))./Size_bkg;
    %Average intensity of pixels forming the background
    
% Aurelie's comment:        
%     %stackb=zoom(stack,x1(i),y1(i),dpix);
%     %imagesc(stack);pause;
%     stackz=zoom(stack,dpix+1,dpix+1,dzoom);
%     %figure;imagesc(stackz);pause;
%     Size_peak=(2.*dzoom+1).^2;
%     Size_bkg=(2.*dpix+1).^2-Size_peak;
%     Int1_peak(i)=sum(sum(stackz))./Size_peak;
%     Int1_sumbkg(i)=(sum(sum(stack))-sum(sum(stackz)))./Size_bkg;
    
    Int1_sumcor = double(zeros(1,L));
    Int1_sumcor(i)=Int1_peak(i)-Int1_sumbkg(i);
    %substracts the average intensity of the background from the average
    %intensity within the circle (for frame i)
    Int1sumcor=Int1sumcor.';
    Int1_sumbkg=Int1_sumbkg.';
    
    
    %Int1_cor = double(zeros(L,2));
    Int1_cor(i)={Int1_sumcor(i),Int1_sumbkg(i)};
        
      
    %Int1_cor = cell2mat(Int1_cor);
    %Int1_cor=[Int1_cor;Int1_sumcor(i),Int1_sumbkg(i)];
    %"Int1_cor" is a vector containing the background corrected intensity
    %of the tracked particle for each frame of the tracked projected movie 
    i;
    
   % waitbar(i/L, IntOwaitbar, [num2str((i/L)*100, 2) ' %']) %updates the waitbar
   % if getappdata(IntOwaitbar, 'Cancel') %if "Cancel" button has been pressed 
   %    fprintf ('Analysis has been canceled during the measurement of the intensity in channel 1'); 
   %    canceled = 1; %"canceled is set to "1" if analysis has been canceled 
   %    delete(IntOwaitbar)
       
   %    Int1_max = 100000;  %if Int1_max is 100000 this means that the analysis has been canceled during the measurement of the intensity in channel 1; needed to stop "TRAC" function
   %    Int1_cor = 10000;   %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
   %    Int2_max = 10000;   %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
   %    Int2_cor = 10000;   %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
       
   %    return %cancel the function here 
   % end
    
    %pause(0.1)
end
%delete(IntOwaitbar);

%%%%%%%%PART 2%%%%%%%%%%%%%%
%transform track to fit in channel 2 %as done for channel 1
'Transforming track2'
 %A=double(zeros(1,10));
 u=double(zeros(1,2));
 X2_sub=double(zeros(L,1));
 Y2_sub=double(zeros(L,1));
parfor i=1:L %number of iterations equals the number of frames of the projected movie
    if D==2
        A=double(zeros(1,10));
        A=[1,x1(i),y1(i),x1(i).*y1(i),x1(i).*x1(i),y1(i).*y1(i)];
        %A: vector with 6 entries; results of different operations on the
        %rounded subpixel x und y values
        %A(1): 1
        %A(2): rounded subpixel x value
        %A(3): rounded subpixel y value
        %A(4): product: rounded subpixel x value times rounded subpixel y value
        %A(5): rounded subpixel x value^2
        %A(6): rounded subpixel y value^2
    elseif D==3
        A=[1,x1(i),y1(i),x1(i).*y1(i),x1(i).^2,y1(i).^2,y1(i).*x1(i).^2,x1(i).*y1(i).^2,x1(i).^3,y1(i).^3];
        %A: vector with 10 entries; results of different operations on the
        %rounded subpixel x und y values
        %A(1): 1
        %A(2): rounded subpixel x value
        %A(3): rounded subpixel y value
        %A(4): product: rounded subpixel x value times rounded subpixel y value
        %A(5): rounded subpixel x value^2
        %A(6): rounded subpixel y value^2
        %A(7): rounded subpixel y value times rounded subpixel x value^2
        %A(8): rounded subpixel x value times rounded subpixel x value^2
        %A(9): rounded subpixel x value^3
        %A(10): rounded subpixel y value^3
    end
    %Aurelie's comment:
    %u=A*transf.tdata;
    u=A*transf.tdata; %A and tranf.tdata have the same dimensions and are multiplied
    X2sub(i)=u(1);Y2sub(i)=u(2); %these are the coordinates of the track in the not tracked movie (CHECK AGAIN AFTER UNDERSTANDING TRANF)
end
X2sub=X2sub';Y2sub=Y2sub'; % flips the vectors

%measure maximum intensity in channel 2 %the same as for channel 1
info = imfinfo(Filename2);
W = info.Width;
H = info.Height;

stack = uint16(zeros(2*dpix+1,2*dpix+1));
x2r=round(X2sub);y2r=round(Y2sub);


'Measuring intensity in channel 2'

Int2_max=double(zeros(1,2));
Int2_cor=double(zeros(L,L));
Int2_sumcor = double(zeros(1,L));
%canceled= 0;
%"canceled is set to "1" if analysis has been canceled
%IntTwaitbar = waitbar(0, '1', 'Name', 'Measuring intensity in channel 2 ...', 'CreateCancelBtn', 'setappdata(gcbf,''Cancel'',1)')
%initializes a waitbar with "Cancel" button 
A=double(zeros(1,10));
parfor i= 1:L
    PixRegx=[x2r(i)-dpix,x2r(i)+dpix];
    PixRegy=[y2r(i)-dpix,y2r(i)+dpix];
    stack = uint16(zeros(2*dpix+1,2*dpix+1));
    %creates a matrix of zeros which has the dimensions of the area of interest 
    %"uint16" converts into unsigned integers of type "uint16" 
    if (x2r(i)<=dpix) | (y2r(i)<=dpix)| (x2r(i)>W-dpix) | (y2r(i)>H-dpix)
    stack(:,:)=0;
    else
        stack(:,:) = imread(Filename2,frame_init+i-1,'PixelRegion',{PixRegy,PixRegx});

    end
%     stack(:,:) = imread(Filename2,frame_init+i-1);
%     stackb=double(zoom(stack,x2r(i),y2r(i),dpix));
      % figure;imagesc(stackb);title('channel 2');   

    Int2_max(i)=max(max(double(stack)));
    
    %Measure and substrack background
    dzoom=2;
    circle=circle_d(dzoom,dpix);%r:radius dpix:half-size of the box; 0 in the peak
    Peak=(1-circle).*stack;
    BackG=circle.*stack;
    Size_peak=sum(sum(1-circle));
    Size_bkg=sum(sum(circle));
    Int2_peak(i)=sum(sum(Peak))./Size_peak;
    Int2_sumbkg(i)=sum(sum(BackG))./Size_bkg;

%     stackz=zoom(stack,dpix+1,dpix+1,dzoom);
%     Size_peak=(2.*dzoom+1).^2;
%     Size_bkg=(2.*dpix+1).^2-Size_peak;
%     Int2_peak(i)=sum(sum(stackz))./Size_peak;
%     Int2_sumbkg(i)=(sum(sum(stack))-sum(sum(stackz)))./Size_bkg;

    Int2_sumcor = double(zeros(1,L));
    Int2_sumcor(i)=Int2_peak(i)-Int2_sumbkg(i);
    Int2sumcor=Int2sumcor.';
    Int2_sumbkg=Int2_sumbkg.';
    %Int2_cor(i)={Int2_sumcor(i),Int2_sumbkg(i)};    
   
    %Int2_cor = double(zeros(L,2));
    Int2_cor(i)={Int2_sumcor(i),Int2_sumbkg(i)};
    %Int2_cor(i,1)=Int2_sumcor(i);
    %Int2_cor(i,2)=Int2_sumbkg(i);
    %Int2_cor = cell2mat(Int2_cor);
   
    
%     waitbar(i/L, IntTwaitbar, [num2str((i/L)*100, 2) ' %']) %updates the waitbar
%     if getappdata(IntTwaitbar, 'Cancel') %if "Cancel" button has been pressed 
%        fprintf ('Analysis has been canceled during the measurement of the intensity in channel 2'); 
%        canceled = 1; %"canceled is set to "1" if analysis has been canceled 
%        delete(IntTwaitbar)
%        
%        Int2_max = 100000;  %if Int2_max is 100000 this means that the analysis has been canceled during the measurement of the intensity in channel 2; needed to stop "TRAC" function
%        Int1_cor = 10000;   %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
%        Int1_max = 10000;   %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
%        Int2_cor = 10000;   %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
%        
%        return %cancel the function here 
%     end
    %pause(0.1)
end
delete (gcp) %shuts down parallel pool gcp = get current parallel pool  
%  delete(IntTwaitbar)
'Finished'
end
%=============================================================
%=============================================================
     function [stackb]=zoom(stack,xpix,ypix,dpix)
         
         %to zoom around a particle, half-size of the box:dpix
         S=size(stack);
         xpix=round(xpix);
         ypix=round(ypix);
         if (xpix-dpix<=0) | (ypix-dpix<=0)| (xpix+dpix>S(2)) | (ypix+dpix>S(1))
             stackb=0;
         else
             stackb=stack(ypix-dpix:ypix+dpix,xpix-dpix:xpix+dpix);
         end
     end    
%==============================================================
function circle=circle_d(r,dpix)
% Aurelie's comment: Generates circular Mask with radius r and box size l
%I think she meant: box size 2dipix+1

circle=zeros(2*dpix+1);

for i = -dpix:dpix
    for j= -dpix:dpix
        if round(sqrt((i*i)+(j*j)))>r
        circle(i+dpix+1,j+dpix+1)=1;
        end
    end
end
circle=uint16(circle);
%generates a matrix of size 2dpix+1-by-2dpix+1 in which all entries are 1
%except for those within a circle with radius r where they are 0
end         