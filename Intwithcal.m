function [Int1_max,Int1_cor,Int2_max,Int2_cor] = Intwithcal(x1,y1,proj_movie_tracked,proj_movie_untracked,transf,frame_init,XY_correction_status)
%CHANNEL 1: tracked channel 
%CHANNEL 2: untracked channel
    
    
%takes track done on proj_movie_tracked: x1,y1
%takes the output of the calibration: transf
%Measure intensity in channel 1 by fitting with 2D gaussian 
%and substracting background
%Transform track with calibration information
%Measure intensity in channel 2
%Please give polynomial degree used for calibration:D
%Aurelie Dupont

%close all;

%Inputs:
%x1: matrix with subpixel accurate x values of track
%y1: matrix with subpixel accurate y values of track
%proj_movie_tracked: 3D array containing the tracked projected movie
%proj_movie_untracked: 3D array containing the untracked projected movie
%transf: structure containing info about the transformation matrix 
%frame_init: index of the frame of the projected movie for which tracking was started
%XY_correction_status: boolean giving info if analysis was run with or
%without correction for shift in XY

%Outputs:
%Int1_max: vector containing the intensity value of the pixel with the maximum intensity for each frame of
%the projected movie of channel 1
%Int1_cor: vector containing the background corrected intensity
%of the tracked particle for each frame of channel 1
%Int2_max: vector containing the intensity value of the pixel with the maximum intensity for each frame of
%the projected movie of channel 2
%Int2_cor: vector containing the background corrected intensity
%of the tracked particle for each frame of channel 2



%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
corrx = 0; %correction factor of x coordinate; insert when working on cropped movies; set to 0 when working on uncropped movies
corry = 0; %correction factor of y coordinate; insert when working on cropped movies; set to 0 when working on uncropped movies
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

L=length(x1); %L: number of frames for which particle was tracked 
dpix=4; % half size of the ROI around the coordinate identified by manual tracking 

%Preallocation if movie should be exported
%outputChannel1 = 'Channel1.tif'; %contains the name of the outputfile 
%Channel1path=char(strcat(result_location, '\',outputChannel1));
%movie1 = uint16(zeros(2*dpix+1,2*dpix+1,L)); %preallocates the stack  
%outputChannel2 = 'Channel2.tif'; %contains the name of the outputfile
%Channel2path=char(strcat(result_location, '\',outputChannel2));
%movie2 = uint16(zeros(2*dpix+1,2*dpix+1,L)); %preallocates the stack  

% if analysis has been done incl. correction for XY shift 
if XY_correction_status == 1

    if length(transf.tdata) == 10 %if there are 10 coefficients saved for mapping the polynomial degree used for mapping was 3
        D=3;
    elseif length(transf.tdata) == 6 %if there are 6 coefficients saved for mapping the polynomial degree used for mapping was 2
        D=2;
    end

end

%%%%%%%%PART 1%%%%%%%%%%%%%%
%measure intensity in first, tracked channel
%info = imfinfo(Filename1);

W = length(proj_movie_tracked(1,:,1));
%W = info.Width;
%saves the info in the field "Width" of the variable "info" in "W". Width of
%the image in pixels (e.g. =512) 

H = length(proj_movie_tracked(:,1,1));
%H = info.Height;
%saves the info in the field "Height" of the variable "info" in "H". Height of
%the image in pixels (e.g. =256)

stack = uint16(zeros(2*dpix+1,2*dpix+1));
%creates a matrix of zeros which has the dimensions of the area of interest/ROI for fitting the virus 
%"uint16" converts into unsigned integers of type "uint16" 

x1r=round(x1);y1r=round(y1);
%rounds the subpixel x and y coordinates to the nearest integer

%to save subpixel accurate XY coordinates in the tracked channel
%NameSubX1results='Subpixel X coordinates in the tracked channel.txt';
%SubX1path=char(strcat(result_location, '\',NameSubX1results));
%Saves the path to the subpixel X coordinates in channel 1
%save(SubX1path,'x1','-ascii');
%NameSubY1results='Subpixel Y coordinates in the tracked channel.txt';
%SubY1path=char(strcat(result_location, '\',NameSubY1results));
%Saves the path to the subpixel Y coordinates in channel 1
%save(SubY1path,'y1','-ascii');

disp('');
disp('Measuring intensity in tracked channel ongoing');

Int1_cor=[]; %creates an array in which the background-corrected intensity for the tracked particle will be saved; one entry for each tracked frame of the projected movie 

%waitbar
canceled=0;
%"canceled is set to "1" if analysis has been canceled
IntOwaitbar = waitbar(0, '1', 'Name', 'Measuring intensity in tracked channel ...', 'CreateCancelBtn', 'setappdata(gcbf,''Cancel'',1)'); 
%initializes a waitbar with "Cancel" button 

for i= 1:L
    %i runs over all frames of the projected movie for which tracking was
    %performed
    PixRegx=[x1r(i)-dpix,x1r(i)+dpix];
    %creates an array of type double with 2 elements: 
    %1. (rounded subpixel x coordinate of frame i) - dpix, 
    %2. (rounded subpixel x coordinate of frame i) + dpix
    %gives the boundaries of the ROI around the rounded subpixel accurate x
    %and y coordinates
        
    PixRegy=[y1r(i)-dpix,y1r(i)+dpix];
    %does the same for y   
         
    if (x1r(i)<=dpix) | (y1r(i)<=dpix) | (x1r(i)>W-dpix) | (y1r(i)>H-dpix)
        
        stack(:,:)=0;
        %pipe "|" means "OR"
        %if the particle is closer to the edge of the image than half the
        %the size of the fitting window/ROI in x and y no fitting can be performed and the 9x9
        %matrix is filled with zeros for these positions
        
    else
        
        stack(:,:) = proj_movie_tracked (PixRegy(1):PixRegy(2), PixRegx(1):PixRegx(2), frame_init+i-1); 
        %stack(:,:) = imread(Filename1,frame_init+i-1,'PixelRegion',{PixRegy,PixRegx});
        %saves the intensities of the pixels within the area of
        %interest around the rounded subpixel accurate x and y
        %coordinates of the track in 16-bit greyscale of frame i of
        %the tracked projected movie 
   
        %Aurelie's comments:    
        %stack(:,:) = imread(Filename1,frame_init+i-1);
        %stackb=double(zoom(stack,x1r(i),y1r(i),dpix));
        %imagesc(stackb);title('channel 1');pause;
        
    end
    
    Int1_max(i)=max(max(double(stack)));
    %saves the intensity of the brightest pixel within the ROI in Int1_max
    
    %converts the intensity values in "stack" into double precision values
    %and stores the maximum intensity in "Int1_max(i) => Int1_max is a
    %vector containing the intensity value of the pixel with the maximum intensity for each frame of
    %the projected movie of channel 1
    
    %Measure and substrack background
    dzoom=2;
    circle=circle_d(dzoom,dpix); 
    %generates a matrix of size 2dpix+1-by-2dpix+1 in which all entries are 1
    %except for those within a circle with radius dzoom around the central pixel of
    %the matrix; within this circle entries are 0

    Peak=(1-circle).*stack;
    %1-circle:
    %generates a matrix of size 2dpix+1-by-2dpix+1 in which all entries are
    %0 except for those within a circle with radius dzoom around the central pixel of
    %the matrix; within this circle entries are 1
    
    %Peak: only intensity values within a circle of 2 pixels around the
    %rounded subpixel accurate x and y coordinates are kept
    
    BackG=circle.*stack;
    %only intensity values of pixels outside the circle are kept 
    
    Size_peak=sum(sum(1-circle));
    %sum(1-circle): sums up the number of pixels creating the circle for each column of the box (is a vector)  
    %sum(sum(1-circle)): sums up the resulting vector => Size_peak contains
    %the number of pixels forming the peak/circle
   
    Size_bkg=sum(sum(circle));
    %sum(circle): sums up the number of pixels outside the circle within the box; for each column of the box (is a vector)  
    %sum(sum(circle)): sums up the resulting vector => Size_bkg contains
    %the number of pixels forming the background within the box
    
    Int1_peak(i)=sum(sum(Peak))./Size_peak;
    %Average intensity of pixels within the circle 
    Int1_sumbkg(i)=sum(sum(BackG))./Size_bkg;
    %Average intensity of pixels forming the background
    
    % Aurelie's comment:        
    %     %stackb=zoom(stack,x1(i),y1(i),dpix);
    %     %imagesc(stack);pause;
    %     stackz=zoom(stack,dpix+1,dpix+1,dzoom);
    %     %figure;imagesc(stackz);pause;
    %     Size_peak=(2.*dzoom+1).^2;
    %     Size_bkg=(2.*dpix+1).^2-Size_peak;
    %     Int1_peak(i)=sum(sum(stackz))./Size_peak;
    %     Int1_sumbkg(i)=(sum(sum(stack))-sum(sum(stackz)))./Size_bkg;
    
    Int1_sumcor(i)=Int1_peak(i)-Int1_sumbkg(i);
    %substracts the average intensity of the background from the average
    %intensity within the circle (for frame i)
    Int1_cor=[Int1_cor;Int1_sumcor(i),Int1_sumbkg(i)];
    %"Int1_cor" is a vector containing the background corrected intensity
    %of the tracked particle for each frame of the tracked projected movie
    %and the corresponding avarage intensity of the background 
    %Int1_cor is returned by the function and saved in
    %"Curdata.intGcorrected" and "Curdata.intGbackground", respectively
    i;   
    
    %For movie export
    %movie1 (:,:,i) = stack(:,:);
    
    %for waitbar
    waitbar(i/L, IntOwaitbar, [num2str((i/L)*100, 2) ' %']) %updates the waitbar
    if getappdata(IntOwaitbar, 'Cancel') %if "Cancel" button has been pressed 
       fprintf ('Analysis has been canceled during the measurement of the intensity in the tracked channel'); 
       canceled = 1; %"canceled is set to "1" if analysis has been canceled 
       delete(IntOwaitbar)
       
       Int1_max = 100000;  %if Int1_max is 100000 this means that the analysis has been canceled during the measurement of the intensity in channel 1; needed to stop "TRAC" function
       Int1_cor = 10000;   %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
       Int2_max = 10000;   %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
       Int2_cor = 10000;   %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
       
       return %cancel the function here 
    end
    
    %pause(0.1)
end
delete(IntOwaitbar);

%For movie export
%for K=1:length(movie1(1, 1, :)) %iterates over the stack containing the corrected frames
    %imwrite(movie1(:, :, K), Channel1path, 'tiff', 'WriteMode', 'append',  'Compression','none'); %writes all corrected frames into a single tiff file and saves it in the current folder 
    %imwrite(movie1(:, :, K), outputChannel1, 'tiff', 'WriteMode', 'append',  'Compression','none'); %writes all corrected frames into a single tiff file and saves it in the current folder 
%end
%save(Channel1path, 'movie1');
%Ivo's fun end

% if user chose to correct XY shift the coordinates in the untracked
% channel are calculated
if XY_correction_status == 1

    %%%%%%%%PART 2%%%%%%%%%%%%%%
    %transform track to fit in channel 2 %as done for channel 1
    %disp('Transforming track2');

    for i=1:L %iterates over all frames of the projected movie for which tracking was performed 
        if D==2
            A=[1,x1(i),y1(i),x1(i).*y1(i),x1(i).*x1(i),y1(i).*y1(i)];
            %A: vector with 6 entries; results of different operations on the
            %rounded subpixel x und y values
            %A(1): 1
            %A(2): rounded subpixel x value
            %A(3): rounded subpixel y value
            %A(4): product: rounded subpixel x value times rounded subpixel y value
            %A(5): rounded subpixel x value^2
            %A(6): rounded subpixel y value^2
        elseif D==3
            A=[1,x1(i)+corrx,y1(i)+corry,(x1(i)+corrx).*(y1(i)+corry),(x1(i)+corrx).^2,(y1(i)+corry).^2,(y1(i)+corry).*(x1(i)+corrx).^2,(x1(i)+corrx).*(y1(i)+corry).^2,(x1(i)+corrx).^3,(y1(i)+corry).^3];
            %A=[1,x1(i),y1(i),x1(i).*y1(i),x1(i).^2,y1(i).^2,y1(i).*x1(i).^2,x1(i).*y1(i).^2,x1(i).^3,y1(i).^3];
            %A: vector with 10 entries; results of different operations on the
            %rounded subpixel x und y values
            %A(1): 1
            %A(2): rounded subpixel x value
            %A(3): rounded subpixel y value
            %A(4): product: rounded subpixel x value times rounded subpixel y value
            %A(5): rounded subpixel x value^2
            %A(6): rounded subpixel y value^2
            %A(7): rounded subpixel y value times rounded subpixel x value^2
            %A(8): rounded subpixel x value times rounded subpixel x value^2
            %A(9): rounded subpixel x value^3
            %A(10): rounded subpixel y value^3
        end
        %Aurelie's comment:
        %u=A*transf.tdata;
        u=A*transf.tdata; %A and tranf.tdata have the same dimensions and are multiplied
        X2sub(i)=(u(1)-corrx);Y2sub(i)=(u(2)-corry); %these are the coordinates of the track in the not tracked movie (CHECK AGAIN)
        %X2sub(i)=u(1);Y2sub(i)=u(2); 
    end
    X2sub=X2sub';Y2sub=Y2sub'; % flips the vectors

end

%For movie export in the untracked channel
%NameSubX2results='Subpixel X coordinates untracked channel.txt';
%SubX2path=char(strcat(result_location, '\',NameSubX2results));
%Saves the path to the subpixel X coordinates in channel 2
%save(SubX2path,'X2sub','-ascii');
%NameSubY2results='Subpixel Y coordinates untracked channel.txt';
%SubY2path=char(strcat(result_location, '\',NameSubY2results));
%Saves the path to the subpixel Y coordinates in channel 2
%save(SubY2path,'Y2sub','-ascii');

%for saving the coordinates of the track in the untracked channel; obtained
%by transforming the coordinates obtained in the tracked channel 
%save('Subpixel X coordinates ch2.mat','X2sub')
%save('Subpixel Y coordinates ch2.mat','Y2sub')
%save('Subpixel coordinates ch2.txt','X2sub','Y2sub','-ascii')
%Ivo's fun

%measure maximum intensity in channel 2 %the same as for channel 1
%info = imfinfo(Filename2);

W = length(proj_movie_untracked(1,:,1)); %this should not be needed as both have to have the same length
%W = info.Width;
H = length(proj_movie_tracked(:,1,1)); %this should not be needed as both have to have the same length
%H = info.Height;

stack = uint16(zeros(2*dpix+1,2*dpix+1));

% if user chose to correct XY shift the coordinates in the untracked
% channel are rounded
if XY_correction_status == 1

    x2r=round(X2sub);y2r=round(Y2sub);

end

disp('');
disp('Measuring intensity in untracked channel ongoing');

Int2_cor=[];

canceled= 0;
%"canceled is set to "1" if analysis has been canceled
IntTwaitbar = waitbar(0, '1', 'Name', 'Measuring intensity in the untracked channel ...', 'CreateCancelBtn', 'setappdata(gcbf,''Cancel'',1)');
%initializes a waitbar with "Cancel" button 
for i= 1:L
    
    % if user chose to correct XY shift he boundaries of the ROI around the shift-corrected rounded subpixel accurate x
    %and y coordinates in the untracked channel are calculated 
    if XY_correction_status == 1    
       
       PixRegx=[x2r(i)-dpix,x2r(i)+dpix];
       PixRegy=[y2r(i)-dpix,y2r(i)+dpix];
            
        if (x2r(i)<=dpix) | (y2r(i)<=dpix) | (x2r(i)>W-dpix) | (y2r(i)>H-dpix)
            
            stack(:,:)=0;

        else

            stack(:,:) = proj_movie_untracked (PixRegy(1):PixRegy(2), PixRegx(1):PixRegx(2), frame_init+i-1);
            %stack(:,:) = imread(Filename2,frame_init+i-1,'PixelRegion',{PixRegy,PixRegx});

        end
        
    else % if the user doesn't want to correct for XY shift          
           
        PixRegx=[x1r(i)-dpix,x1r(i)+dpix];
        PixRegy=[y1r(i)-dpix,y1r(i)+dpix];
        
        if (x1r(i)<=dpix) | (y1r(i)<=dpix) | (x1r(i)>W-dpix) | (y1r(i)>H-dpix)
            
            stack(:,:)=0;

        else

            stack(:,:) = proj_movie_untracked (PixRegy(1):PixRegy(2), PixRegx(1):PixRegx(2), frame_init+i-1);
            %stack(:,:) = imread(Filename2,frame_init+i-1,'PixelRegion',{PixRegy,PixRegx});

        end
        
    end

    Int2_max(i)=max(max(double(stack))); %saves the intensity of the brightest pixel within the ROI in Int2_max
    
    %Measure and substrack background
    dzoom=2;
    circle=circle_d(dzoom,dpix);%r:radius dpix:half-size of the box; 0 in the peak
    Peak=(1-circle).*stack;
    BackG=circle.*stack;
    Size_peak=sum(sum(1-circle));
    Size_bkg=sum(sum(circle));
    Int2_peak(i)=sum(sum(Peak))./Size_peak;
    Int2_sumbkg(i)=sum(sum(BackG))./Size_bkg;

%     Aurelie's comments:
%     stackz=zoom(stack,dpix+1,dpix+1,dzoom);
%     Size_peak=(2.*dzoom+1).^2;
%     Size_bkg=(2.*dpix+1).^2-Size_peak;
%     Int2_peak(i)=sum(sum(stackz))./Size_peak;
%     Int2_sumbkg(i)=(sum(sum(stack))-sum(sum(stackz)))./Size_bkg;

    Int2_sumcor(i)=Int2_peak(i)-Int2_sumbkg(i);
    Int2_cor=[Int2_cor;Int2_sumcor(i),Int2_sumbkg(i)];
   
    
    %For movie export in the untracked channel 
    %writes movie for channel 2
    %movie2 (:,:,i) = stack(:,:);  
    
    %waitbar
    waitbar(i/L, IntTwaitbar, [num2str((i/L)*100, 2) ' %']) %updates the waitbar
    if getappdata(IntTwaitbar, 'Cancel') %if "Cancel" button has been pressed 
       fprintf ('Analysis has been canceled during the measurement of the intensity in the untracked channel'); 
       canceled = 1; %"canceled is set to "1" if analysis has been canceled 
       delete(IntTwaitbar)
       
       Int2_max = 100000;  %if Int2_max is 100000 this means that the analysis has been canceled during the measurement of the intensity in channel 2; needed to stop "TRAC" function
       Int1_cor = 10000;   %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
       Int1_max = 10000;   %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
       Int2_cor = 10000;   %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
       
       return %cancel the function here 
    end
    %pause(0.1)
end
delete(IntTwaitbar)

%for movie export in the untracked channel:
%for K=1:length(movie2(1, 1, :)) %iterates over the stack containing the corrected frames
 %   imwrite(movie2(:, :, K), outputChannel2, 'tiff', 'WriteMode', 'append',  'Compression','none'); %writes all corrected frames into a single tiff file and saves it in the current folder 
    %imwrite(movie2(:, :, K), Channel2path, 'tiff', 'WriteMode', 'append',  'Compression','none'); %writes all corrected frames into a single tiff file and saves it in the current folder 
%end
%save(Channel2path, 'movie2');
%Ivo's fun end

%disp('Finished')

%=============================================================
%=============================================================
     function [stackb]=zoom(stack,xpix,ypix,dpix)
         
         %to zoom around a particle, half-size of the box:dpix
         S=size(stack);
         xpix=round(xpix);
         ypix=round(ypix);
         if (xpix-dpix<=0) | (ypix-dpix<=0)| (xpix+dpix>S(2)) | (ypix+dpix>S(1))
             stackb=0;
         else
             stackb=stack(ypix-dpix:ypix+dpix,xpix-dpix:xpix+dpix);
         end
        
%==============================================================
function circle=circle_d(r,dpix)
% Aurelie's comment: Generates circular Mask with radius r and box size l
%I think she meant: box size 2dpix+1

circle=zeros(2*dpix+1);

for i = -dpix:dpix
    for j= -dpix:dpix
        if round(sqrt((i*i)+(j*j)))>r
        circle(i+dpix+1,j+dpix+1)=1;
        end
    end
end
circle=uint16(circle);
%generates a matrix of size 2dpix+1-by-2dpix+1 in which all entries are 1
%except for those within a circle with radius r around the central pixel of
%the matrix; within this circle entries are 0
