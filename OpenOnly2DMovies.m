function [Trackedmovie, Untrackedmovie] = OpenOnly2DMovies
%Saves the paths to all movies needed for the "Only 2D" analysis 
%You only need to select the path to the folder containing the movies you
%want to analyse
%Please save all movies in one folder and include " tracked projected" and 
%" untracked projected" in the respective file names   

%Inputs: no inputs

%Outputs: 
%Trackedmovie: Path to the tracked projected movie
%Untrackedmovie: Path to the untracked projected movie

%Ivo M. Gl�ck

MovieFolder = uigetdir('X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Experiments\Results experiments','Select the folder containing the movies to analyse');
%MovieFolder contains the path to the folder selected by the user
allFiles = dir(MovieFolder);
%allFiles is a structure containing info (as fields) about all files in the folder 
allNames = { allFiles.name };
%saves the names of all files in the folder in a cell array

% 1. Save path to tracked projected movie 
SearchTrackedProjected = strfind(allNames,' tracked projected');
%cell array with one entry for all entries in "allNames" searches all entries 
%in "all names" for the string specified and stores the position at which the 
%string starts as a number; if the string is empty it saves an empty "[]"  
Index = find(~cellfun(@isempty, SearchTrackedProjected));
%finds the index of the non-empty entry in "SearchTrackedProjected" which corresponds to the index of the file name in "allNames"  
NameTrackedProjected = allNames(Index);
%Saves the name of the file containing the tracked projected movie 


Trackedmovie=char(strcat(MovieFolder, '\',NameTrackedProjected));
%Saves the path to the tracked projected movie

%2. Save path to untracked projected movie 
%Operation analog to tracked projected movie
SearchUntrackedProjected = strfind(allNames,' untracked projected');
Index = find(~cellfun(@isempty, SearchUntrackedProjected));
NameUntrackedProjected = allNames(Index);

Untrackedmovie=char(strcat(MovieFolder, '\',NameUntrackedProjected));



