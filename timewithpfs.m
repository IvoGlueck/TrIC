function [stacktime] = timewithpfs (PFS_timings_file_name, PFS_timings_file_path, nPlanes, removal_status)
    %Inputs:
    %nPlanes: Number of Z planes inserted in the GUI
    
    %Outputs:
    %stacktime: Time stemps [s] passed before each stack/each frame of the projected movie
    
    %Gets the path to the file containing the time stamps recorded by the Labview program (Remove first line containing text in text file)  
    %[FileName,PathName,FilterIndex] = uigetfile('X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Experiments\Analysis experiments\20161213_61st_exp\Track 1\*.txt','Select timestamps of PFS');
    rawtimepath=char(strcat(PFS_timings_file_path,PFS_timings_file_name));
    
    frametime=load(rawtimepath); %for every frame the time passed before the frame recorded by labview [ms]
    
    if removal_status == 1
    
        %Removes times belonging to first stack 
        pointer = 0;
        for i=1:nPlanes
            pointer = pointer+1;
            if pointer <= nPlanes
                frametime(1) = []; 
            end
        end

        %removes timestamp of the first frame of each stack
        removalList = 1:(nPlanes):(numel(frametime));  %removalList contains the index of the first frame of every stack
        frametime(removalList) = [];

        %removes time points belonging to incomplete stacks at the end of the movie
        N_full_stacks = floor((numel(frametime))/(nPlanes-1)); % number of complete stacks
        N_frames_remove_end = (numel(frametime) - (N_full_stacks*(nPlanes-1))); % number of frames belonging to incomplete stacks
        frametime = frametime(1:(end-N_frames_remove_end));

        %sets starting time point to 0
        frametime = frametime-(frametime(1));

        %calculates timestamps of the projected movie: for each stack the time passed before the stack
        counter=0;
        for index = 1:(nPlanes-1):numel(frametime)-(nPlanes-2)   
            counter = counter + 1;
            stacktime(counter) = frametime(index);
        end
        stacktime = stacktime';
    
    
    else
        
        %removes time points belonging to incomplete stacks at the end of the movie
        N_full_stacks = floor((numel(frametime))/(nPlanes)); % number of complete stacks
        N_frames_remove_end = (numel(frametime) - (N_full_stacks*(nPlanes))); % number of frames belonging to incomplete stacks
        frametime = frametime(1:(end-N_frames_remove_end));

        %sets starting time point to 0
        frametime = frametime-(frametime(1));

        %calculates timestamps of the projected movie: for each stack the time passed before the stack
        counter=0;
        for index = 1:(nPlanes):numel(frametime)-(nPlanes-1)   
            counter = counter + 1;
            stacktime(counter) = frametime(index);
        end
        stacktime = stacktime';
        
    end   
    
    %calculates the time in seconds
    frametime = frametime/1000;
    stacktime = stacktime/1000;    
    