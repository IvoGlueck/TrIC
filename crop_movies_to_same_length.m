function [same_length_movie_ch1, same_length_movie_ch2] = crop_movies_to_same_length (uncropped_movie_ch1, uncropped_movie_ch2)
    
% crops loaded movies to the same length

    length_uncropped_movie_ch1 = length(uncropped_movie_ch1(1,1,:)); 
    
    length_uncropped_movie_ch2 = length(uncropped_movie_ch2(1,1,:));
    
    if length_uncropped_movie_ch1 > length_uncropped_movie_ch2
        
        same_length_movie_ch1 = uncropped_movie_ch1(:,:,1:length_uncropped_movie_ch2);
        
        same_length_movie_ch2 = uncropped_movie_ch2;
        
    end
    
    if length_uncropped_movie_ch2 > length_uncropped_movie_ch1
        
        same_length_movie_ch2 = uncropped_movie_ch2(:,:,1:length_uncropped_movie_ch1);
        
        same_length_movie_ch1 = uncropped_movie_ch1;
        
    end