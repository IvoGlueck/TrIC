function [transf12,transf21,Accuracy] = calib4_mass_working(Filename1,Filename2)  
%to automatically detect fluorescent beads position/spinning dik pattern and create the
%transformation matrix for dual channel calibration
%Filename2 is aligned to Filename1
%Aur�lie Dupont

%close all;

disp('Calculation of transformation matrix ongoing');
info = imfinfo(Filename1);
W = info.Width;
H = info.Height;

%Preallocates variable for stack containing disk pattern in the fixed channel
I1 = zeros([H W length(info)],'uint16');

for frame = 1:length(info)
    
    I1(:,:,frame)=imread(Filename1, 'Index', frame); %I1 contains image 1
    
end

I1_average_proj = uint16(mean(I1, 3));
    
info = imfinfo(Filename2);

%Preallocates variable for stack containing disk pattern in the channel which will be shifted
I2 = zeros([H W length(info)],'uint16');
    
for frame = 1:length(info)

    I2(:,:,frame)=imread(Filename2, 'Index', frame); %I2 contains image 2 which will be shifted
    
end

I2_average_proj = uint16(mean(I2, 3));

NonAlignedMerge = imfuse(I1_average_proj,I2_average_proj,'falsecolor','Scaling','joint','ColorChannels',[2 1 0]);
figure('name', 'Merged channels (not aligned)');
imshow(NonAlignedMerge)

%Polynomial degree, choose 2 or 3
D=3; %for spinning disk pattern set to 3
%tic; %activate in case you want to measure the time the program needs for calculation of the matrix 

%adjustable parameters
dp=5;%size of the ROI around each particle, for peak finding, default=10; for spinning disk pattern set to 5
dps=5; %to differentiate two different particles; for spinning disk pattern set to 5
para1=1;%how high above background should the particle be, default=5; for spinning disk pattern set to 1
para2=1;%how high above background should the particle be, default=5; for spinning disk pattern set to 1

%locate the spots in the first channel
psub1=Find_Peaks(I1_average_proj,para1,dp); %psub1 contains the coordinates of the peaks in I1
%p1_out=Refine_gauss(psub1,I1);
p1_out=Refine_mass(psub1,I1_average_proj); %Center of mass determination to obtain sub-pixel accuracy position in 2D
l1=length(psub1); %number of peaks found in image I1

%To show detected peaks in Filename1 %COMMENT OUT
%figure('name', 'Detected peaks Filename1'); hold on;
%imagesc(I1);axis image;%COMMENT OUT
%hold on;plot(p1_out(:,1),p1_out(:,2),'+w'); %COMMENT OUT

%First look for a global shift (best with Tucam)
IM1=fft2(I1_average_proj); %calculates 2D fast Fourier transform of I1; value of each pixel is transformed 
IM2=fft2(I2_average_proj); %calculates 2D fast Fourier transform of I2; value of each pixel is transformed
CC=abs(real(fftshift(ifft2(IM1.*conj(IM2))))); %conj(IM2) returns complex conjugate of the elements of Z; ifft2: inverse fast fourier transform; CC contains the absolute values of the real parts of the transformation matrix, one value for each pixel of the image 
[a,b]=find(CC==max(max(CC))); % a,b are the coordinates of the maximum of CC (single coordinate)
transfCC = cp2tform(psub1,psub1,'polynomial',D);% transfCC is a structure with fields; to obtain a tform structure %cp2tform(movingPoints, fixedPoints, transformation type, order of the polynomial to use); infers spatial transformation from control point pairs
transfCC.tdata(1,1)=-(b-floor(W./2)); %rounds values
transfCC.tdata(1,2)=-(a-floor(W./2)); %rounds values
%-(b-floor(W./2))
%-(a-floor(W./2))
I2shiftCC=uint16(imtransform(I2_average_proj,transfCC,'XData',[1 W], 'YData',[1 H]));%contains the image of Filename2 shifted with respect to the image of Filename1 (global shift)
%To save the tform structure
%filename = 'transfCC.mat';
%save(filename,transfCC);

%To save transformation matrix correcting global shift:
%save global_transf.mat transfCC
%File=strcat(Filename2(1:length(Filename2)-4),'Global_Mass_trans.tif');
%imwrite(I2shiftCC,File,'tiff');

%locate the spots in the other channel
psub2=Find_Peaks(I2shiftCC,para2,dp); %finds the peaks in the globally shifted image (Filename2)
%p2_out=Refine_gauss(psub2,I2);
p2_out=Refine_mass(psub2,I2shiftCC); %Center of mass determination to obtain sub-pixel accuracy position in 2D
l2=length(psub2);   %contains the number of peaks found

%To show detected peaks in Filename2 (globally shifted) 
%figure('name', 'Detected peaks in globally shifted Filename2'); %COMMENT OUT
%imagesc(I2shiftCC);axis image; hold on; %COMMENT OUT
%plot(p2_out(:,1),p2_out(:,2),'+w'); hold off; %COMMENT OUT

%compare the two lists; does some further selection of which points to take
%for the calculation of the transformation matrix
L1=[];
L2=[];
for i=1:l1 %iterates over the number of peaks found in image 1
    Ix=find(psub2(:,1)>psub1(i,1)-dps & psub2(:,1)<psub1(i,1)+dps);
    if ~isempty(Ix)
        Iy=find(psub2(Ix,2)>psub1(i,2)-dps & psub2(Ix,2)<psub1(i,2)+dps);
        if length(Iy)==1
            P1=i;
            P2=Ix(Iy);
            L1=[L1;p1_out(P1,:)];
            L2=[L2;p2_out(P2,:)];
        end
    end
end

%To depict the points remaining after the selection for calculation of the matrix
%  length(L1)
  %figure('name', 'Detected peaks Filename1 after selection'); hold on; %COMMENT OUT
  %imagesc(I1);axis image; hold on; %COMMENT OUT
  %plot(L1(:,1),L1(:,2),'+w');hold off; %COMMENT OUT
  
  %figure('name', 'Detected peaks in globally shifted Filename2 after selection'); %COMMENT OUT
  %imagesc(I2shiftCC);axis image; hold on; %COMMENT OUT
  %plot(L2(:,1),L2(:,2),'+w');hold off; %COMMENT OUT




 L2(:,1)=L2(:,1)-(b-floor(W./2));
 L2(:,2)=L2(:,2)-(a-floor(W./2));
 %length(L2)
 Accuracy=L1-L2;
 Accuracy=mean(abs(Accuracy)).*0.130;
 
%Where the calculation actually takes place
%Alternative to "cp2tform"
%transf12 = fitgeotrans(L2,L1,'polynomial',D);
%transf21 = fitgeotrans(L1,L2,'polynomial',D);

%TFORM = cp2tform(movingPoints,fixedPoints, transformtype)
transf12 = cp2tform(L2,L1,'polynomial',D); %transformation matrix where Filename1 is kept and Filename2 is shifted 
transf21 = cp2tform(L1,L2,'polynomial',D); %transformation matrix where Filename2 is kept and Filename1 is shifted

%Old comments by Aur�lie:
% transf12.tdata(1,1)=transf12.tdata(1,1)+(b-floor(W./2));%or shift the positions before cp2tform
% transf12.tdata(1,2)=transf12.tdata(1,2)+(a-floor(W./2));
%T2 = fliptform(T)%reverse the tform?

%COMMENT OUT
%B2 = imtransform(I2,transf12,'XData',[1 info.Width], 'YData',[1 info.Height]); %here the transformation matrix is applied to Filename2, B2 contains the shifted image 
%To depict and save the shifted image
%figure;imagesc(B2);axis image;
%File=strcat(Filename2(1:length(Filename2)-4),'Mass_trans.tif');
%imwrite(B2,File,'tiff');
%COMMENT OUT

%transf=load(Data_path);
%correct_shift_stack (Filename2, transf12)

%save transf12.mat transf12; %save the transformation matrix shifting Filename2

%toc; %activate in case you want to measure the time the program needs for calculation of the matrix

disp('Calculation of transformation matrix completed');

%====================================================================%
function psub=Find_Peaks(frame,para,dp)

%Detect spots in the frame
%Inspired from find_peaks_seg.m from Gregor Heiss

Thres_par=0.90; %sets pixelwise threshold
%(keeps only 1-Thres_par brightest pixels)
% para=5;

frame=uint16(frame); %Ivo's comment: Converts gray scale values of image to 16-bit unsigned integer meaning: the pixel intensities are now measured in values from 0 to 65.535

% mask for peaks finding
% dp=10;
dp_peak=2;

% generates masks
s=size(frame(:,:,1)); %s is a vector containing the number of pixels of the image in x and y
frame_outer=uint16(zeros(s(1),s(2))); %creates an array of zeros with the dimensions x and y (e.g. 512x256) 
frame_inner=ones(s(1)-2*(dp), s(2)-2*(dp)); %creates an array of ones with the dimensions x-2*dp and y-2*dp (e.g. 502x246) that means excludes the outmost 10 pixels 
frame_outer(dp+1:s(1)-dp,dp+1:s(2)-dp)=frame_inner; %transforms frame_outer into a matrix of size x and y containing ones with a frame of dp (e.g. 5) zeros around it


%circle_outer=uint16(xor(circle_d(dp,2*dp+1), circle_d(dp_peak,2*dp+1)));
circle_outer=circle_d(dp_peak,2*dp+1); %not a circle anymore; creates an array of ones (2*dp+1 (11x11)) with an inner circle-like area of zeros
circle_inner=abs(1-circle_d(dp_peak,2*dp+1)); %the same as "circle _outer" but ones and zeros exchanged
circle_exclude=circle_d(dp,2*dp+1); %large circle of zeros inbetween ones
j=0;
peak=[];

thrash=(sort(reshape(frame,1,[]))); %writes all pixel values of the image into a 1xnumof pixel vector and sorts the values in ascending order
% threshold map contains 2% of brightest pixels -> more than 500 peaks
thrash=thrash(round(length(thrash)*Thres_par)); %gives the value of the of the pixel at 90% length of trash
peak_map=uint16(frame>thrash); %gives 1 at peaks positions %creates matrix with the dimensions of the frame, fills it with zeros and inserts one where the pixel values exceeds the value saved in thrash; the 10% brightest pixels are selected 


%peak_map=peak_map.*frame_outer.*frame.*(abs(1-peaks_master));

peak_map=peak_map.*frame.*frame_outer; %sets a frame of size dp around the peak map where the values are 0; at peaks positions the intensitiy values of the image are shown

% look for the brightest peak!
[px, py]=find(peak_map==max(max(peak_map)),1,'first'); %finds the coordinates of the first max peak in matrix
i=0;
psub=[];%peaks positions
% perm=(size(circle_exclude,1)-1)/2 %8

% as long as peaks in threshold map
while sum(sum(peak_map))>0 %as long as sum over all peaks in the treshold map is greater than 0 
    % get the ROI 11x11 with dp=5
    roi=frame(px-dp:px+dp,py-dp:py+dp); % contains the pixel intensities of 11x11 around the peak position

    % check if peak
    sig=roi.*(circle_inner); %takes only the particle (disk with diameter 2*dp_peak+1) %only pixel intensities within circle with radius of 2*dp_peak+1 around peak are kept
%     imagesc(sig);pause;
    msig=mean(sig(sig>0)); %mean of peak intensity
    
    bg=roi.*circle_outer; %takes the outer of the particle
%     imagesc(bg);pause;
    mbg=mean(bg(bg>0));%mean background
    sbg=std(double(bg(bg>0))); %calculates the standard deviation of the background 

            if msig > ((para*sbg)+mbg) %here peaks are selected according to there intensity above background 
                    i=i+1;             %contains the number of selected peaks
                    psub(i,1:2)=[py px]; %saves the peaks position 
            else
                j=j+1;      %contains the number of non-selected peaks
            end
  


       % erase peaks in thershold mask
%         peak_map(px-perm:px+perm,py-perm:py+perm)=peak_map(px-perm:px+perm,py-perm:py+perm).*circle_exclude;
peak_map(px-dp:px+dp,py-dp:py+dp)=peak_map(px-dp:px+dp,py-dp:py+dp).*circle_exclude;
        [px, py]=find(peak_map==max(max(peak_map)),1,'first'); %finds the next maximum 
        %imagesc(peak_map); to depict the peak map at the end 
end %while

%=======================================================================
 function [ax,ay,flag]=Gaussian2D(frame,xmax,ymax)

%to fit a frame with a 2D gaussian
%returns [ax,ay] coordinates of the maximum 
%close all;

I=double(frame);
I=I-mean(mean(I));
mm=max(max(I(:,:)));
%[my mx]= find(I(:,:)==mm,1);
my=ymax;
mx=xmax;
[n,m]=size(I);
[X,Y]=meshgrid(1:n,1:m);%your x-y coordinates
x(:,1)=X(:); % x= first column
x(:,2)=Y(:); % y= second column

f=I(:);          % your data f(x,y) (in column vector)
%mI=max(max(I(:,:)));
%[myf mxf]= find(I(:,:)==mI,1);

fun = @(c,x) c(1)+c(2)*exp(-((x(:,1)-c(3))/c(4)).^2-((x(:,2)-c(5))/c(6)).^2);
c0=[-10 mm mx 3 my 3];%start-guess here
options=optimset('TolX',1e-3,'MaxIter',4000,'MaxTime',.5,'Display','off');

%fit
% [x,resnorm,residual,exitflag]
[cc, a , b , c, d]=lsqcurvefit(fun,c0,x,f,[],[],options);
d;
cc;
flag=c;

ax=cc(3);
ay=cc(5);

% Ifit=fun(cc,x); %your fitted gaussian in vector
% Ifit=reshape(Ifit,[n m]);%gaussian reshaped as matrix
% cc=round(cc);
% 
% figure
%  imagesc([Ifit I])
% drawnow
% title(['Sigma: ' num2str(cc(4)) ' / ' num2str(cc(6))]);
% axis image
% axis off
% pause;


%========================================================================
function S_out=Refine_mass(Spots,Frame)

%Center of mass determination to obtain sub-pixel accuracy position in 2D
%Frame should be square

sz=size(Frame);
%L=length(Spots);
L=size(Spots);
L=L(1);
dp=3;
Sx=[];
Sy=[];
for j = 1:L
%local image
    py =Spots(j,1); px=Spots(j,2);
    roi=Frame(px-dp:px+dp,py-dp:py+dp);
    %size(roi);
%Projections 
Framex=sum(roi,1);
Framey=sum(roi,2);
Sx=0;Sy=0;
j;
for i=1:2*dp+1
   Sx=Sx+i.*Framex(i);
   Sy=Sy+i.*Framey(i);
end
Centerx=Sx./sum(Framex);
Centery=Sy./sum(Framey);
% figure;plot(Framex,'.');hold on;plot(Centerx,1,'+r');hold off;
% figure;plot(Framey);hold on;plot(Centery,1,'+r');hold off;
% pause;
  
        Xsub(j)=Centerx+py-dp-1;
        Ysub(j)=Centery+px-dp-1;

end

S_out=[Xsub' Ysub'];



%==============================================================
function circle=circle_d(r,l)
% Generates circular Mask with radius r and box size l

circle=zeros(l);
l=(l-1)/2;
for i = -l:l
    for j= -l:l
        if round(sqrt((i*i)+(j*j)))>r
        circle(i+l+1,j+l+1)=1;
        end
    end
end
circle=uint16(circle);


