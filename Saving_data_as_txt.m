%this code goes at the end of AllAnalysis

global Curdata;
global usersvalues;
global Filenames;
%as "Curdata" and "Filenames" are both global variables the fields and their entries still exist  

usersvalues.new3D=get(handles.new3D,'Value');
%defines a new variable "uservalues" which is of data type struture and 
%creates a new field "new3D" in it. The value of the function "get" is
%stored in this field.
%reads out if the tick box "new 3D" in the GUI is ticked; if its ticked it 
%"Value" is 1, if its not ticked "Value" is 0 

 if usersvalues.new3D==1
    %if the values stored in usersvalues.new3D equals 1 (which is the case if 
    %the "New 3D" tick box is ticked) the if-loop is entered; if the box is
    %not ticked the value in "uservalues.new3D" is 0
          
     finaldata=[Curdata.frames Curdata.time Curdata.x Curdata.y Curdata.z ...
                Curdata.xsub Curdata.ysub Curdata.zsub ...
                Curdata.maxcc Curdata.maxccrand Curdata.ccposx Curdata.ccposy Curdata.ccposz ...
                Curdata.intGmax Curdata.intGcorrected Curdata.intGbackground ...
                Curdata.intRmax Curdata.intRcorrected Curdata.intRbackground ...
                Curdata.speed];
 else
    finaldata= [Curdata.frames Curdata.time Curdata.x Curdata.y Curdata.z ...
                Curdata.xsub Curdata.ysub Curdata.zsub ...
                Curdata.maxcc Curdata.ccposx Curdata.ccposy ...
                Curdata.intGmax Curdata.intGcorrected Curdata.intGbackground ...
                Curdata.intRmax Curdata.intRcorrected Curdata.intRbackground ...
                Curdata.speed]; 
                %else contains the same as the if part with the exception of "maxccrand"
                %and ccposz
                %info for 2D analysis

 end
 
                DefaultName =char(Filenames.rawtrack);
                %creates the variable "DefaultName" and saves the value of
                %"Filenames.rawtrack" (path to file containing results of manual 2D tracking 
                %to it. 

                %supresses the letters _TRAC if the track file was loaded via the open
                %analysis button
                DefaultName = regexprep(DefaultName, '_TRAC', '');

                %supresses the letters .txt (so that they do not appear in the middle of
                %the suggested name)
                DefaultName = regexprep(DefaultName, '.txt', '');

                %adds _TRAC or _TRAC3D after the suggested name
                if  usersvalues.new3D==1         
                    DefaultName = char(strcat(DefaultName,'_TRAC3D'));
                else
                    DefaultName = char(strcat(DefaultName,'_TRAC'));
                end
                
                Data_path=char(strcat(usersvalues.result, '\', DefaultName));
                dlmwrite(Data_path,finaldata,'delimiter','\t','precision',6);
                %writes array "finaldata" into an ASCII-format file called as the file 
                %containing the 2D manual tracking followed by "_TRAC3D" or "_TRAC" using tabs to seperate
                %data 
                   
                
                %----------------
                %This code goes at the end of multiselect function
                
                %% writes results into text file 
                
 if usersvalues.new3D==1
    %if the values stored in usersvalues.new3D equals 1 (which is the case if 
    %the "New 3D" tick box is ticked) the if-loop is entered; if the box is
    %not ticked the value in "uservalues.new3D" is 0
          
     finaldata=[Curdata.frames Curdata.time Curdata.x Curdata.y Curdata.z ...
                Curdata.xsub Curdata.ysub Curdata.zsub ...
                Curdata.maxcc Curdata.maxccrand Curdata.ccposx Curdata.ccposy Curdata.ccposz ...
                Curdata.intGmax Curdata.intGcorrected Curdata.intGbackground ...
                Curdata.intRmax Curdata.intRcorrected Curdata.intRbackground ...
                Curdata.speed];
 else
    finaldata= [Curdata.frames Curdata.time Curdata.x Curdata.y Curdata.z ...
                Curdata.xsub Curdata.ysub Curdata.zsub ...
                Curdata.maxcc Curdata.ccposx Curdata.ccposy ...
                Curdata.intGmax Curdata.intGcorrected Curdata.intGbackground ...
                Curdata.intRmax Curdata.intRcorrected Curdata.intRbackground ...
                Curdata.speed]; 
                %else contains the same as the if part with the exception of "maxccrand"
                %and ccposz
                %info for 2D analysis

 end
                
             %% saves results in text file    
                Results_file_name=char(strcat('Results_Track_', track_name, '.txt')); %contains the name of the file containing the results of the current track
                Results_folder_name=char(strcat('Results_Track_', track_name)); %contains the name of the folder in which the results will be saved
                Parent_data_path=char(strcat(usersvalues.result_location, '\')); %path to the folder containing the result folders
                mkdir ([Parent_data_path Results_folder_name]) %creates the folder in which the results will be saved 
                Data_path = char(strcat(usersvalues.result_location, '\',Results_folder_name, '\', Results_file_name)); %total path for saving the results file of the current track
                dlmwrite(Data_path,finaldata,'delimiter','\t','precision',6);
                %writes array "finaldata" into an ASCII-format file called as the file 
                %containing the 2D manual tracking followed by "_TRAC3D" or "_TRAC" using tabs to seperate
                %data 
                