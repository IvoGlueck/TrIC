function varargout = TRAC_old(varargin)
% varargout (function with variable/any number of outputs) 
% varargin (variable with variable/any number of inputs)
% 
% TRAC_old M-file for TRAC_old.fig
%      TRAC_old, by itself, creates a new TRAC_old or raises the existing
%      singleton*. Singleton says that only one TRAC_old window can be open at
%      each time. If the TRAC_old.m code is executed twice the open TRAC_old window
%      will be activated (brought to front) but no second window will be
%      opened. 
%
%      H = TRAC_old returns the handle to a new TRAC_old or the handle to
%      the existing singleton*.
%
%      TRAC_old('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRAC_old.M with the given input arguments.
%
%      TRAC_old('Property','Value',...) creates a new TRAC_old or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TRAC_old_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TRAC_old_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TRAC_old

% Last Modified by GUIDE v2.5 01-Apr-2018 15:55:42

% below starts the initialization of the GUI.

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1; 
gui_State = struct('gui_Name',       mfilename, ... %contains the name of the current mfile so here TRAC
                   'gui_Singleton',  gui_Singleton, ... %is an option (0 or 1) that allows only one or several instance of your Gui to be started at the same time.
                   'gui_OpeningFcn', @TRAC_old_OpeningFcn, ...
                   'gui_OutputFcn',  @TRAC_old_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
               %gui_State is a structure containing 6 fields
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

%the above code initializes the GUI and is automatically inserted if a new
%GUI is created 

% --- Executes just before TRAC_old is made visible.
function TRAC_old_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TRAC_old (see VARARGIN)

% Choose default command line output for TRAC_old
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TRAC_old wait for user response (see UIRESUME)
% uiwait(handles.figure1);

set(get(handles.axes1,'XLabel'),'String','relative X position (�m)'); 
set(get(handles.axes1,'YLabel'),'String','relative Y position (�m)'); 
%labels the axes of the "XY Tracking with fit" plot

set(get(handles.axes2,'XLabel'),'String','relative time (s)');
set(get(handles.axes2,'YLabel'),'String','relative Z position (�m)');
%labels the axes of the "Z Tracking with fit" plot

set(get(handles.axes4,'XLabel'),'String','relative time (s)');
set(get(handles.axes4,'YLabel'),'String','background-corrected intensity');
%sets the labels of the axes of the "Intensity" plot 

set(get(handles.axes3,'XLabel'),'String','relative time (s)');
set(get(handles.axes3,'YLabel'),'String', sprintf ('max. value of ICC \n of tracked particles'));
%sets the labels of the axes of the upper "Cross-correlation colocalization analysis" plot 

set(get(handles.axes5,'XLabel'),'String','relative time (s)');
set(get(handles.axes5,'YLabel'),'String', sprintf ('coordinates of max. value \n of ICC of tracked particles'));
%sets the labels of the axes of the lower "Cross-correlation colocalization analysis" plot 



% --- Outputs from this function are returned to the command line.
function varargout = TRAC_old_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;







%% Run only XY tracking

% --- Executes on button press in loadtrack.
function loadtrack_Callback(hObject, eventdata, handles)
% hObject    handle to loadtrack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Curdata 
global Filenames
% a variable of type "global" is visible/can be accessed by all functions
% which set it as global 

[FileName,PathName,FilterIndex] = uigetfile('.txt','Select raw tracked data');
%returns the name and path to the file you select; under "filter index" the
%extension of the file is returned (e.g. .m) (not used in this function)
%(creates the variables "FileName", "Path Name" and "FilterIndex")
%command "uigetfile" has the general inputs (FilterSpec,DialogTitle)
%If FilterSpec is a file name, that file name displays, selected in the File 
% name field. The extension of the file is the default filter.
%here an open dialog box with the title "Select raw tracked data" is opened; and
%files with extension ".txt" can be selected 

 A=char(strcat(PathName,FileName));
 %strcat concatenates "PathName" and "FileName"; the resulting path will 
 %lead to the file which should be opened; the path will be of type char (letters)
 % and stored in the variable "A"

    data=load(A);
  %loads data stored in the file found under path A and stores it in variable "data"
  %"data" contains a column with the frames for which the tracking was done, and the corresponding x and y values 
  %three columns in total 
    
Filenames.rawtrack=A;
% in the variable "Filenames" a field "rawtrack" is created in which the
% value of A (the path to the results of the 2D manual tracking is contained) is saved. 

Curdata.frames=data(:,1);
% in the variable "Curdata" a field "frames" is created in which the value
% of the first column of "data" (tracked frames) is saved 
%"frames" is a one dimensional array => matrix 

Curdata.x=data(:,2);
% in the variable "Curdata" a field "x" is created in which the value
% of the second column of "data" (x values) is saved 

Curdata.y=data(:,3);
% in the variable "Curdata" a field "y" is created in which the value
% of the third column of "data" (y values) is saved 


[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the tracked projected movie');
%returns the name and path to the file you select; under "filter index" the
%extension of the file is returned (e.g. .m) (unused here)

Filenames.trackedmovie=char(strcat(PathName,FileName));
% in the variable "Filenames" a field "trackedmovie" is created in which the path to the 
% tracked projected movie is saved. 
 
%Aurelies comment: 
% [FileName,PathName,FilterIndex] = uigetfile('.tif','Select the non tracked movie');
% Filenames.untrackedmovie=char(strcat(PathName,FileName));


[Xsub,Ysub] = XYtrackingfit(Curdata.x,Curdata.y,Filenames.trackedmovie,Curdata.frames(1));
%outputs are: "Xsub" and "Ysub" are the results of the 2DGaussian fit with subpixel 
%resolution produced by the function "XYtrackingfit" (see seperate file); 
%inputs are: the x and y positions obtained by manual tracking, the path to the tracked projected movie and the 
%value stored in the first row of the array "curdata.frames" which is the first frame 
%for which 2D manual tracking was performed (given in parentheses, cannot be 0)

Curdata.xsub=Xsub;
%in the variable "Curdata" a field "xsub" is created in which the value
% of "Xsub" is saved. 

Curdata.ysub=Ysub;
%in the variable "Curdata" a field "ysub" is created in which the value
% of "Ysub" is saved. 

plot(handles.axes1,Curdata.xsub,Curdata.ysub);
%plots the fitted X and Y values of the tracking in the window "XY Tracking with fit" in the GUI; 
%"(handles.axes1)" sends the information into into the "XY tracking with fit" window in the GUI; 
%sets this window as active.
hold(handles.axes1,'on');plot(handles.axes1,Curdata.xsub(1),Curdata.ysub(1),'*r');hold(handles.axes1,'off');
%hold "on" retains plots of the "handles.axes1" so that new plots added to
%the axes do not delete existing plots.
%the first entry in the matrix "Curdata.xsub" and "Curdata.ysub" are
%plotted into the graph with a red star => starting position of the virus
%track is shown
%hold off sets the hold state to off for the "handles.axes1" so that new plots added to the axes clear existing 
%plots and reset all axes properties.





%% Save analysis (currently not needed as results are automatically saved)

% --- Executes on button press in saveanalysis. Obtained by the GUIDE
% environment:
function saveanalysis_Callback(hObject, eventdata, handles)
% hObject    handle to saveanalysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Curdata;
global Filenames;
%as "Curdata" and "Filenames" are both global variables the fields and their entries still exist  

usersvalues.new3D=get(handles.new3D,'Value');
%defines a new variable "uservalues" which is of data type struture and 
%creates a new field "new3D" in it. The value of the function "get" is
%stored in this field.
%reads out if the tick box "new 3D" in the GUI is ticked; if its ticked it 
%"Value" is 1, if its not ticked "Value" is 0 


%fn=fieldnames(Curdata);
%finaldata=[];
% N=length(fn)
%L=length(Curdata.(fn{1}));
% for i = 1:N
% finaldata=[finaldata Curdata.(fn{i})];
% end

 if usersvalues.new3D==1
 %if the values stored in usersvalues.new3D equals 1 (which is the case if 
 %the "New 3D" tick box is ticked) the if-loop is entered; if the box is
 %not ticked the value in "uservalues.new3D" is 0
          
     finaldata=[Curdata.frames Curdata.time Curdata.x Curdata.y Curdata.z ...
                Curdata.xsub Curdata.ysub Curdata.zsub ...
                Curdata.maxcc Curdata.maxccrand Curdata.ccposx Curdata.ccposy Curdata.ccposz ...
                Curdata.intGmax Curdata.intGcorrected Curdata.intGbackground ...
                Curdata.intRmax Curdata.intRcorrected Curdata.intRbackground ...
                Curdata.speed];


 else
    finaldata=[Curdata.frames Curdata.time Curdata.x Curdata.y Curdata.z ...
                Curdata.xsub Curdata.ysub Curdata.zsub ...
                Curdata.maxcc Curdata.ccposx Curdata.ccposy ...
                Curdata.intGmax Curdata.intGcorrected Curdata.intGbackground ...
                Curdata.intRmax Curdata.intRcorrected Curdata.intRbackground ...
                Curdata.speed]; 
                %else contains the same as the if part with the exception of "maxccrand"
                %and ccposz
                %info for 2D analysis

 end
 
DefaultName =char(Filenames.rawtrack);
% creates the variable "DefaultName" and saves the value of
% "Filenames.rawtrack" (path to file containing results of manual 2D tracking 
%to it. 

%supresses the letters _TRAC if the track file was loaded via the open
%analysis button
DefaultName = regexprep(DefaultName, '_TRAC', '');

%supresses the letters .txt (so that they do not appear in the middle of
%the suggested name)
DefaultName = regexprep(DefaultName, '.txt', '');

%adds _TRAC or _TRAC3D after the suggested name
if usersvalues.new3D==1
    DefaultName = char(strcat(DefaultName,'_TRAC3D'));
else
    DefaultName = char(strcat(DefaultName,'_TRAC'));
end


[FileName,PathName,FilterIndex] = uiputfile('.txt','Save analysed data',DefaultName);
 A=char(strcat(PathName,FileName));
dlmwrite(A,finaldata,'delimiter','\t','precision',6);
% writes array "finaldata" into an ASCII-format file called as the file 
%containing the 2D manual tracking followed by "_TRAC3D" or "_TRAC" using tabs to seperate
% data 
set(handles.ResultsPrinter,'String',horzcat('Data saved in ',FileName));
%in the top right of the TRAC_old window the name of the file in which the data
%are saved is printed 


%% opens result files 

% --- Executes during object creation, after setting all properties.
function listOpeningFiles_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listOpeningFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in openanalysis.
function openanalysis_Callback(hObject, eventdata, handles)
% hObject    handle to openanalysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global usersvalues

% Get filepath and filenames of loading directory/folder
[filename, filepath, cCheck] = uigetfile('X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Software\Matlab\Tiff reading\Movie_loading\Multitrack analysis\Results_analysis_txt\*.txt','Select track files', 'Multiselect', 'on');

assignin('base', 'filename', filename);
assignin('base', 'filepath', filepath);
assignin('base', 'cCheck', cCheck);

% If user did not press cancel
if (cCheck ~= 0)
    % Reset selection to first entry
    set(handles.listOpeningFiles, 'Value', 1);
    
    % Show selected file names in the listbox
    set(handles.listOpeningFiles, 'String', filename);
end

%saves the path to the selected files 
usersvalues.trackpath = filepath;

 set(handles.currentpathname,'String',filepath);
 %writes path to the folder containing the results into window "Current pathname:"


% --- Executes on selection change in listOpeningFiles.
function listOpeningFiles_Callback(hObject, eventdata, handles)
% hObject    handle to listOpeningFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listOpeningFiles contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listOpeningFiles

global Curdata
global Filenames
global usersvalues

% Get string and value of results file
resultsfilename = get(handles.listOpeningFiles, 'String');
resultsfilevalue = get(handles.listOpeningFiles, 'Value');

% Display selected file
if (iscell(resultsfilename))
   fileName = resultsfilename(resultsfilevalue);
   %fprintf('Selected file: %s\n', fileName);
else
    fileName = resultsfilename;
    %fprintf('Selected file: %s\n', fileName);
end  
    Data_path = char(strcat(usersvalues.trackpath,fileName));
    set(handles.currentpathname,'String',usersvalues.trackpath);
    %writes path of selected file into window "Current pathname:"
    Filenames.FileNameOnly=fileName;
    set(handles.showtracknber,'String',Filenames.FileNameOnly);
    data = load(Data_path);

    
    
if usersvalues.new3D==1
    
    Curdata.frames=data(:,1);
    %saves the first column of "data" in the field "Curdata.frames"

    Curdata.time=data(:,2);
    %saves the second column of "data" in the field "Curdata.time"; time since
    %the movie was started 

    Curdata.x=data(:,3);
    %saves the third column of "data" in the field "Curdata.x"; x position
    %obtained by manual tracking (pixels)

    Curdata.y=data(:,4);
    %saves the fourth column of "data" in the field "Curdata.y"; y position
    %obtained by manual tracking (pixels)

    Curdata.z=data(:,5);
    %saves the fifth column of "data" in the field "Curdata.z"; z position
    %obtained by manual tracking (pixels) ????

    Curdata.xsub=data(:,6);
    %saves the sixth column of "data" in the field "Curdata.xsub"; x position
    %obtained by 2D gaussian fitting (subpixels accuracy)

    Curdata.ysub=data(:,7);
    %saves the seventh column of "data" in the field "Curdata.ysub"; y position
    %obtained by 2D gaussian fitting (subpixels accuracy)

    Curdata.zsub=data(:,8);
    %saves the eigth column of "data" in the field "Curdata.zsub"; z position
    %obtained by 2D gaussian fitting (subpixels accuracy)

    Curdata.maxcc=data(:,9);
    %saves the ninth column of "data" in the field "Curdata.maxcc";
    %maximal value of the cross-correlation (normalized, comprised between 0 and 1)

    Curdata.maxccrand=data(:,10);
    %saves the ninth column of "data" in the field "Curdata.maxccrand";
    %maximal value of the randomized cross-correlation 

    Curdata.ccposx=data(:,11);
    %saves the eleventh column of "data" in the field "Curdata.ccposx";
    %x position of the cross-correlation maximum (pixels, comprised between -10 and +10)

    Curdata.ccposy=data(:,12);
    %saves the twelvth column of "data" in the field "Curdata.ccposy";
    %y position of the cross-correlation maximum (pixels, comprised between -10 and +10)

    Curdata.ccposz=data(:,13);
    %saves the 13th column of "data" in the field "Curdata.ccposz";
    %z position of the cross-correlation maximum (pixels, comprised between -10 and +10)

    Curdata.intGmax=data(:,14);
    %saves the 14th column of "data" in the field "Curdata.intGmax";
    %maximal intensity in green channel

    Curdata.intGcorrected=data(:,15);
    %saves the 15th column of "data" in the field "Curdata.intGcorrected";
    %background corrected intensity in green channel

    Curdata.intGbackground=data(:,16);
    %saves the 16th column of "data" in the field "Curdata.intGbackground";
    %background intensity in green channel

    Curdata.intRmax=data(:,17);
    %saves the 17th column of "data" in the field "Curdata.intRmax";
    %maximal intensity in red channel

    Curdata.intRcorrected=data(:,18);
    %saves the 18th column of "data" in the field "Curdata.intRcorrected";
    %background corrected intensity in red channel

    Curdata.intRbackground=data(:,19);
    %saves the 19th column of "data" in the field "Curdata.intRbackground";
    %background intensity in red channel

    Curdata.speed=data(:,20);
    %saves the 20th column of "data" in the field "Curdata.speed";
    %instantaneous velocity (�m/s)

 else
    
    Curdata.frames=data(:,1);
    Curdata.time=data(:,2);
    Curdata.x=data(:,3);
    Curdata.y=data(:,4);
    Curdata.z=data(:,5);
    Curdata.xsub=data(:,6);
    Curdata.ysub=data(:,7);
    Curdata.zsub=data(:,8);
    Curdata.maxcc=data(:,9);
    Curdata.ccposx=data(:,10);
    Curdata.ccposy=data(:,11);
    Curdata.intGmax=data(:,12);
    Curdata.intGcorrected=data(:,13);
    Curdata.intGbackground=data(:,14);
    Curdata.intRmax=data(:,15);
    Curdata.intRcorrected=data(:,16);
    Curdata.intRbackground=data(:,17);
    Curdata.speed=data(:,18);
    %the same as for "New3D" except that "randomized cross-correlation" and the 
    %"z position of the cross-correlation maximum (pixels, comprised between -10 and +10)
    %are not included" 
    
 end
    
%plots subpixel accurate XY coordinates of track in the GUI in "XY tracking with fit"
plot(handles.axes1,Curdata.xsub*usersvalues.pixxy,Curdata.ysub*usersvalues.pixxy); 

%labels the axes of the "XY Tracking with fit"-plot
set(get(handles.axes1,'XLabel'),'String','relative X position (�m)'); 
set(get(handles.axes1,'YLabel'),'String','relative Y position (�m)');

%plots Z coordinates in the "Z Tracking with fit" plot of the GUI and labels the axes
plot(handles.axes2,Curdata.time,Curdata.zsub*usersvalues.pixZ); %plots the substack accurate Z coordinates in �m against the time since the beginning of the track in the window "Z tracking with fit" of the GUI

%!!!Activate if corrected indices should be indicated!!!
%hold(handles.axes2,'on');plot(handles.axes2,Curdata.time(Index_corrected),Curdata.zsub(Index_corrected),'*r');hold(handles.axes2,'off'); %Z coordinates that have been corrected are marked with a red star in the plot 
set(get(handles.axes2,'XLabel'),'String','relative time (s)');
set(get(handles.axes2,'YLabel'),'String','relative Z position (�m)');

% plots the results in the upper "Cross-correlation colocalization analysis" plot
plot(handles.axes3,Curdata.time,Curdata.maxcc,'.');ylim(handles.axes3,[0 (max(Curdata.maxcc)+0.05)]);%Aurelie's original: plot(handles.axes3,Curdata.time,Curdata.maxcc,'.');ylim(handles.axes3,[0 1]); 
%Curdata.maxcc is a vector containing the maximum value the cross-correlation between 
%the stacks of tracked channel and the not tracked channel; contains one value for each stack
%plotted against the time since the beginning of the track 

%sets the labels of the axes of the upper "Cross-correlation colocalization analysis" plot 
set(get(handles.axes3,'XLabel'),'String','relative time (s)');
set(get(handles.axes3,'YLabel'),'String', sprintf ('max. value of ICC \n of tracked particle'));

% plots the refined coordinates of the maximum of the cross-correlation in the lower "Cross-correlation colocalization analysis" window 
plot(handles.axes5,Curdata.time,Curdata.ccposx,'.c', Curdata.time,Curdata.ccposy,'.m', Curdata.time,Curdata.ccposz,'.k'); ylim(handles.axes5,[-10 10]);
legend ({'X', 'Y', 'Z'}, 'FontSize', 6, 'Location', 'NorthWest')
%Curdata.ccposx is a vector containing the subpixel accurate x coordinate of the maximum of the the cross-correlation between 
%the stacks of the tracked and untracked channel; contains one value for each stack
%plotted against the time since the beginning of the track (only values between -10 and 10 are shown in the graph)

%sets the labels of the axes of the lower "Cross-correlation colocalization analysis" plot
set(get(handles.axes5,'XLabel'),'String','relative time (s)');
set(get(handles.axes5,'YLabel'),'String', sprintf ('coordinates of max. of ICC \n of tracked particle (pixels)'));


plot(handles.axes4,Curdata.time,Curdata.intGcorrected,'+g');
%plots the background corrected intensity in the green channel against the
%time in the window "intensity"
%uses green "crosses" for plotting
hold(handles.axes4,'on');plot(handles.axes4,Curdata.time,Curdata.intRcorrected,'.r'); hold(handles.axes4,'off');
%plots the background corrected intensity in the red channel against the
%time in the window "intensity" without overwriting the previous plot 
%uses red "dots" for plotting    

%sets the labels of the axes of the "Intensity" plot 
set(get(handles.axes4,'XLabel'),'String','relative time (s)');
set(get(handles.axes4,'YLabel'),'String','background-corrected intensity');

%% full plot analysis


% --- Executes on button press in fullplot.
function fullplot_Callback(hObject, eventdata, handles)
% hObject    handle to fullplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Curdata
global Filenames
global usersvalues

% usersvalues.dt=str2double(get(handles.inttime,'String'));
% Curdata.time=Curdata.frames.*usersvalues.dt;
[out] = fullplotanalysis(Curdata, usersvalues);
%creates a Matlab Figure 1 containing a plot of  
% a) the instantaneous speed in �m/s against the the absolute time since 
%    the beginning of the track in seconds  
% b) the Cross correlation maximum against the absolute time since the 
%    beginning of the track in seconds   
% c) the background corrected intensity in the green and in the red channel
%    against the the absolute time since the beginning of 
%    the track in seconds   
% d) The subpixel accurate x (blue) and y (red) coordinates of the cross 
%    correlation maximum against the absolute time since the beginning of 
%    the track in seconds  

%creates a Matlab Figure 2 containing a 3D plot of the virus trace including projections onto the three axes and 
%indication of the starting position by a green circle 









% --- Executes on button press in Intensity.
function Intensity_Callback(hObject, eventdata, handles)
% hObject    handle to Intensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Curdata
global Filenames
global usersvalues
%creates the global variable "uservalues"; no values saved into here 



[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the tracked projected movie');
Filenames.trackedmovie=char(strcat(PathName,FileName));
%creates the field "trackedmovie" in the variable "Filenames" and saves the
%path to the tracked projected movie into it

[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the NON-tracked projected movie');
Filenames.untrackedmovie=char(strcat(PathName,FileName));
%creates the field "untrackedmovie" in the variable "Filenames" and saves the
%path to the non-tracked projected movie into it

[FileName,PathName,FilterIndex] = uigetfile('.mat','Select the transformation structure resulting from calibration');
Filenames.transf=char(strcat(PathName,FileName));
%creates the field "transf" in the variable "Filenames" and saves the
%path to the transformation matrix into it


transf=load(Filenames.transf);
    fn=fieldnames(transf); %if .mat file
    %saves the data stored in the transformation matrix in a cell array of
    %strings called "fn"
    transf=transf.(fn{1});
    %calls to first row in the cell array "fn" and stores it in the
    %variable "transf"
    
[Int1_max,Int1_cor,Int2_max,Int2_cor] = Intwithcal(Curdata.xsub,Curdata.ysub,Filenames.trackedmovie,Filenames.untrackedmovie,transf,Curdata.frames(1));
%calls the function "Intwithcal" and gives into it the x and y position of
%the tracked particles with subpixel accuracy, the tracked movie, the
%untracked movie, the transformation matrix and 
usersvalues.trackchannel=get(handles.trackchannel,'Value');
%checks which channel has been selected for the tracking 
if usersvalues.trackchannel==1
Curdata.intGmax=Int1_max';
Curdata.intGcorrected=Int1_cor(:,1);
Curdata.intGbackground=Int1_cor(:,2);
Curdata.intRmax=Int2_max';
Curdata.intRcorrected=Int2_cor(:,1);
Curdata.intRbackground=Int2_cor(:,2);
else
Curdata.intGmax=Int2_max';
Curdata.intGcorrected=Int2_cor(:,1);
Curdata.intGbackground=Int2_cor(:,2);
Curdata.intRmax=Int1_max';
Curdata.intRcorrected=Int1_cor(:,1);
Curdata.intRbackground=Int1_cor(:,2);
end


usersvalues.PFS=get(handles.PFS,'Value'); 
%check if "With PFS" is checked
if usersvalues.PFS == 1;
usersvalues.firstFrame=str2double(get(handles.firstframe,'String'));
usersvalues.trackedFrames=str2double(get(handles.trackedframes,'String'));

usersvalues.dt = timewithpsf(usersvalues.Nz, usersvalues.firstFrame, usersvalues.trackedFrames);

else
usersvalues.dt=str2double(get(handles.inttime,'String'));
%returns the interval time set by the user and saves it into a
%double-precision representation which is stored in uservalues.dt
end

Curdata.time=(Curdata.frames-Curdata.frames(1)).*usersvalues.dt;

plot(handles.axes4,Curdata.time,Curdata.intGcorrected,'+g');
hold(handles.axes4,'on');plot(handles.axes4,Curdata.time,Curdata.intRcorrected,'.r'); hold(handles.axes4,'off');
set(get(handles.axes4,'XLabel'),'String','relative time (s)');
set(get(handles.axes4,'YLabel'),'String','background-corrected intensity');
%sets the labels of the axes of the "Intensity" plot 











% --- Executes on button press in CCanalysis.
function CCanalysis_Callback(hObject, eventdata, handles)
% hObject    handle to CCanalysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
parpool;
global Curdata
global Filenames
global usersvalues

usersvalues.twoD=get(handles.only2D,'Value');
usersvalues.new3D=get(handles.new3D,'Value');

[FileName,PathName,FilterIndex] = uigetfile('.mat','Select the transformation structure resulting from calibration');
Filenames.transf=char(strcat(PathName,FileName));
transf=load(Filenames.transf);
    fn=fieldnames(transf); %if .mat file
    transf=transf.(fn{1});
    
if usersvalues.twoD==1
    [FileName,PathName,FilterIndex] = uigetfile('.tif','Select the tracked movie');
Filenames.trackedmovie=char(strcat(PathName,FileName));

[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the NON-tracked movie');
Filenames.untrackedmovie=char(strcat(PathName,FileName));

[Max,Pos] = CContraj2D_trans(Filenames.trackedmovie,Filenames.untrackedmovie,Curdata.xsub,Curdata.ysub,Curdata.frames(1),Curdata.frames(end),transf);

Curdata.maxcc=Max;
Curdata.ccposx=Pos(:,1);
Curdata.ccposy=Pos(:,2);
Curdata.z=zeros(length(Curdata.x),1);
Curdata.zsub=zeros(length(Curdata.x),1);
%Calculate speed
usersvalues.dt=str2double(get(handles.inttime,'String'));
usersvalues.pixxy=str2double(get(handles.pixxy,'String'));
Curdata.time=(Curdata.frames-Curdata.frames(1)).*usersvalues.dt;
Curdata.speed= instspeed_2D(Curdata.time,Curdata.xsub,Curdata.ysub,usersvalues.pixxy);
    
else
usersvalues.Nz=str2double(get(handles.Zslices,'String'));

[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the tracked movie NOT PROJECTED');
Filenames.trackedmovieZstack=char(strcat(PathName,FileName));

[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the NON-tracked movie NOT PROJECTED');
Filenames.untrackedmovieZstack=char(strcat(PathName,FileName));

 if usersvalues.new3D==1
    disp('new 3D CC analysis');
    [Max,Pos,Maxrand,indoutofZ] = CC3D_trans(Filenames.trackedmovieZstack,Filenames.untrackedmovieZstack,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end),transf);
    Curdata.maxcc=Max;
    Curdata.maxccrand=Maxrand;
    Curdata.ccposx=Pos(:,1);
    Curdata.ccposy=Pos(:,2);
    Curdata.ccposz=Pos(:,3);
    
        if  isempty(indoutofZ) 
            set(handles.ResultsPrinter,'String','');
        else
            nonfitZ=char();
            for h=1:length(indoutofZ)
            nonfitZ=strcat(nonfitZ, ' ', num2str(indoutofZ(h)), ' ', ',');
            end
        set(handles.ResultsPrinter,'String',horzcat('Z position of CCmax could not be fitted for frame(s):', nonfitZ));
        end    
        
 else
[Max,Pos] = CContraj3D_trans(Filenames.trackedmovieZstack,Filenames.untrackedmovieZstack,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end),transf);

Curdata.maxcc=Max;
Curdata.ccposx=Pos(:,1);
Curdata.ccposy=Pos(:,2);
 end
end

%Ivo's stuff:
plot(handles.axes5,Curdata.time,Curdata.ccposx,'.c', Curdata.time,Curdata.ccposy,'.m', Curdata.time,Curdata.ccposz,'.k'); ylim(handles.axes5,[-10 10]);
legend ({'X', 'Y', 'Z'}, 'FontSize', 6, 'Location', 'NorthWest')
%Curdata.ccposx is a vector containing the subpixel accurate x coordinate of the maximum of the the cross-correlation between 
%the stacks of channel 1 and channel 2 
%contains one value for each stack
%plotted against the time since the beginning of the movie (only values
%between -10 and 10 are shown in the graph)
%plotted in the lower "Cross-correlation colocalization analysis" window
set(get(handles.axes5,'XLabel'),'String','relative time (s)');
set(get(handles.axes5,'YLabel'),'String', sprintf ('coordinates of max. of ICC \n of tracked particles'));
%sets the labels of the axes of the lower "Cross-correlation colocalization analysis" plot 


plot(handles.axes3,Curdata.time,Curdata.maxcc,'.');ylim(handles.axes3,[0 1]);
set(get(handles.axes3,'XLabel'),'String','relative time (s)');
set(get(handles.axes3,'YLabel'),'String', sprintf ('max. value of ICC \n of tracked particles'));
%sets the labels of the axes of the upper "Cross-correlation colocalization analysis" plot 

%Aurelie's stuff:
%plot(handles.axes5,Curdata.time,Curdata.ccposx,'.');ylim(handles.axes5,[-10 10]);
%hold(handles.axes5,'on');plot(handles.axes5,Curdata.time,Curdata.ccposy,'.r');hold(handles.axes5,'off');
delete (gcp) %shuts down parallel pool gcp = get current parallel pool











% --- Executes on button press in ztracking.
function ztracking_Callback(hObject, eventdata, handles)
% hObject    handle to ztracking (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Curdata
global Filenames
global usersvalues

usersvalues.Nz=str2double(get(handles.Zslices,'String'));

%if isempty (Filenames.trackedmovieZstack)==1

[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the tracked movie NOT PROJECTED');
Filenames.trackedmovieZstack=char(strcat(PathName,FileName));
[Z,Zsub,Index_corrected] = TDtrackingfit(Curdata.xsub,Curdata.ysub,Filenames.trackedmovieZstack,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end));

%else
 %   [Z,Zsub,Index_corrected] = TDtrackingfit(Curdata.xsub,Curdata.ysub,Filenames.trackedmovieZstack,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end));
%end

Curdata.zsub=Zsub;
Curdata.z=Z;

usersvalues.dt=str2double(get(handles.inttime,'String'));
Curdata.time=(Curdata.frames-Curdata.frames(1)).*usersvalues.dt;

plot(handles.axes2,Curdata.time,Curdata.zsub); 
hold(handles.axes2,'on');plot(handles.axes2,Curdata.time(Index_corrected),Curdata.zsub(Index_corrected),'*r');hold(handles.axes2,'off');
set(get(handles.axes2,'XLabel'),'String','relative time (s)');
set(get(handles.axes2,'YLabel'),'String','relative Z position (�m)');
%labels the axes of the "Z Tracking with fit" plot


usersvalues.pixxy=str2double(get(handles.pixxy,'String'));
usersvalues.pixZ=str2double(get(handles.pixz,'String'));
Curdata.speed= instspeed(Curdata.time,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.pixxy,usersvalues.pixZ);

% --- Executes on button press in xytracking.
function xytracking_Callback(hObject, eventdata, handles)
% hObject    handle to xytracking (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Curdata
global Filenames

set(handles.ResultsPrinter,'String', 'Only XY tracking performed') %every time only XY tracking is started by clicking the "Run XY tracking" button this is shown in the window in the upper right of the TRAC window

[Xsub,Ysub] = XYtrackingfit(Curdata.x,Curdata.y,Filenames.trackedmovie,Curdata.frames(1));

if Xsub==0;  %if Xsub is 0 this means that analysis has been aborted during XY tracking; needed to stop "TRAC" function
    set(handles.ResultsPrinter,'String', 'Analysis has been canceled during XY tracking') %message is shown in the upper right of the TRAC window
    return %function is left 
end

Curdata.xsub=Xsub;
Curdata.ysub=Ysub;

plot(handles.axes1,Curdata.xsub,Curdata.ysub);
hold(handles.axes1,'on');plot(handles.axes1,Curdata.xsub(1),Curdata.ysub(1),'*r');hold(handles.axes1,'off');
set(get(handles.axes1,'XLabel'),'String','relative X position (�m)'); 
set(get(handles.axes1,'YLabel'),'String','relative Y position (�m)'); 
%labels the axes of the "XY Tracking with fit plot"


function Zdist_Callback(hObject, eventdata, handles)
% hObject    handle to Zdist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Zdist as text
%        str2double(get(hObject,'String')) returns contents of Zdist as a double
global usersvalues

usersvalues.pixZ = str2num(get(hObject,'String'));


% --- Executes during object creation, after setting all properties.
function Zdist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Zdist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Zslices_Callback(hObject, eventdata, handles)
% hObject    handle to Zslices (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Zslices as text
%        str2double(get(hObject,'String')) returns contents of Zslices as a double
global usersvalues

usersvalues.Nz = str2num(get(hObject,'String'));


% --- Executes during object creation, after setting all properties.
function Zslices_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Zslices (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function inttime_Callback(hObject, eventdata, handles)
% hObject    handle to inttime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inttime as text
%        str2double(get(hObject,'String')) returns contents of inttime as a double
global usersvalues

usersvalues.dt = str2num(get(hObject,'String'));



% --- Executes during object creation, after setting all properties.
function inttime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inttime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in trackchannel.
function trackchannel_Callback(hObject, eventdata, handles)
% hObject    handle to trackchannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns trackchannel contents as cell array
%        contents{get(hObject,'Value')} returns selected item from trackchannel
global usersvalues

usersvalues.trackchannel = get(hObject,'Value');

% --- Executes during object creation, after setting all properties.
function trackchannel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trackchannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in only2D.
function only2D_Callback(hObject, eventdata, handles)
% hObject    handle to only2D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of only2D




%==========================================================================
%==========================================================================
% --- Executes on button press in Loadmovies.
function Loadmovies_Callback(hObject, eventdata, handles)
% hObject    handle to Loadmovies (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% loading global variables

global Filenames
global usersvalues
global movies

%% reading user inputs into GUI

%Reading in what kind of analysis has been chosen
usersvalues.twoD=get(handles.twoD,'Value'); %contains the status of the "2D" tickbox
usersvalues.old_threeD=get(handles.old3D,'Value'); %contains the status of the "Old 3D" tickbox
usersvalues.new_threeD=get(handles.new3D,'Value'); %contains the status of the "New 3D" tickbox
usersvalues.XYcorrection=get(handles.XYcorrection,'Value'); %contains the status of the "New 3D" tickbox

%Reads in number of Z slices if a 3D analysis has been chosen 
if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1 
    
    usersvalues.Nz=str2double(get(handles.Zslices,'String')); %Z slices

    %Error message if the number of Z slices has not been entered in the GUI
    if usersvalues.Nz == 0
       Nz_msgbox = msgbox('Please enter the number of Z slices recorded per stack before loading the movies', 'Error','error');
       return
    end

end


%% saves paths to raw movies + transformation structure
%Gets file names and folderpaths

if usersvalues.twoD == 1
    % 2D movies
    [TwoDmoviename_tracked,folderpath_tracked,FilterIndex] = uigetfile('.tif', 'Select movie file(s) belonging to the TRACKED channel (if multiple files are selected MIND THE ORDER)', 'MultiSelect', 'on');
    [TwoDmoviename_untracked,folderpath_untracked,FilterIndex] = uigetfile('.tif', 'Select movie file(s) belonging to the UNTRACKED channel (if multiple files are selected MIND THE ORDER)', 'MultiSelect', 'on');    

end 

if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1
    % Z stack movies
    [Rawmoviesnames_tracked,folderpath_tracked,FilterIndex] = uigetfile('.tif', 'Select movie file(s) belonging to the TRACKED channel (if multiple files are selected MIND THE ORDER)' , 'MultiSelect', 'on');
    [Rawmoviesnames_untracked,folderpath_untracked,FilterIndex] = uigetfile('.tif', 'Select movie file(s) belonging to the UNTRACKED channel (if multiple files are selected MIND THE ORDER)' , 'MultiSelect', 'on');

end 

%if XY correction is selected
if usersvalues.XYcorrection == 1 
    
    % Transformation structure
    [FileNameTransfFile,PathTransfFile,FilterIndex] = uigetfile('.mat','Select the transformation structure resulting from calibration', 'X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Experiments\Analysis experiments\20161213_61st_exp\Track 1\Transformation matrix');
    Filenames.transf=char(strcat(PathTransfFile,FileNameTransfFile));

end
%% sets info box

%if XY correction is selected
if usersvalues.XYcorrection == 1 
    
    set(handles.ResultsPrinter,'String', 'Loading of movies and transf. structure ongoing ...')
    pause(1);

else
    
    set(handles.ResultsPrinter,'String', 'Loading of movies ongoing ...')
    pause(1);

end
%% Loads movies

%if 2D analysis is selected ...
if usersvalues.twoD == 1
    
    % 2D movies
    disp('Loading of movie in the tracked channel ongoing');
    Load2Dmovie (TwoDmoviename_tracked, folderpath_tracked) 
    
    disp('Loading of movie in the untracked channel ongoing');
    Load2Dmovie (TwoDmoviename_untracked, folderpath_tracked)

    %if length(movies.non_proj_movie_tracked(1,1,:)) ~= movies.length(non_proj_movie_untracked(1,1,:))
       %this function is still missing
     %   [movies.non_proj_movie_tracked, movies.non_proj_movie_untracked] = crop_to_same_length (movies.non_proj_movie_tracked, movies.non_proj_movie_untracked(1,1,:));
    %end

end 

%if a 3D analysis is selected ...
if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1 

    % the non-projected movies are loaded ...
    disp('Loading of non-projected movie in the tracked channel ongoing');
    movies.non_proj_movie_tracked = LoadNonProjectedMovie (usersvalues.Nz, Rawmoviesnames_tracked, folderpath_tracked);

    disp('Loading of non-projected movie in the untracked channel ongoing');
    movies.non_proj_movie_untracked = LoadNonProjectedMovie (usersvalues.Nz, Rawmoviesnames_untracked, folderpath_untracked);

    %if length(movies.non_proj_movie_tracked(1,1,:)) ~= movies.length(non_proj_movie_untracked(1,1,:))
       %this function is still missing
     %   [movies.non_proj_movie_tracked, movies.non_proj_movie_untracked] = crop_to_same_length (movies.non_proj_movie_tracked, movies.non_proj_movie_untracked(1,1,:));
    %end

    % and the projected movies are created 
    disp('Projection of movie in the tracked channel ongoing');
    movies.proj_movie_tracked = CreateProjectedMovie(movies.non_proj_movie_tracked, usersvalues.Nz);

    disp('Projection of movie in the untracked channel ongoing');
    movies.proj_movie_untracked = CreateProjectedMovie(movies.non_proj_movie_untracked, usersvalues.Nz);

end

disp('Movies loaded');

%% sets info box

if usersvalues.XYcorrection == 1 
    
    set(handles.ResultsPrinter,'String', 'Movies and transf. structure loaded')
    
else
    
    set(handles.ResultsPrinter,'String', 'Movies loaded')

end

%% Things to put somewhere
%set(handles.currentpathname,'String',PathName);
%%


%%
%==========================================================================
%==========================================================================
% --- Executes on button press in AllAnalysis.
function AllAnalysis_Callback(hObject, eventdata, handles)
%Is called when the "Run analysis" button is pressed 
% hObject    handle to AllAnalysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% starts parallel pool if non is running
p = gcp;

%% loads global variables
global Curdata
global Filenames
global usersvalues
global movies

% checks if movies + transformation structure are loaded
if movies.non_proj_movie_tracked == 0
   Zsl_Int_msgbox = msgbox('Please load movies and transformation structure before starting the analysis', 'Error','error');
   return
end

%% reads user inputs into GUI 
% z slices
usersvalues.Nz=str2double(get(handles.Zslices,'String')); %

%error message
if usersvalues.Nz == 0
   Zsl_Int_msgbox = msgbox('Please enter the number of Z slices recorded per stack before starting the analysis', 'Error','error');
   return
end

% PFS
usersvalues.PFS=get(handles.PFS,'Value');

%"Only 2D" tick box
usersvalues.twoD=get(handles.only2D,'Value');

%"New 3D" tick box
usersvalues.new3D=get(handles.new3D,'Value');

%pixel size
usersvalues.pixxy = str2double(get(handles.pixxy,'String'));

%error message
if usersvalues.pixxy == 0
   Zsl_Int_msgbox = msgbox('Please enter the pixel size before starting the analysis', 'Error','error');
   return
end

%distance between the Z planes
usersvalues.pixZ = str2double(get(handles.Zdist,'String'));

%error message
if usersvalues.pixZ == 0
   Zsl_Int_msgbox = msgbox('Please enter the distance between Z planes before starting the analysis', 'Error','error');
   return
end

%% reads in the time info
if  usersvalues.PFS == 1  
    
    %loads time stamps generated by the PFS 
    usersvalues.dt = timewithpfs(usersvalues.Nz);
    
    %Inputs:
    %usersvalues.Nz: Number of Z planes inserted in the GUI
    
    %Outputs:
    %uservalues.dt: Time stamps [s] passed before each stack/each frame of the projected movie

else
    
    %reads out the interval time entered in the GUI
    usersvalues.dt=str2double(get(handles.inttime,'String'));

end

%error message %THIS DOESN'T WORK
%if usersvalues.dt == 0 && usersvalues.PFS == 0 
 %  Zsl_Int_msgbox = msgbox('Please enter interval time or chose the "With PFS" option before starting the analysis', 'Error','error');
  % return
%end

%% sets info box
set(handles.ResultsPrinter,'String', 'Entire analysis is performed')

%% loads raw track data + depicts name of current track
[track_name,track_path,FilterIndex] = uigetfile('.txt','Select raw tracked data', 'X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Software\Matlab\Tiff reading\Movie_loading\Test movie files');
[ current_track_data, full_track_path, track_name ] = LoadTrack( track_path, track_name );

usersvalues.trackname = track_name;
Filenames.rawtrack = full_track_path; %contains the path to the file containing the raw track
Curdata.frames = current_track_data(:,1);
Curdata.x = current_track_data(:,2); % X positions in pixel coordinates obtained by manual tracking; for Aurelie's movies with single camera for both channels: ranges from 0 to 256
Curdata.y = current_track_data(:,3); % Y positions in pixel coordinates obtained by manual tracking; for Aurelie's movies with single camera for both channels: ranges from 0 to 512

%in the field below "Current track:" the name of the file containing the tracking data is shown 
set(handles.showtracknber,'String',track_name);

%% saves result location
%select folder in which results should be saved 
usersvalues.result_location = uigetdir ('X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Software\Matlab\Tiff reading\Movie_loading\Multitrack analysis','Please select the folder for results');

%% runs XY tracking and plots the results
[Xsub,Ysub] = XYtrackingfit(Curdata.x,Curdata.y,movies.proj_movie_tracked,Curdata.frames(1));
%"Xsub" and "Ysub" are subpixel accurate X and Y coordinates of the track obtained by 2D
%gaussian fitting

if Xsub==0  %if Xsub is 0 this means that analysis has been aborted during XY tracking; needed to stop "TRAC" function
    set(handles.ResultsPrinter,'String', 'Analysis has been canceled during XY tracking') %message is shown in the upper right of the TRAC window
    return %function is left 
end

%saves subpixel accurate XY coordinates of track
Curdata.xsub=Xsub;
Curdata.ysub=Ysub;

%plots subpixel accurate XY coordinates of track in the GUI in "XY tracking with fit"
plot(handles.axes1,Curdata.xsub*usersvalues.pixxy,Curdata.ysub*usersvalues.pixxy); 

%labels the axes of the "XY Tracking with fit"-plot
set(get(handles.axes1,'XLabel'),'String','relative X position (�m)'); 
set(get(handles.axes1,'YLabel'),'String','relative Y position (�m)'); 

%% calculates speed in 2D
%not sure why the following this is done 
if usersvalues.twoD==1
    Curdata.zsub=Curdata.xsub.*0;
    %".*" means pointwise multiplication, each entry in "Curdata.xsub" is multiplied by 0
    %=> "Curdata.zsub" is vector containing a "0" for each frame
    Curdata.z=Curdata.xsub.*0;
    Curdata.speed=Curdata.xsub.*0;
    Curdata.time=(Curdata.frames-Curdata.frames(1)).*usersvalues.dt;
    %"(Curdata.frames-Curdata.frames(1)" is vector containing a number for each frame 
    %(e.g. for 159 tracked frames: numbers from 0 to 158)
    %each of the numbers is multiplied by the interval time between the frames 
    %=> vector containing for each frame the absolute time in seconds since tracking
    %has started (0 for the first frame)

%Run calculation of speed 2D
Curdata.speed= instspeed_2D(Curdata.time,Curdata.xsub,Curdata.ysub,usersvalues.pixxy);

%Inputs:
%Curdata.time: Array with entry for each step of the track containing absolute time since the beginning of the track
%Curdata.xsub: subpixel accurate X coordinates of the track for each frame 
%Curdata.ysub: subpixel accurate Y coordinates of the track for each frame 
%uservalues.pixxy: Pixel size x y entered in the GUI

%Output:
%Curdata.speed: the instantaneous speed for each tracked frame (in �m/s)
end

%% runs Z tracking
if usersvalues.new3D == 1
[Z,Zsub,Index_corrected] = TDtrackingfit(Curdata.xsub,Curdata.ysub,movies.non_proj_movie_tracked,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end), usersvalues.PFS);
%Inputs:
%Curdata.xsub: subpixel x coordinates of track
%Curdata.ysub: subpixel y coordinates of track
%movies.non_proj_movie_tracked: non-projected movie of the tracked channel as a 3D array; 16bits
%usersvalues.Nz: number of Z slices entered in the GUI
%"Curdata.frames(1)": first frame of the tracked projected movie for which tracking was performed 
%"Curdata.frames(end)": last frame of the tracked projected movie for which tracking was performed
%usersvalues.PFS: contains the info if measurements have been done with or without perfect focus system (at Andor SD)

%Outputs:
%Z: Z coordinates of track with plane accuracy; for each stack the plane with the maximum avarage intensity
%Zsub: subplane z coordinates of track/Z coordinates 
%Index_corrected:  contains the frames of the tracked projected movie for which the substack accurate Zpositon was corrected since the fit resulted in a jump between frames of projected movie greater than 2.5 ???

% error message THIS DOESN'T WORK; CHANGE!
%if usersvalues.new3D == 1 && Zsub == 0  %if "New 3D analysis" is ticked and Zsub is 0 this means that analysis has been aborted during Z tracking; needed to stop "TRAC_old" function
    %set(handles.ResultsPrinter,'String', 'Analysis has been canceled during Z tracking') %message is shown in the upper right of the TRAC_old window
    %return %function is left 
%end

%saves z coordinates
Curdata.zsub=Zsub; %saves subplane z coordinates of track 
Curdata.z=Z; %Z coordinates of track with plane accuracy; for each stack the plane with the first maximum avarage intensity

%calculates time points of current track
if usersvalues.PFS == 1
   Curdata.time=(usersvalues.dt(Curdata.frames(1):Curdata.frames(end))-usersvalues.dt(Curdata.frames(1))); %calculates for each frame of the track the time since the beginning of the track   
else
    usersvalues.dt=str2double(get(handles.inttime,'String')); %reads out the interval time entered in the GUI
    Curdata.time=(Curdata.frames-Curdata.frames(1)).*usersvalues.dt; %calculates for each frame of the track the time since the beginning of the track
end

%plots Z coordinates in the "Z Tracking with fit" plot of the GUI and labels the axes
plot(handles.axes2,Curdata.time,Curdata.zsub*usersvalues.pixZ); %plots the substack accurate Z coordinates in �m against the time since the beginning of the track in the window "Z tracking with fit" of the GUI
hold(handles.axes2,'on');plot(handles.axes2,Curdata.time(Index_corrected),Curdata.zsub(Index_corrected),'*r');hold(handles.axes2,'off'); %Z coordinates that have been corrected are marked with a red star in the plot 
set(get(handles.axes2,'XLabel'),'String','relative time (s)');
set(get(handles.axes2,'YLabel'),'String','relative Z position (�m)');

%% calculates speed in 3D
Curdata.speed = instspeed(Curdata.time,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.pixxy,usersvalues.pixZ);
%calculates the instantaneous speed on the basis of the 3D tracking data (xsub, ysub, zsub), the pixel size (pixxy) and the distance between planes of the stack

%Inputs: 
%Curdata.time: Array with entry for each step of the track containing absolute time since the beginning of the track
%Curdata.xsub: subpixel accurate X coordinates of the track for each frame 
%Curdata.ysub: subpixel accurate Y coordinates of the track for each frame 
%Curdata.zsub: Z coordinates with subplane accuracy (results of fit)
%usersvalues.pixxy: Pixel size x y entered in the GUI
%usersvalues.pixZ: Distance between Z slices entered in the GUI

%Outputs:
%Curdata.speed: vector containing the instantaneous velocity considering 3D movement for each frame of the track 
%for first frame Curdata.speed=0

end

%% runs colocalization analysis

% Loads transformation structure
transf = LoadTransfStructure( Filenames.transf );

% 2D
if usersvalues.twoD==1
    [Max,Pos] = CContraj2D_trans(movies.proj_movie_tracked,movies.proj_movie_untracked,Curdata.xsub,Curdata.ysub,Curdata.frames(1),Curdata.frames(end),transf);
%Inputs:
%movies.proj_movie_tracked: 3D array containing the tracked projected movie
%movies.proj_movie_untracked: 3D array containing the untracked projected movie
%Curdata.xsub and Curdata.ysub: matrices containing coordinates with
%subpixel resolution for each frame of the projected movie
%Curdata.frames(1): index of the frame of the projected movie for which tracking was started
%Curdata.frames(end): index of the frame of the projected movie for which tracking
%was ended
%transf: structure containing info about the transformation matrix 

%Outputs:
% Max: array containing the value of the maximum of the cross-correlation between
% each frame of Filename1 and Filname2
% Pos: array containing the x and y coordinates of the maximum of the crosscorrelation for each frame  

% error message if waitbar without parfor/with cancel button is used
if Max == 100000  %if Max is 100000 this means that analysis has been aborted during Z tracking; needed to stop "TRAC" function
    set(handles.ResultsPrinter,'String', 'Analysis has been canceled during 2D cross-correlation colocalization analysis') %message is shown in the upper right of the TRAC window
    return %function is left 
end

% 3D cross-correlation analysis incl. subplane maximum of cross-correlation
% and cross-correlation "background" with randomized second channel
elseif usersvalues.new3D==1
    disp('New 3D cross-correlation analysis chosen')
    [Max,Pos,Maxrand,indoutofZ] = CC3D_trans(movies.non_proj_movie_tracked,movies.non_proj_movie_untracked,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end),transf,usersvalues.PFS);
    %New 3D cross-correlation colocalization" analysis    
        
    %Inputs:
    %movies.non_proj_movie_tracked: 3D array containing the tracked non-projected movie
    %movies.non_proj_movie_untracked: 3D array containing the untracked non-projected movie
    %Curdata.xsub: matrix with subpixel accurate x values of track
    %Curdata.ysub: matrix with subpixel accurate y values of track
    %Curdata.zsub: matrix with subplane accurate z values of track
    %Curdata.Nz: number of planes of each stack
    %Curdata.frames(1): index of the frame of the projected movie for which tracking was started
    %Curdata.frames(end): index of the frame of the projected movie for which tracking
    %was ended
    %transf:structure containing info about the transformation matrix 
    %usersvalues.PFS: contains the info if measurements have been done with or without perfect focus system (at Andor SD)
    
    %Outputs:
    %1. Max is a vector containing the maximum value of the cross-correlation between 
    %the stacks of channel 1 and channel 2 
    %Max contains one value for each stack 

    %2. Pos is a vector containing 
    %A) the subpixel accurate x (Pos1, Pos(1)) and y (Pos2, Pos(2)) coordinates of the
    %maximum of the cross correlation between the tracked and the untracked
    %non-projected movie (done for each stack for the plane containing the
    %maximum => one Pos1 and Pos2 value for each stack)
    %B) the substack accurate Z position (Pos(3)) of the cross-correlation between the tracked and the untracked
    %non-projected movie (one value for each stack)  
    
    %3. Maxrand is a vector containing the maximum value the cross-correlation between 
    %the stacks of channel 1 and the randomized stacks of channel 2 (all pixels of one stack are randomized)  
    %Max contains one value for each stack 

    %indoutofZ: contains the indices of the frames of the projected movie for which the maximum of the cross-correlation could not be fitted in Z

    
    
    %depicts indices of frames of the projected movie for which Z position of maximum could not be fitted in the result printer at the right top of TrIC window
    if isempty(indoutofZ) 
        set(handles.ResultsPrinter,'String','');
    else
        nonfitZ=char();
        for h=1:length(indoutofZ)
        nonfitZ=strcat(nonfitZ, ' ', num2str(indoutofZ(h)), ' ', ',');
        end
    set(handles.ResultsPrinter,'String',horzcat('Z position of CCmax could not be fitted for frame(s):', nonfitZ));
    end
    
    %error message for waitbar without parfor
    if Maxrand == 100000000  %if Maxrand is 100000000 this means that analysis has been aborted during New 3D cross-correlation colocalization analysis; needed to stop "TRAC" function
    set(handles.ResultsPrinter,'String', 'Analysis has been canceled during new 3D cross-correlation colocalization analysis') %message is shown in the upper right of the TRAC window
    return %function is left 
    end
    
    %saves the results
    Curdata.maxccrand=Maxrand; %maximum value of the cross-correlation between the tracked channel and the randomized not tracked channel 
    Curdata.ccposz=Pos(:,3); %substack accurate Z position of cross correlation maximum
else
    %3D cross-correlation analysis without Zsub and Maxrand  %%NOT YET UPDATED TO FULL FRAME
    [Max,Pos] = CContraj3D_trans(Filenames.trackedmovieZstack,Filenames.untrackedmovieZstack,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end),transf,usersvalues.PFS);
   
    %Inputs:
    %Filenames.trackedmovieZstack: path to the tracked non-projected movie
    %Filenames.untrackedmovieZstack: path to the not tracked non-projected movie
    %Curdata.xsub: subpixel accurate X coordinates of the track for each frame 
    %Curdata.ysub: subpixel accurate Y coordinates of the track for each frame 
    %Curdata.zsub: Z coordinates with substack accuracy (results of fit)
    %usersvalues.Nz: number of planes entered in the GUI
    %Curdata.frames(1): index of the frame of the projected movie for which tracking was started
    %Curdata.frames(end): index of the frame of the projected movie for which tracking was ended
    %transf: structure containing info about the transformation matrix
    %usersvalues.PFS: contains the info if measurements have been done with or without perfect focus system (at Andor SD)

    %Outputs:
    %Max: array containing the value of the maximum of the cross-correlation between
    %each frame of Filename1 and Filname2 (TAKING 3D info into
    %account)
    %Pos: array containing the subpixel accurate x and y coordinates of the maximum of the cross 
    %correlation for each frame of the projected movie/for each stack   

    %error message for waitbar without parfor
    if Max == 100000  %if Maxrand is 100000 this means that analysis has been aborted during 3D cross-correlation colocalization analysis; needed to stop "TRAC" function
        set(handles.ResultsPrinter,'String', 'Analysis has been canceled during 3D cross-correlation colocalization analysis') %message is shown in the upper right of the TRAC window
        return %function is left 
    end

end

%saves the results
Curdata.maxcc=Max; %maximum value of the cross-correlation between the tracked channel and the not tracked channel  
Curdata.ccposx=Pos(:,1); %subpixel accurate x coordinates of cross-correlation maximum
Curdata.ccposy=Pos(:,2); %subpixel accurate y coordinates of cross-correlation maximum

% plots the results in the upper "Cross-correlation colocalization analysis" plot
plot(handles.axes3,Curdata.time,Curdata.maxcc,'.');ylim(handles.axes3,[0 (max(Curdata.maxcc)+0.05)]);%Aurelie's original: plot(handles.axes3,Curdata.time,Curdata.maxcc,'.');ylim(handles.axes3,[0 1]); 
%Curdata.maxcc is a vector containing the maximum value the cross-correlation between 
%the stacks of tracked channel and the not tracked channel; contains one value for each stack
%plotted against the time since the beginning of the track (only values
%between 0 and 1 are shown in the graph)

%sets the labels of the axes of the upper "Cross-correlation colocalization analysis" plot 
set(get(handles.axes3,'XLabel'),'String','relative time (s)');
set(get(handles.axes3,'YLabel'),'String', sprintf ('max. value of ICC \n of tracked particle'));

% plots the refind coordinates of the maximum of the cross-correlation in the lower "Cross-correlation colocalization analysis" window 
plot(handles.axes5,Curdata.time,Curdata.ccposx,'.c', Curdata.time,Curdata.ccposy,'.m', Curdata.time,Curdata.ccposz,'.k'); ylim(handles.axes5,[-10 10]);
legend ({'X', 'Y', 'Z'}, 'FontSize', 6, 'Location', 'NorthWest')
%Curdata.ccposx is a vector containing the subpixel accurate x coordinate of the maximum of the the cross-correlation between 
%the stacks of the tracked and untracked channel; contains one value for each stack
%plotted against the time since the beginning of the track (only values between -10 and 10 are shown in the graph)

%sets the labels of the axes of the lower "Cross-correlation colocalization analysis" plot
set(get(handles.axes5,'XLabel'),'String','relative time (s)');
set(get(handles.axes5,'YLabel'),'String', sprintf ('coordinates of max. of ICC \n of tracked particle (pixels)'));

%% calculates and plots background-corrected intensity in both channels
[Int1_max,Int1_cor,Int2_max,Int2_cor] = Intwithcal(Curdata.xsub,Curdata.ysub,movies.proj_movie_tracked,movies.proj_movie_untracked,transf,Curdata.frames(1));

%Inputs:
%Curdata.xsub: matrix with subpixel accurate x values of track
%Curdata.ysub: matrix with subpixel accurate y values of track
%movies.proj_movie_tracked: 3D array containing the tracked projected movie
%movies.proj_movie_untracked: 3D array containing the untracked projected movie
%transf: structure containing info about the transformation matrix 
%Curdata.frames(1): index of the frame of the projected movie for which tracking was started

%Outputs:
%Int1_max: vector containing the intensity value of the pixel with the maximum intensity for each frame of
%the projected movie of channel 1
%Int1_cor: vector containing the background corrected intensity
%of the tracked particle for each frame of channel 1
%Int2_max: vector containing the intensity value of the pixel with the maximum intensity for each frame of
%the projected movie of channel 2
%Int2_cor: vector containing the background corrected intensity
%of the tracked particle for each frame of channel 2

%error message appearing when cancel button of waitbar is pressed
if  Int1_max == 100000  %if Int1_max is 100000 this means that analysis has been aborted during New 3D cross-correlation colocalization analysis; needed to stop "TRAC" function
    set(handles.ResultsPrinter,'String', 'Analysis has been canceled during the measurement of the intensity in the tracked channel') %message is shown in the upper right of the TRAC window
    return %function is left 
end

%error message appearing when cancel button of waitbar is pressed
if  Int2_max == 100000  %if Int2_max is 100000 this means that analysis has been aborted during New 3D cross-correlation colocalization analysis; needed to stop "TRAC" function
    set(handles.ResultsPrinter,'String', 'Analysis has been canceled during the measurement of the intensity in the untracked channel') %message is shown in the upper right of the TRAC window
    return %function is left 
end

%saves the results of the intensity measurements 
usersvalues.trackchannel=get(handles.trackchannel,'Value');
if  usersvalues.trackchannel==1
    Curdata.intGmax=Int1_max';
    Curdata.intGcorrected=Int1_cor(:,1);
    Curdata.intGbackground=Int1_cor(:,2);
    Curdata.intRmax=Int2_max';
    Curdata.intRcorrected=Int2_cor(:,1);
    Curdata.intRbackground=Int2_cor(:,2);
else
    Curdata.intGmax=Int2_max';
    Curdata.intGcorrected=Int2_cor(:,1);
    Curdata.intGbackground=Int2_cor(:,2);
    Curdata.intRmax=Int1_max';
    Curdata.intRcorrected=Int1_cor(:,1);
    Curdata.intRbackground=Int1_cor(:,2);
end

%plots the results in the "Intensity" graph at the lower right of the TrIC
%GUI
plot(handles.axes4,Curdata.time,Curdata.intGcorrected,'+g');
hold(handles.axes4,'on');plot(handles.axes4,Curdata.time,Curdata.intRcorrected,'.r'); hold(handles.axes4,'off');

%sets the labels of the axes of the "Intensity" plot 
set(get(handles.axes4,'XLabel'),'String','relative time (s)');
set(get(handles.axes4,'YLabel'),'String','background-corrected intensity');

%% shuts down parallel pool 
%delete (gcp) %shuts down parallel pool gcp = get current parallel pool

%% saves results of current track

if usersvalues.new3D==1
    %if the values stored in usersvalues.new3D equals 1 (which is the case if 
    %the "New 3D" tick box is ticked) the if-loop is entered; if the box is
    %not ticked the value in "uservalues.new3D" is 0
          
     finaldata=[Curdata.frames Curdata.time Curdata.x Curdata.y Curdata.z ...
                Curdata.xsub Curdata.ysub Curdata.zsub ...
                Curdata.maxcc Curdata.maxccrand Curdata.ccposx Curdata.ccposy Curdata.ccposz ...
                Curdata.intGmax Curdata.intGcorrected Curdata.intGbackground ...
                Curdata.intRmax Curdata.intRcorrected Curdata.intRbackground ...
                Curdata.speed];
 else
    finaldata= [Curdata.frames Curdata.time Curdata.x Curdata.y Curdata.z ...
                Curdata.xsub Curdata.ysub Curdata.zsub ...
                Curdata.maxcc Curdata.ccposx Curdata.ccposy ...
                Curdata.intGmax Curdata.intGcorrected Curdata.intGbackground ...
                Curdata.intRmax Curdata.intRcorrected Curdata.intRbackground ...
                Curdata.speed]; 
                %else contains the same as the if part with the exception of "maxccrand"
                %and ccposz
                %info for 2D analysis

end

                DefaultName = track_name;
                %DefaultName =char(Filenames.rawtrack);
                %creates the variable "DefaultName" and saves the value of
                %"Filenames.rawtrack" (path to file containing results of manual 2D tracking 
                %to it. 

                %supresses the letters _TRAC if the track file was loaded via the open
                %analysis button
                DefaultName = regexprep(DefaultName, '_TRAC', '');

                %supresses the letters .txt (so that they do not appear in the middle of
                %the suggested name)
                DefaultName = regexprep(DefaultName, '.txt', '');

                %adds _TRAC or _TRAC3D after the suggested name
                if  usersvalues.new3D==1         
                    DefaultName = char(strcat(DefaultName,'_TRAC3D'));
                else
                    DefaultName = char(strcat(DefaultName,'_TRAC'));
                end
                
                Data_path=char(strcat(usersvalues.result_location, '\', DefaultName, '.txt'));
                dlmwrite(Data_path,finaldata,'delimiter','\t','precision',6);
                %writes array "finaldata" into an ASCII-format file called as the file 
                %containing the 2D manual tracking followed by "_TRAC3D" or "_TRAC" using tabs to seperate
                %data 

%% completion message

disp('TrIC analysis completed');

%% velocity button
%==========================================================================
%==========================================================================
% --- Executes on button press in velocity.
function velocity_Callback(hObject, eventdata, handles)
% hObject    handle to velocity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%Recalculate the velocity of one or of multiple EXISTING TRAC_old files
%The speed is in �m/s
%Those newly computed speeds are then saved AUTOMATICALLY, which means that
%they REPLACE the former values for speed.

global Curdata
global Filenames
global usersvalues


[RawFileNames,PathName] = uigetfile('*TRAC.txt','Select analysed data, multiple selection possible:','MultiSelect','on');

% %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% If multiple files are selected, Matlab creates a cell string array,
%whereas it creates a simple char array if only one file is selected. The
%following lines are here to solve this.
% %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

%if single file is selected the variable "RawFileNames" is a character array with the dimensions
%"1 x number of characters of file name"

%if multiple files are selected the variable "RawFileNames" is a cell
%string array with the dimensions of 1 x the number of files selected
%(columns)

[nbl,nbc] = size(RawFileNames);
%saves the dimensions of the array "RawFileNames" in the variables of type double "nbl"
%and "nbc" e.g. for the file name "TrackG1_TRAC Ivo - Kopie.txt" nbl=1 and nbc=28
[nbl1,nbc1] = size(RawFileNames(1));
%the variables "nbl1" and "nbc1" are created and the number of rows in the
%array is saved into it; here: 1 (not sure about that"

if ischar(RawFileNames)
    %if "RawFileNames" is a character array the if loop is entered
    StrBuffer='';
    %creates the variable "StrBuffer" of type "char" and saves "''" into it
    SingleFile=1;
    %creates the variable "SingleFile" of type "double" and saves "1" into it
    for m=1:nbc
        StrBuffer=strcat(StrBuffer,RawFileNames(1,m));
        %For-loop goes from 1 to nbc and saves the value of "RawFileNames"
        %into "StrBuffer"; "StrBuffer" contains file name of analysed data
    end
    nbc=1;
    %"nbc" is reset to "1"
    FileNames{1}=StrBuffer;
    %Cell array "FileNames" is created and and in the first row the value
    %of "StrBuffer" is saved
else
    %if "RawFileNames" is a not a character array the else option is entered
    SingleFile=0;
    %creates the variable "SingleFile" of type "double" and saves "0" into it
    FileNames=RawFileNames;
    %in the cell array "FileNames" the value of "RawFileNames" is saved
end

%Set the boundaries for the following loop
[nbl,nbc] = size(FileNames);
%this value is either "1 x 1" (if only single file was selected) or "1 x number of
%selected files" if multiple files were selected

for i = 1:nbc
    A=char(strcat(PathName,FileNames(i)));
    %'strcat' horizontally concatenates string => path incl file name to file to open   
    Filenames.rawtrack=A;
    Filenames.FileNameOnly=char(FileNames(i));
    %only the name of the file saved in 'FileNames' is saved in the
    %variable 'Filenames.FileNameOnly'
    data=load(A);
    %one file after the other saved in "FileNames" is loaded  
    
    
    
    Curdata.frames=data(:,1);
    Curdata.time=data(:,2);
    Curdata.x=data(:,3);
    Curdata.y=data(:,4);
    Curdata.z=data(:,5);
    Curdata.xsub=data(:,6);
    Curdata.ysub=data(:,7);
    Curdata.zsub=data(:,8);
    Curdata.maxcc=data(:,9);
    Curdata.ccposx=data(:,10);
    Curdata.ccposy=data(:,11);
    Curdata.intGmax=data(:,12);
    Curdata.intGcorrected=data(:,13);
    Curdata.intGbackground=data(:,14);
    Curdata.intRmax=data(:,15);
    Curdata.intRcorrected=data(:,16);
    Curdata.intRbackground=data(:,17);
    Curdata.speed=data(:,18);
    %and the data are saved in the variable "Curdata"
    
    usersvalues.twoD = get(handles.only2D,'Value');
    %checks whether the "only 2D" box is ticked
    usersvalues.pixxy = str2double(get(handles.pixxy,'String'));
    %reads out the value entered in the field 'pixel size xy' and saves it as
    %a double precision in the variable uservalues 
    usersvalues.pixZ = str2double(get(handles.Zdist,'String'));
    %reads out the value entered in the field 'distance between Z slices' and saves it as
    %a double precision in the variable uservalues 
    
    if usersvalues.twoD==1
        % usersvalues.dt=str2double(get(handles.inttime,'String'));
        % Curdata.time=(Curdata.frames-Curdata.frames(1)).*usersvalues.dt;
        Curdata.speed= instspeed_2D(Curdata.time,Curdata.xsub,Curdata.ysub,usersvalues.pixxy);
        set(handles.ResultsPrinter,'String',horzcat('2D Velocity measured for track ',Filenames.FileNameOnly));
        % horzcat: horizontally concatenates arrays
        %field 'ResultsPrinter' is a the top left of the GUI, shows the
        %track analysed
    else
        
        Curdata.speed= instspeed(Curdata.time,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.pixxy,usersvalues.pixZ);
        set(handles.ResultsPrinter,'String',horzcat('3D Velocity measured for track ',Filenames.FileNameOnly));
    end
    
    finaldata=[Curdata.frames Curdata.time Curdata.x Curdata.y Curdata.z ...
        Curdata.xsub Curdata.ysub Curdata.zsub ...
        Curdata.maxcc Curdata.ccposx Curdata.ccposy ...
        Curdata.intGmax Curdata.intGcorrected Curdata.intGbackground ...
        Curdata.intRmax Curdata.intRcorrected Curdata.intRbackground ...
        Curdata.speed];

    dlmwrite(A,finaldata,'delimiter','\t','precision',6);
    %writes the matrix 'finaldata' in the file specified under path 'A' 
    %(meaning it overwrites the values saved there before using tabs as delimiters and
    %six significant digits as the precision 
end
%Reduces the pathname to the two last directories
%Finds the '\' in PathName
SlashPosition = strfind(PathName, '\');
%Split the string and keep the part after the second '\' from the right
PathNameShkd=PathName(SlashPosition(length(SlashPosition)-2)+1 :end);
%Supresses '\' at the end of the name
PathNameShkd=PathNameShkd(1 :end-1);

if SingleFile==0
    set(handles.ResultsPrinter,'String',horzcat('New speeds saved for all selected tracks in ',PathNameShkd));
else
    set(handles.ResultsPrinter,'String',horzcat('New speeds saved for this track only: ',Filenames.FileNameOnly));
end
%prints message in the 'ResultsPrinter' field in the GUI
    
    



function pixxy_Callback(hObject, eventdata, handles)
% hObject    handle to pixxy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pixxy as text
%        str2double(get(hObject,'String')) returns contents of pixxy as a double
global usersvalues
usersvalues.pixxy = str2double(get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function pixxy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pixxy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in new3D.
function new3D_Callback(hObject, eventdata, handles)
% hObject    handle to new3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of new3D


% --- Executes on button press in PFS.
function PFS_Callback(hObject, eventdata, handles)
% hObject    handle to PFS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PFS



function planes_Callback(hObject, eventdata, handles)
% hObject    handle to planes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of planes as text
%        str2double(get(hObject,'String')) returns contents of planes as a double


% --- Executes during object creation, after setting all properties.
function planes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to planes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




%% Generation of transformation matrix

% --- Executes on button press in Transfmatrix.
function Transfmatrix_Callback(hObject, eventdata, handles)
% hObject    handle to Transfmatrix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[FileName,PathName,FilterIndex] = uigetfile('.tif','Select disk pattern of fixed channel', 'X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Software\Matlab\Second window');
%[FileName,PathName,FilterIndex] = uigetfile('.txt','Select raw tracked data', 'X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Experiments\Results experiments');
 Pathfixed=char(strcat(PathName,FileName));
 
 [FileName,PathName,FilterIndex] = uigetfile('.tif','Select disk pattern of channel to be shifted', 'X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Software\Matlab\Second window');
%[FileName,PathName,FilterIndex] = uigetfile('.txt','Select raw tracked data', 'X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Experiments\Results experiments');
 Pathshifted=char(strcat(PathName,FileName));
 PathAllDisks=PathName;

 calib4_mass_working(Pathfixed,Pathshifted)
 
 [FileName,PathName,FilterIndex] = uigetfile('.mat','Select transformation matrix', 'X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Software\Matlab\Second window\Results');
%[FileName,PathName,FilterIndex] = uigetfile('.txt','Select raw tracked data', 'X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Experiments\Results experiments');
 Transfpath=char(strcat(PathName,FileName));
 
correct_shift_stack (Pathshifted, Transfpath)

allFiles = dir(PathAllDisks);
allNames = { allFiles.name };
SearchShiftedFile = strfind(allNames,'_shifted');
Index = find(~cellfun(@isempty, SearchShiftedFile));
NameShiftedFile = allNames(Index);
ShiftedFile=char(strcat(PathAllDisks, '\', NameShiftedFile));

I1=imread(Pathfixed);
I2=imread(ShiftedFile);
AlignedMerge = imfuse(I1,I2,'falsecolor','Scaling','joint','ColorChannels',[2 1 0]);
figure('name', 'Merged channels (aligned)');
imshow(AlignedMerge)

%% Export of movie

% --- Executes on button press in movie.
function movie_Callback(hObject, eventdata, handles)
% hObject    handle to movie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global Curdata
global Filenames
global usersvalues
global movies

question = questdlg('Are the corresponding movies and the transformation structure loaded?', 'Movies and tranformation structure','Yes','No','Yes');

switch question 
    case 'No'
        return
    case 'Yes'
end

[FileName,PathName,FilterIndex] = uiputfile('X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Software\Matlab\Tiff reading\Movie_loading\Multitrack analysis\Results_analysis_txt\Movie_track_', 'Chose saving directory and root name for movie', 'X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Software\Matlab\Tiff reading\Movie_loading\Multitrack analysis\Results_analysis_txt\Movie_track_');
 usersvalues.trackname = FileName;
 usersvalues.result_location = PathName;
 
disp ('Export of movie in progress ...'); %this statement is shown in the command window

%give boundaries around the track in pixels 
pixel_boundary = 10;

% implement that the boundary is adjusted if the given one exceeds the
% frame dimension 


%% tracked channel 
disp('... Tracked Channel ...');

%converts the tracked projected movie into RGB (only part (pixel area and frames) for which tracking was performed) as inverted (black on white, contrast adjusted)  RGB stack
RGB_stack_tracked = RGB_converter_stack (movies.proj_movie_tracked, min(Curdata.x)-pixel_boundary, max(Curdata.x)+pixel_boundary, min(Curdata.y)-pixel_boundary, max(Curdata.y)+pixel_boundary, Curdata.frames(1), length(Curdata.frames)); 

%saves the name of the movie file created 
moviename_tracked = strcat(usersvalues.trackname(1:length(usersvalues.trackname)-4), '_tracked_projected_RGB_inverted'); 

%creates a movie in which the track is encircled; last three numbers specify the color of the circle
CreateMovie(RGB_stack_tracked, Curdata.xsub, Curdata.ysub, min(Curdata.x)-pixel_boundary, min(Curdata.y)-pixel_boundary, usersvalues.result_location, moviename_tracked, 255, 0, 0); 


%% untracked channel 
disp('... Untracked Channel ...');

%converts the untracked projected movie into RGB (only part (pixel area and frames) for which tracking was performed) as inverted (black on white, contrast adjusted) RGB stack
RGB_stack_untracked = RGB_converter_stack (movies.proj_movie_untracked, min(Curdata.x)-pixel_boundary, max(Curdata.x)+pixel_boundary, min(Curdata.y)-pixel_boundary, max(Curdata.y)+pixel_boundary, Curdata.frames(1), length(Curdata.frames)); 

%Calculates the coordinates of the track in the UNtracked channel out of the coordinates in the tracked channel
[XsubNotTracked, YsubNotTracked] = CoordinateTransformer (Curdata.xsub, Curdata.ysub, Filenames.transf); 

%saves the name of the movie file created
moviename_untracked = strcat(usersvalues.trackname(1:length(usersvalues.trackname)-4), '_untracked_projected_RGB_inverted'); %Saves the name of the RGB converted inverted avi-movie

%creates a movie in which the track is encircled; last three numbers specify the color of the circle 
CreateMovie(RGB_stack_untracked, XsubNotTracked, YsubNotTracked, min(Curdata.x)-pixel_boundary, min(Curdata.y)-pixel_boundary, usersvalues.result_location, moviename_untracked, 255, 0, 0); 

%% merged channel  
disp('... Merge ...');

%shifts the UNtracked movie to match with the tracked movie
proj_movie_untracked_shifted = correct_shift_stack (movies.proj_movie_untracked, Filenames.transf, Curdata.frames(1), length(Curdata.frames)); 

%merges the tracked and the shifted UNtracked movie
merged_RGB_stack = Merge_RGB_Stacks (movies.proj_movie_tracked, proj_movie_untracked_shifted, min(Curdata.x)-pixel_boundary, max(Curdata.x)+pixel_boundary, min(Curdata.y)-pixel_boundary, max(Curdata.y)+pixel_boundary, Curdata.frames(1), length(Curdata.frames)); 

%saves the name of the movie file created
moviename_merged_movie = strcat(usersvalues.trackname(1:length(usersvalues.trackname)-4), '_merge_XY_shift_corrected'); 

%creates a movie in which the track is encircled; last three numbers specify the color of the circle
CreateMovie(merged_RGB_stack, Curdata.xsub, Curdata.ysub, min(Curdata.x)-pixel_boundary, min(Curdata.y)-pixel_boundary, usersvalues.result_location, moviename_merged_movie, 255, 255, 255);

%% completion message
disp('Movies saved');
    
%% Multitrack analysis

% --- Executes on button press in multitrack.
function multitrack_Callback(hObject, eventdata, handles)
% hObject    handle to multitrack (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% starting message 
disp('Multitrack analysis started');

%% starts parallel pool if non is running
p = gcp;

%% loads global variables
global Curdata
global Filenames
global usersvalues
global movies

%% checks if movies + transformation structure are loaded
if movies.non_proj_movie_tracked==0
   Zsl_Int_msgbox = msgbox('Please load movies and transformation structure before starting the analysis', 'Error','error');
   return
end

%% reads user inputs into GUI 
% z slices
usersvalues.Nz=str2double(get(handles.Zslices,'String')); 
% error message
if usersvalues.Nz==0
   Zsl_Int_msgbox=msgbox('Please enter the number of Z slices before starting the analysis', 'Error','error');
   return
end

% PFS
usersvalues.PFS=get(handles.PFS,'Value');

%"Only 2D" tick box
usersvalues.twoD=get(handles.only2D,'Value');

%"New 3D" tick box
usersvalues.new3D=get(handles.new3D,'Value');

%pixel size
usersvalues.pixxy=str2double(get(handles.pixxy,'String'));
%error message
if usersvalues.pixxy==0
   Zsl_Int_msgbox=msgbox('Please enter the pixel size before starting the analysis', 'Error','error');
   return
end

%distance between the Z planes
usersvalues.pixZ=str2double(get(handles.Zdist,'String'));
%error message
if usersvalues.pixZ==0
   Zsl_Int_msgbox=msgbox('Please enter the distance between z planes before starting the analysis', 'Error','error');
   return
end

%% reads in the time info
if  usersvalues.PFS == 1  
    
    %loads time stamps generated by the PFS 
    usersvalues.dt = timewithpfs(usersvalues.Nz);
    
    %Inputs:
    %usersvalues.Nz: Number of Z planes inserted in the GUI
    
    %Outputs:
    %uservalues.dt: Time stamps [s] passed before each stack/each frame of the projected movie

else
    
    %reads out the interval time entered in the GUI
    usersvalues.dt=str2double(get(handles.inttime,'String'));

end

%error message %THIS DOESN'T WORK
%if usersvalues.dt == 0 && usersvalues.PFS == 0 
 %  Zsl_Int_msgbox = msgbox('Please enter interval time or chose the "With PFS" option before starting the analysis', 'Error','error');
  % return
%end

%% reads in the tracks
%gets path to multitrack file generated by TrackMate
[multitrack_file_name,multitrack_file_path,FilterIndex] = uigetfile('.xml','Select multitrack file generated by TrackMate', 'X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Software\TrackMate');
multitrack_file=char(strcat(multitrack_file_path,multitrack_file_name));

%returns all tracks in a cell array of cells, one cell per track;  
[tracks]=load_multitrack_file(multitrack_file);  

%% saves result location
%select folder in which results should be saved 
usersvalues.result_location=uigetdir ('X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Software\Matlab\Tiff reading\Movie_loading\Multitrack analysis','Please select the folder for results');

%% START analysis loop
number_of_tracks = int2str(length(tracks));
for i = 1:length(tracks)
    current_track_data = tracks{i,1};
    
    %% reads in current track + depicts name of current track
    track_name=int2str(i);
    %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    %+1 as indexing in ImageJ starts with 0
    %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    Curdata.frames=current_track_data(:,1)+1;
    %Curdata.frames=current_track_data(:,1);
    Curdata.x=current_track_data(:,2)+1; % X positions in pixel coordinates obtained by manual tracking; for Aurelie's movies with single camera for both channels: ranges from 0 to 256
    %Curdata.x=current_track_data(:,2);
    Curdata.y=current_track_data(:,3)+1; % Y positions in pixel coordinates obtained by manual tracking; for Aurelie's movies with single camera for both channels: ranges from 0 to 512
    %Curdata.y=current_track_data(:,3);
    
    %in the field below "Current track:" the name of the file containing the tracking data is shown 
    %show_track_name = sprintf ('Track ', track_name,' out of ', number_of_tracks, ' tracks is analysed');
    show_track_name = char(strcat('Track ', track_name,' out of ', number_of_tracks, ' tracks is analysed'));
    set(handles.showtracknber,'String',show_track_name);
    pause(1);
    
    %% calculates time points of current track
    if usersvalues.PFS==1
       Curdata.time=(usersvalues.dt(Curdata.frames(1):Curdata.frames(end))-usersvalues.dt(Curdata.frames(1))); %calculates for each frame of the track the time since the beginning of the track   
    else
       usersvalues.dt=str2double(get(handles.inttime,'String')); %reads out the interval time entered in the GUI
       Curdata.time=(Curdata.frames-Curdata.frames(1)).*usersvalues.dt; %calculates for each frame of the track the time since the beginning of the track
    end
    
    %% runs XY tracking and plots the results
    [Xsub,Ysub]=XYtrackingfit(Curdata.x,Curdata.y,movies.proj_movie_tracked,Curdata.frames(1));
    %"Xsub" and "Ysub" are subpixel accurate X and Y coordinates of the track obtained by 2D
    %gaussian fitting

    if Xsub==0  %if Xsub is 0 this means that analysis has been aborted during XY tracking; needed to stop "TRAC" function
       set(handles.ResultsPrinter,'String', 'Analysis has been canceled during XY tracking') %message is shown in the upper right of the TRAC window
       return %function is left 
    end

    %saves subpixel accurate XY coordinates of track
    Curdata.xsub=Xsub;
    Curdata.ysub=Ysub;

    %plots subpixel accurate XY coordinates of track in the GUI in "XY tracking with fit"
    plot(handles.axes1,Curdata.xsub*usersvalues.pixxy,Curdata.ysub*usersvalues.pixxy); 

    %labels the axes of the "XY Tracking with fit"-plot
    set(get(handles.axes1,'XLabel'),'String','relative X position (�m)'); 
    set(get(handles.axes1,'YLabel'),'String','relative Y position (�m)'); 
    
    %% calculates speed in 2D
    %not sure why the following this is done 
    if usersvalues.twoD==1
       Curdata.zsub=Curdata.xsub.*0;
       %".*" means pointwise multiplication, each entry in "Curdata.xsub" is multiplied by 0
       %=> "Curdata.zsub" is vector containing a "0" for each frame
        Curdata.z=Curdata.xsub.*0;
        Curdata.speed=Curdata.xsub.*0;
     %weired stuff end   
      
      %Run calculation of speed 2D
        Curdata.speed= instspeed_2D(Curdata.time,Curdata.xsub,Curdata.ysub,usersvalues.pixxy);

        %Inputs:
        %Curdata.time: Array with entry for each step of the track containing absolute time since the beginning of the track
        %Curdata.xsub: subpixel accurate X coordinates of the track for each frame 
        %Curdata.ysub: subpixel accurate Y coordinates of the track for each frame 
        %uservalues.pixxy: Pixel size x y entered in the GUI

        %Output:
        %Curdata.speed: the instantaneous speed for each tracked frame (in �m/s)
    end

    %% runs Z tracking
    if usersvalues.new3D==1
        [Z,Zsub,Index_corrected] = TDtrackingfit(Curdata.xsub,Curdata.ysub,movies.non_proj_movie_tracked,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end), usersvalues.PFS);
        %Inputs:
        %Curdata.xsub: subpixel x coordinates of track
        %Curdata.ysub: subpixel y coordinates of track
        %movies.non_proj_movie_tracked: non-projected movie of the tracked channel as a 3D array; 16bits
        %usersvalues.Nz: number of Z slices entered in the GUI
        %"Curdata.frames(1)": first frame of the tracked projected movie for which tracking was performed 
        %"Curdata.frames(end)": last frame of the tracked projected movie for which tracking was performed
        %usersvalues.PFS: contains the info if measurements have been done with or without perfect focus system (at Andor SD)

        %Outputs:
        %Z: Z coordinates of track with plane accuracy; for each stack the plane with the maximum avarage intensity
        %Zsub: subplane z coordinates of track/Z coordinates 
        %Index_corrected:  contains the frames of the tracked projected movie for which the substack accurate Zpositon was corrected since the fit resulted in a jump between frames of projected movie greater than 2.5 ???

        % error message THIS DOESN'T WORK; CHANGE!
        %if usersvalues.new3D == 1 && Zsub == 0  %if "New 3D analysis" is ticked and Zsub is 0 this means that analysis has been aborted during Z tracking; needed to stop "TRAC_old" function
            %set(handles.ResultsPrinter,'String', 'Analysis has been canceled during Z tracking') %message is shown in the upper right of the TRAC_old window
            %return %function is left 
        %end

        %saves z coordinates
        Curdata.zsub=Zsub; %saves subplane z coordinates of track 
        Curdata.z=Z; %Z coordinates of track with plane accuracy; for each stack the plane with the first maximum avarage intensity        

        %plots Z coordinates in the "Z Tracking with fit" plot of the GUI and labels the axes
        plot(handles.axes2,Curdata.time,Curdata.zsub*usersvalues.pixZ); %plots the substack accurate Z coordinates in �m against the time since the beginning of the track in the window "Z tracking with fit" of the GUI
        hold(handles.axes2,'on');plot(handles.axes2,Curdata.time(Index_corrected),Curdata.zsub(Index_corrected),'*r');hold(handles.axes2,'off'); %Z coordinates that have been corrected are marked with a red star in the plot 
        set(get(handles.axes2,'XLabel'),'String','relative time (s)');
        set(get(handles.axes2,'YLabel'),'String','relative Z position (�m)');

    %% calculates speed in 3D
        Curdata.speed = instspeed(Curdata.time,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.pixxy,usersvalues.pixZ);
        %calculates the instantaneous speed on the basis of the 3D tracking data (xsub, ysub, zsub), the pixel size (pixxy) and the distance between planes of the stack

        %Inputs: 
        %Curdata.time: Array with entry for each step of the track containing absolute time since the beginning of the track
        %Curdata.xsub: subpixel accurate X coordinates of the track for each frame 
        %Curdata.ysub: subpixel accurate Y coordinates of the track for each frame 
        %Curdata.zsub: Z coordinates with subplane accuracy (results of fit)
        %usersvalues.pixxy: Pixel size x y entered in the GUI
        %usersvalues.pixZ: Distance between Z slices entered in the GUI

        %Outputs:
        %Curdata.speed: vector containing the instantaneous velocity considering 3D movement for each frame of the track 
        %for first frame Curdata.speed=0

    end
    
    %% runs colocalization analysis

    % Loads transformation structure
    transf = LoadTransfStructure( Filenames.transf );

    % 2D
    if usersvalues.twoD==1
        [Max,Pos] = CContraj2D_trans(movies.proj_movie_tracked,movies.proj_movie_untracked,Curdata.xsub,Curdata.ysub,Curdata.frames(1),Curdata.frames(end),transf);
        %Inputs:
        %movies.proj_movie_tracked: 3D array containing the tracked projected movie
        %movies.proj_movie_untracked: 3D array containing the untracked projected movie
        %Curdata.xsub and Curdata.ysub: matrices containing coordinates with
        %subpixel resolution for each frame of the projected movie
        %Curdata.frames(1): index of the frame of the projected movie for which tracking was started
        %Curdata.frames(end): index of the frame of the projected movie for which tracking
        %was ended
        %transf: structure containing info about the transformation matrix 

        %Outputs:
        %Max: array containing the value of the maximum of the cross-correlation between
        %each frame of Filename1 and Filname2
        %Pos: array containing the x and y coordinates of the maximum of the crosscorrelation for each frame  

    % error message if waitbar without parfor/with cancel button is used
    if Max == 100000  %if Max is 100000 this means that analysis has been aborted during Z tracking; needed to stop "TRAC" function
        set(handles.ResultsPrinter,'String', 'Analysis has been canceled during 2D cross-correlation colocalization analysis') %message is shown in the upper right of the TRAC window
        return %function is left 
    end

    % 3D cross-correlation analysis incl. subplane maximum of cross-correlation
    % and cross-correlation "background" with randomized second channel
    elseif usersvalues.new3D==1
        %New 3D cross-correlation colocalization" analysis
        disp('New 3D cross-correlation analysis chosen')
        [Max,Pos,Maxrand,indoutofZ] = CC3D_trans(movies.non_proj_movie_tracked,movies.non_proj_movie_untracked,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end),transf,usersvalues.PFS);
               
        %Inputs:
        %movies.non_proj_movie_tracked: 3D array containing the tracked non-projected movie
        %movies.non_proj_movie_untracked: 3D array containing the untracked non-projected movie
        %Curdata.xsub: matrix with subpixel accurate x values of track
        %Curdata.ysub: matrix with subpixel accurate y values of track
        %Curdata.zsub: matrix with subplane accurate z values of track
        %Curdata.Nz: number of planes of each stack
        %Curdata.frames(1): index of the frame of the projected movie for which tracking was started
        %Curdata.frames(end): index of the frame of the projected movie for which tracking
        %was ended
        %transf:structure containing info about the transformation matrix 
        %usersvalues.PFS: contains the info if measurements have been done with or without perfect focus system (at Andor SD)
    
        %Outputs:
        %1. Max is a vector containing the maximum value of the cross-correlation between 
        %the stacks of channel 1 and channel 2 
        %Max contains one value for each stack 

        %2. Pos is a vector containing 
        %A) the subpixel accurate x (Pos1, Pos(1)) and y (Pos2, Pos(2)) coordinates of the
        %maximum of the cross correlation between the tracked and the untracked
        %non-projected movie (done for each stack for the plane containing the
        %maximum => one Pos1 and Pos2 value for each stack)
        %B) the substack accurate Z position (Pos(3)) of the cross-correlation between the tracked and the untracked
        %non-projected movie (one value for each stack)  
    
        %3. Maxrand is a vector containing the maximum value the cross-correlation between 
        %the stacks of channel 1 and the randomized stacks of channel 2 (all pixels of one stack are randomized)  
        %Max contains one value for each stack 

        %indoutofZ: contains the indices of the frames of the projected movie for which the maximum of the cross-correlation could not be fitted in Z

        %depicts indices of frames of the projected movie for which Z position of maximum could not be fitted in the result printer at the right top of TrIC window
        if isempty(indoutofZ) 
            set(handles.ResultsPrinter,'String','');
        else
            nonfitZ=char();
        for h=1:length(indoutofZ)
            nonfitZ=strcat(nonfitZ, ' ', num2str(indoutofZ(h)), ' ', ',');
        end
            set(handles.ResultsPrinter,'String',horzcat('Z position of CCmax could not be fitted for frame(s):', nonfitZ));
        end
    
        %error message for waitbar without parfor
        if Maxrand == 100000000  %if Maxrand is 100000000 this means that analysis has been aborted during New 3D cross-correlation colocalization analysis; needed to stop "TRAC" function
            set(handles.ResultsPrinter,'String', 'Analysis has been canceled during new 3D cross-correlation colocalization analysis') %message is shown in the upper right of the TRAC window
            return %function is left 
        end
    
        %saves the results
        Curdata.maxccrand=Maxrand; %maximum value of the cross-correlation between the tracked channel and the randomized not tracked channel 
        Curdata.ccposz=Pos(:,3); %substack accurate Z position of cross correlation maximum
    else
        %3D cross-correlation analysis without Zsub and Maxrand  %%NOT YET UPDATED TO FULL FRAME
        [Max,Pos] = CContraj3D_trans(Filenames.trackedmovieZstack,Filenames.untrackedmovieZstack,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end),transf,usersvalues.PFS);
   
        %Inputs:
        %Filenames.trackedmovieZstack: path to the tracked non-projected movie
        %Filenames.untrackedmovieZstack: path to the not tracked non-projected movie
        %Curdata.xsub: subpixel accurate X coordinates of the track for each frame 
        %Curdata.ysub: subpixel accurate Y coordinates of the track for each frame 
        %Curdata.zsub: Z coordinates with substack accuracy (results of fit)
        %usersvalues.Nz: number of planes entered in the GUI
        %Curdata.frames(1): index of the frame of the projected movie for which tracking was started
        %Curdata.frames(end): index of the frame of the projected movie for which tracking was ended
        %transf: structure containing info about the transformation matrix
        %usersvalues.PFS: contains the info if measurements have been done with or without perfect focus system (at Andor SD)

        %Outputs:
        %Max: array containing the value of the maximum of the cross-correlation between
        %each frame of Filename1 and Filname2 (TAKING 3D info into
        %account)
        %Pos: array containing the subpixel accurate x and y coordinates of the maximum of the cross 
        %correlation for each frame of the projected movie/for each stack   

        %error message for waitbar without parfor
        if Max == 100000  %if Maxrand is 100000 this means that analysis has been aborted during 3D cross-correlation colocalization analysis; needed to stop "TRAC" function
            set(handles.ResultsPrinter,'String', 'Analysis has been canceled during 3D cross-correlation colocalization analysis') %message is shown in the upper right of the TRAC window
            return %function is left 
        end
    end

    %saves the results
    Curdata.maxcc=Max; %maximum value of the cross-correlation between the tracked channel and the not tracked channel  
    Curdata.ccposx=Pos(:,1); %subpixel accurate x coordinates of cross-correlation maximum
    Curdata.ccposy=Pos(:,2); %subpixel accurate y coordinates of cross-correlation maximum

    %plots the results in the upper "Cross-correlation colocalization analysis" plot
    plot(handles.axes3,Curdata.time,Curdata.maxcc,'.');ylim(handles.axes3,[0 (max(Curdata.maxcc)+0.05)]);%Aurelie's original: plot(handles.axes3,Curdata.time,Curdata.maxcc,'.');ylim(handles.axes3,[0 1]); 
    %Curdata.maxcc is a vector containing the maximum value the cross-correlation between 
    %the stacks of tracked channel and the not tracked channel; contains one value for each stack
    %plotted against the time since the beginning of the track (only values
    %between 0 and 1 are shown in the graph)

    %sets the labels of the axes of the upper "Cross-correlation colocalization analysis" plot 
    set(get(handles.axes3,'XLabel'),'String','relative time (s)');
    set(get(handles.axes3,'YLabel'),'String', sprintf ('max. value of ICC \n of tracked particle'));

    %plots the refind coordinates of the maximum of the cross-correlation in the lower "Cross-correlation colocalization analysis" window 
    plot(handles.axes5,Curdata.time,Curdata.ccposx,'.c', Curdata.time,Curdata.ccposy,'.m', Curdata.time,Curdata.ccposz,'.k'); ylim(handles.axes5,[-10 10]);
    legend ({'X', 'Y', 'Z'}, 'FontSize', 6, 'Location', 'NorthWest')
    %Curdata.ccposx is a vector containing the subpixel accurate x coordinate of the maximum of the the cross-correlation between 
    %the stacks of the tracked and untracked channel; contains one value for each stack
    %plotted against the time since the beginning of the track (only values between -10 and 10 are shown in the graph)

    %sets the labels of the axes of the lower "Cross-correlation colocalization analysis" plot
    set(get(handles.axes5,'XLabel'),'String','relative time (s)');
    set(get(handles.axes5,'YLabel'),'String', sprintf ('coordinates of max. of ICC \n of tracked particle (pixels)'));

    %% calculates and plots background-corrected intensity in both channels
    [Int1_max,Int1_cor,Int2_max,Int2_cor]=Intwithcal(Curdata.xsub,Curdata.ysub,movies.proj_movie_tracked,movies.proj_movie_untracked,transf,Curdata.frames(1));

    %Inputs:
    %Curdata.xsub: matrix with subpixel accurate x values of track
    %Curdata.ysub: matrix with subpixel accurate y values of track
    %movies.proj_movie_tracked: 3D array containing the tracked projected movie
    %movies.proj_movie_untracked: 3D array containing the untracked projected movie
    %transf: structure containing info about the transformation matrix 
    %Curdata.frames(1): index of the frame of the projected movie for which tracking was started

    %Outputs:
    %Int1_max: vector containing the intensity value of the pixel with the maximum intensity for each frame of
    %the projected movie of channel 1
    %Int1_cor: vector containing the background corrected intensity
    %of the tracked particle for each frame of channel 1
    %Int2_max: vector containing the intensity value of the pixel with the maximum intensity for each frame of
    %the projected movie of channel 2
    %Int2_cor: vector containing the background corrected intensity
    %of the tracked particle for each frame of channel 2

    %error message appearing when cancel button of waitbar is pressed
    if  Int1_max == 100000  %if Int1_max is 100000 this means that analysis has been aborted during New 3D cross-correlation colocalization analysis; needed to stop "TRAC" function
        set(handles.ResultsPrinter,'String', 'Analysis has been canceled during the measurement of the intensity in the tracked channel') %message is shown in the upper right of the TRAC window
        return %function is left 
    end

    %error message appearing when cancel button of waitbar is pressed
    if  Int2_max == 100000  %if Int2_max is 100000 this means that analysis has been aborted during New 3D cross-correlation colocalization analysis; needed to stop "TRAC" function
        set(handles.ResultsPrinter,'String', 'Analysis has been canceled during the measurement of the intensity in the untracked channel') %message is shown in the upper right of the TRAC window
        return %function is left 
    end

    %saves the results of the intensity measurements 
    usersvalues.trackchannel=get(handles.trackchannel,'Value');
    if usersvalues.trackchannel==1
        Curdata.intGmax=Int1_max';
        Curdata.intGcorrected=Int1_cor(:,1);
        Curdata.intGbackground=Int1_cor(:,2);
        Curdata.intRmax=Int2_max';
        Curdata.intRcorrected=Int2_cor(:,1);
        Curdata.intRbackground=Int2_cor(:,2);
    else
        Curdata.intGmax=Int2_max';
        Curdata.intGcorrected=Int2_cor(:,1);
        Curdata.intGbackground=Int2_cor(:,2);
        Curdata.intRmax=Int1_max';
        Curdata.intRcorrected=Int1_cor(:,1);
        Curdata.intRbackground=Int1_cor(:,2);
    end

    %plots the results in the "Intensity" graph at the lower right of the TrIC
    %GUI
    plot(handles.axes4,Curdata.time,Curdata.intGcorrected,'+g');
    hold(handles.axes4,'on');plot(handles.axes4,Curdata.time,Curdata.intRcorrected,'.r'); hold(handles.axes4,'off');

    %sets the labels of the axes of the "Intensity" plot 
    set(get(handles.axes4,'XLabel'),'String','relative time (s)');
    set(get(handles.axes4,'YLabel'),'String','background-corrected intensity');

    %% shuts down parallel pool 
    %delete (gcp) %shuts down parallel pool gcp = get current parallel pool
  
    %% writes results into text file 
                
 if usersvalues.new3D==1
    %if the values stored in usersvalues.new3D equals 1 (which is the case if 
    %the "New 3D" tick box is ticked) the if-loop is entered; if the box is
    %not ticked the value in "uservalues.new3D" is 0
          
     finaldata=[Curdata.frames Curdata.time Curdata.x Curdata.y Curdata.z ...
                Curdata.xsub Curdata.ysub Curdata.zsub ...
                Curdata.maxcc Curdata.maxccrand Curdata.ccposx Curdata.ccposy Curdata.ccposz ...
                Curdata.intGmax Curdata.intGcorrected Curdata.intGbackground ...
                Curdata.intRmax Curdata.intRcorrected Curdata.intRbackground ...
                Curdata.speed];
 else
    finaldata= [Curdata.frames Curdata.time Curdata.x Curdata.y Curdata.z ...
                Curdata.xsub Curdata.ysub Curdata.zsub ...
                Curdata.maxcc Curdata.ccposx Curdata.ccposy ...
                Curdata.intGmax Curdata.intGcorrected Curdata.intGbackground ...
                Curdata.intRmax Curdata.intRcorrected Curdata.intRbackground ...
                Curdata.speed]; 
                %else contains the same as the if part with the exception of "maxccrand"
                %and ccposz
                %info for 2D analysis

 end
                
    %% saves results in text file    

    Results_file_name=char(strcat('Results_Track_', track_name, '.txt')); %contains the name of the file containing the results of the current track
    Data_path = char(strcat(usersvalues.result_location, '\', Results_file_name)); %total path for saving the results file of the current track         
    dlmwrite(Data_path,finaldata,'delimiter','\t','precision',6);

    %Activate if the result of each track should be saved in a
    %seperate folder
    %Results_file_name=char(strcat('Results_Track_', track_name, '.txt')); %contains the name of the file containing the results of the current track
    %Results_folder_name=char(strcat('Results_Track_', track_name)); %contains the name of the folder in which the results will be saved
    %Parent_data_path=char(strcat(usersvalues.result_location, '\')); %path to the folder containing the result folders
    %mkdir ([Parent_data_path Results_folder_name]) %creates the folder in which the results will be saved 
    %Data_path = char(strcat(usersvalues.result_location, '\',Results_folder_name, '\', Results_file_name)); %total path for saving the results file of the current track
    %dlmwrite(Data_path,finaldata,'delimiter','\t','precision',6);
    %writes array "finaldata" into an ASCII-format file called as the file 
    %containing the 2D manual tracking followed by "_TRAC3D" or "_TRAC" using tabs to seperate
    %data 
    
end    

full_RGB_movie = RGB_converter_stack (movies.proj_movie_tracked, 1, 512, 1, 512, 1, length (movies.proj_movie_tracked(1,1,:)));

Encircle_all_tracks_incl_numbers(full_RGB_movie,tracks,usersvalues.result_location, 'Movie_all_tracks', 255, 0, 0);
%% completion message

disp('Multitrack analysis completed');    


% --- Executes on button press in twoD.
function twoD_Callback(hObject, eventdata, handles)
% hObject    handle to twoD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of twoD


% --- Executes on button press in new3D.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to new3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of new3D


% --- Executes on button press in old3D.
function old3D_Callback(hObject, eventdata, handles)
% hObject    handle to old3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of old3D


% --- Executes on button press in XYcorrection.
function XYcorrection_Callback(hObject, eventdata, handles)
% hObject    handle to XYcorrection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of XYcorrection
