function [Z,Zsub,Index_corrected] = TDtrackingfit(xpix,ypix,non_proj_movie_tracked,Nz,frame_init,frame_end,PFS_status)
% Performs 3D tracking from 2D tracking of Z-projection (max. intensity)

% Inputs:
% xpix: subpixel x coordinates of track
% ypix: subpixel y coordinates of track
% non_proj_movie_tracked: non-projected movie of the tracked channel as a 3D array; 16bits
% Nz is the number of Z slices per stack recorded at the Andor SD/entered in the TrIC GUI
% frame_init: first frame of the projected movie for which 2D tracking was performed; cannot be 0
% frame_end: last frame of the projected movie for which 2D tracking was performed
% PFS_status: contains the info if measurements have been done with or without perfect focus system (at Andor SD)

% Outputs:
% Z: Z coordinates of track with plane accuracy; for each stack the plane with the maximum avarage intensity
% Zsub: substack z coordinates of track/Z coordinates; result of the fit along Z: subplane resolution
% Index_corrected:  contains the frames of the tracked projected movie for which the substack accurate Zpositon was corrected since the fit resulted in a jump between frames of projected movie greater than 2.5 ???
% Aurelie Dupont

%close all;
% Some more detailed description of what is done:
% Z: the index of the first maximum in the vector containing the mean value of the
    %pixel intensity within the window around the xy values identified by 2D
    %manual tracking of each frame of the tracked non-projected movie
    
    %for each stack the index of the plane with the maximum avarage
    %intensity within the window around the xy values identified by 2D
    %manual tracking
    %=> Z contains a value for each frame of the projected movie/one value
    %for each z stack
    %Z coordinates of track with stack accuracy
% Zsub: the Z coordinates of the track with substack accuracy/results from Z
    %fitting
% Index_corrected: NOT SURE ABOUT THAT

%info = imfinfo(Filename);
%"info" is a structure with 37 fields each containing certain info about the file containing the tracked non-projected movie

W = length(non_proj_movie_tracked(1,:,1));
%W = info.Width;
H = length(non_proj_movie_tracked(:,1,1));
%H = info.Height;
L = frame_end - frame_init +1; %"L" contains the number of frames of the projected movie for which tracking was performed

dpix=3; % Aurelie's comment: 4 is good
ErrorDpix=2.5; %Aurelie's comment: in pixels, adjust to correct Z track

%Aurelie's comments:
%stack = uint16(zeros(2*dpix+1,2*dpix+1,Nz));
%%"uint16" converts into unsigned integers of type "uint16" 
%%"zeros(n,m,x)" returns an n by m by x matrix of zeros (as dpix=3 and N its a
%%7x7xthe number of z slices matrix of zeros in this case) "stack" contains one 7x7 matrix for each frame; not sure why "uint16" conversion is necessary here 

xpix=round(xpix);ypix=round(ypix);
%rounds x and y values of manual tracking to the nearest integer

if PFS_status == 1
   Nz = Nz-1; %if measurements have been performed with the perfect focus system at the Andor SD the first plane of every stack has been removed during image readin
end

%Aurelie's comments:
% dp=4;
% dp_peak=4;
% circle_inner=abs(1-circle_d(dp_peak,2*dp+1));%mask

disp('');
disp('3D Tracking ongoing');

Int0 = double (zeros(1,Nz)); %creates a matrix with 1 row and Nz columns filling it with zeros
Z = double (zeros(1,L));
i0 = double (zeros(1,1));
ii = double (zeros(1,1));
Zsub = double (zeros(1,L));

p = ProgressBar(L);
%parfor i = 1:L
for i= 1:L
    %"L" contains the number of tracked projected frames; for-loop runs through all frames of the tracked projected movie 
    PixRegx=[xpix(i)-dpix,xpix(i)+dpix];
    %creates an array of type double with 2 elements: 
    %1. x coordinate of frame i - dpix, 
    %2. x coordinate of frame i + dpix
    %specifies a region around the pixel indentified by manual tracking 
    PixRegy=[ypix(i)-dpix,ypix(i)+dpix];
    %does the same for y 
    
    stack = uint16(zeros(2*dpix+1,2*dpix+1,Nz));
    %"uint16" converts into unsigned integers of type "uint16" 
    %"zeros(n,m,x)" returns an n by m by x matrix of zeros (as dpix=3 its a
    %7x7xthe number of z slices matrix of zeros in this case) "stack" contains one 7x7 matrix for each frame; not sure why "uint16" conversion is necessary here  
    Int = double(zeros(1,1));
    for frame=1:Nz
        %"Nz" contains the number of z slices for each frame; for-loop runs
        %through all z slices for each z stack for which tracking was
        %performed
        if (xpix(i)<=dpix) | (ypix(i)<=dpix) | (xpix(i)>W-dpix) | (ypix(i)>H-dpix)
            stack(:,:,frame)=0;
            %pipe "|" means "OR"
            %if the particle is closer to the edge of the image than half the
            %fitting window in x and y no fitting can be performed and the 7x7
            %matrix is filled with zeros for these positions
        else
            stack(:,:,frame) = non_proj_movie_tracked(PixRegy(1):PixRegy(2), PixRegx(1):PixRegx(2),(frame_init+i-2).*Nz+frame);
            %stack(:,:,frame) = imread(Filename, (frame_init+i-2).*Nz+frame,'PixelRegion',{PixRegy,PixRegx});
            %"imread" reads in the color values of the pixels of the tracked non-projected movie
            %the TIFF-file loaded has a pixel depth of 16-bit and is in grey
            %scale. So the color of each pixel is assigned with a certain
            %number ranging from 0 (black) to 65 535 (white).
            %these values are written into the variable "stack" which is an
            %array of size 7x7xthe number of z slices
       
            %non_proj_movie_tracked contains the tracked non-projected movie 
            %"frame_init+i-2" goes through the numbers of the frames of the
            %tracked projected movie (-2 needed to find the correct frame
            %of the tracked non-projected movie) use example to understand 
            %"(frame_init+i-2).*Nz+frame" goes through the frames of the tracked non-projected movie
        
            %"reads in only a region of each frame of the tracked non-projected movie; region is
            %specified by the boundaries set in "PixRegy" and "PixRegx" => 7x7 region around 
            %the pixel identified by manual tracking
        end
        %Aurelie's comments:
        %stack(:,:,frame) = imread(Filename,(frame_init+i-2).*Nz+frame);
        %stackb=zoomf(stack,xpix(i),ypix(i),dpix,frame);
        
        Int(frame) = mean(mean(stack(:,:,frame)));
        %1. mean of all columns => single value for all columns 
        %2. mean of all these values => single value per traked plane/frame of
        %the tracked non-projected movie
        
        %Aurelie's comments:
        %         Measuring intensity in a disk mask, cf Gregor Heiss
        %         Intc(frame) = mean(mean(stackb.*circle_inner));
        
    end
    %Aurelie's comments:
    %figure;plot(Int,'.-');pause;
    %hold on;plot(Intc,'.-r');hold off;pause
    %         stackb=stack(ypix(i)-dpix:ypix(i)+dpix,xpix(i)-dpix:xpix(i)+dpix,:);
    %         Int(frame) = mean(reshape(stackb,1,numel(stackb)));
    
    Z(i) =find(Int == max(Int),1);
    %finds the index of the first maximum in the vector containing the mean value of the
    %pixel intensity of each frame of the tracked non-projected movie and stores it in Z at position i
    %=> Z contains a value for each frame of the projected movie/one value
    %for each z stack
    
    Int0=Int-min(Int);
    %For each z stack "Int0" contains the values stored in "Int" minus the minimal value stored in "Int"
    %mean intensity of each plane of a z stack minus the minimal mean intensity of all planes of a z stack
    %=> mean intensity values relative to the lowest mean intensity??
    
    %Aurelie's comments:
    %subpixel tracking by fitting with a gaussian
    dp=2; %Aurelie's comments: default 2 ?
    i0=Z(i)-dp;
    %"i0" contains the index of the plane for which the maximum was found
    %-2
    ii=Z(i)+dp;
    %"ii" contains the index of the plane for which the maximum was found
    %+2
    if (Z(i)+dp <= Nz)&(Z(i)-dp>=1)
        %checks whether the planes corresponding to the indices i0 and ii
        %exist for the stacks taken
        Zsub(i) = fitwithgaussian([Z(i)-dp:Z(i)+dp],Int0(Z(i)-dp:Z(i)+dp),Z(i));
        %Inputs are: 
        %A vector containing: 
        %1. the indices from Z(i)-dp until Z(i)+dp, 
        %2. Int0: mean intensity values relative to the lowest mean intensity for theses indices
        %3. Z(i): the index of the first maximum in the vector containing the mean value of the
        %pixel intensity of each frame of the tracked non-projected movie
        
        %Outputs are:
        %position of the center of the peak of the Gaussian fit to the data
        %is stored in the vector Zsub at position i
        %=> Zsub contains one value for each frame of the projected movie 
        %-> Z position of particle with subplane accuracy 
    elseif Z(i)+dp > Nz
        %if the indices Z(i)+/-dp exceed the stack taken then Zsub at this
        %position is set to the value of the frame containing the first
        %maximum Z(i)
        %Aurelie's comment: Zsub(i) = fitwithgaussian([Z(i)-dp:Nz],Int(Z(i)-dp:Nz));
        Zsub(i) = Z(i);
        
    elseif Z(i)-dp<1
        %Aurelie's comment: Zsub(i) = fitwithgaussian([1:Z(i)+dp],Int(1:Z(i)+dp));
        Zsub(i) = Z(i);
        %if the indices Z(i)+/-dp exceed the stack taken than Zsub at this
        %position is set to the value of the frame containg the first
        %maximum Z(i)
    end
        p.progress;
end
    p.stop;
%end



%Aurelie's comments:
%Correct points which are further than ErrorDpix
%from the previous point; for those points the previous value in Zsub and the value itsself are added
%and the value is divided by two => this value is then taken for the value
%itsself and the previous value
Zsub=Zsub'; %turns a the row vector Zsub into a column vector
Z=Z'; %turns a the row vector Z into a column vector
dZ=abs(Zsub(2:L)-Zsub(1:L-1)); %dZ is the absolute difference in Z between two frames 
%of the tracked projected movie/how much the particle moved in Z between two z stacks 
f_er=find(dZ>ErrorDpix); %f_er: finds indices of entries showing that the this difference is greater than ErrorDpix here 2.5 %e.g. for Zsub(118) this is the case
%Zsub;
Ler=length(f_er); %Ler: contains how often the substack accurate Z position jumps by more than 
%ErrorDpix (default 2.5) inbetween to frames of the tracked projected movie 
df=f_er(2:Ler)-f_er(1:Ler-1); %df:saves the difference between consecutive indices stored in f_er 
f_n=find(df>1); % fn saves indices for which values in df are greater than one 
Lf_n=length(f_n); %counts for how many points this is the case
k=1;
Index_corrected=[];
for i= 1:Lf_n %runs over f_n
    if (f_er(f_n(i))+1<=L) %L: last frame of the non-projected movie for which tracking was done + 1 e.g. 159
        %f_er(f_n(i))+1 are the same numbers as the indices of Zsub %e.g. Zsub(117) = Zsub(117)+Zsub(118)/2
        %=> if the index for which the difference was greater than ErrorDpix exist for the z stacks taken   
        
        %Aurelie's comments:
        %              if (f_er(f_n(i))-f_er(k))==0
        %                   Zsub(f_er(k)+1:f_er(f_n(min(i+1,Lf_n))))= (Zsub(f_er(k))+Zsub(f_er(f_n(min(i+1,Lf_n)))+1))./2;
        % %                  f_er(k)+1
        % %                  f_er(f_n(i+1))
        %                   k=f_n(min(i+1,Lf_n))+1;
        %              else
        if k<(f_n(i)+1) %k=1 f_n(i)+1 runs over all of f_n except for the first one 
            Zsub(f_er(k)+1:f_er(f_n(i)))= (Zsub(f_er(k))+Zsub(f_er(f_n(i))+1))./2; %e.g. Zsub(118:117) = Zsub(117)+Zsub(118)/2 WHY NOT CHANGED IN ZSUB??
            %                  f_er(k)+1
            %                  f_er(f_n(i))
            Index_corrected=[Index_corrected,f_er(k)+1:f_er(f_n(i))]; %e.g. Index_corrected=[Index_corrected,118:117] DETAILS!!
            k=f_n(i)+1;
            %Index_corrected contains the frames of the tracked projected
            %movie for which the substack accurate Zpositon was corrected
            %since the fit resulted in a jump between frames of projected
            %movie greater than 2.5
        end
        %end
    else
        Zsub(f_er(k)+1:f_er(f_n(i)))= Zsub(f_er(k)); %e.g. Zsub(118:117)=Zsub(117)
        Index_corrected=[Index_corrected,f_er(k)+1:f_er(f_n(i))]; %e.g. Index_corrected=[Index_corrected,118:117]
    end
end

%Zsub(f_er)=(Zsub(f_er-1)+Zsub(f_er+1))./2;
        
end
    
    %=======================================================================
    % fit the intensity as a function of Z with a gaussian
    function m = fitwithgaussian(X,Y,Z)
    
        %Inputs are: 
        %X: A vector containing the indices from Z(i)-dp until Z(i)+dp, 
        %Y: the values stored in Int0 for these indices 
        %Z: the index of the first maximum in the vector containing the mean value of the
        %pixel intensity of each frame of the tracked non-projected movie
        %(Z(i))
    
    sx = size(X);
    %for dp=2 sx=[1 5]
    if sx(1) < sx(2) %ISN'T this always the case?
        X = X'; %turns a the row vector X into a column vector
    end
    
    sy = size(Y);
    %for dp=2 sy=[1 5]
    if sy(1) < sy(2)
        Y = Y';
    end
    %the same as above
    
    s = fitoptions('Method','NonlinearLeastSquares',... %fitting method DETAILS
        'Lower',[0,0,0],...         %lower bounds of coefficients to be fitted
        'Upper',[3000,50,4],...     %upper bounds of coefficients to be fitted
        'Startpoint',[100,Z,1],...  %initial values for coefficients 
        'MaxIter',100);             %maximum number of interations allowed for the fit
    %creates an object "s" containing info for the fit: pairs: 'Name', Value 
    %here: method, bounds, startpoint and maximal iterations
    
    f = fittype('A.*exp(-(x-m)^2./(2.*s^2))','options',s);
    %defines the function with which to fit => Gaussian
    %A: height/amplitude of the curve's peak
    %m: position of the center of the peak
    %s: standard deviation, controls the width of the peak
    %pairs: 'Name', Value, here: fit option saved in s
    %gives the fit options
   
    %Aurelie's comments:
    %options = fitoptions;
    %options.StartPoint = [1 3 5];
    
    Fres = fit(X,Y,f); %DETAILS HERE!!
    %creates the fit to the data in X and Y with the model specified by f
    %(the fittype) 
    %X: A vector containing the indices from Z(i)-dp until Z(i)+dp, 
    %Y: the values stored in Int0 for these indices 
    coeffvals = coeffvalues(Fres);
    %retuns the coefficients of the "Fres" fit
    A = coeffvals(1); %A: height/amplitude of the curve's peak
    m = coeffvals(2); %m: position of the center of the peak
    s = coeffvals(3); %s: standard deviation, controls the width of the peak 
    % saves the coefficients in A, m and s
    % m is returned by the function
    
    %Aurelie's comments:
%     figure;plot(X,Y,'.')
%     hold on;
%     plot(Fres,'r')
    %pause;
    
    end
%==========================================================================
        function [stackb]=zoomf(stack,xpix,ypix,dpix,frame) %WHERE IS IT CALLED?
            
            %to zoom around a particle, half-size of the box:dpix
            S=size(stack);
            xpix=round(xpix);
            ypix=round(ypix);
            if (xpix-dpix<=0) | (ypix-dpix<=0)| (xpix+dpix>S(2)) | (ypix+dpix>S(1))
                stackb=0;
            else
                stackb=stack(ypix-dpix:ypix+dpix,xpix-dpix:xpix+dpix,frame);
            end
        end    
%==============================================================
function circle=circle_d(r,l)   %WHERE IS IT CALLED?
% Generates circular Mask with radius r and box size l

circle=zeros(l);
l=(l-1)/2;
for i = -l:l
    for j= -l:l
        if round(sqrt((i*i)+(j*j)))>r
        circle(i+l+1,j+l+1)=1;
        end
    end
end
circle=uint16(circle);
end