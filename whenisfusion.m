%function [FT_max] = whenisfusion(Track_TRAC3D)

%To automatically determine the time point of fusion
% i.e. colocalization to uncolocalization
%Aur�lie Dupont

close all;
Track_TRAC3D=Manualtrack1TRAC3Dcompare;

Max=Track_TRAC3D(:,9);
Maxrand=Track_TRAC3D(:,10);
Time=Track_TRAC3D(:,2);
x=Track_TRAC3D(:,11).*0.1398;
y=Track_TRAC3D(:,12).*0.1398;
z=Track_TRAC3D(:,13).*0.3;


%Detect and delete black frames giving very high max values
IndBlack=find(Maxrand>=0.4);
Max(IndBlack)=[];
Maxrand(IndBlack)=[];
Time(IndBlack)=[];
x(IndBlack)=[];
y(IndBlack)=[];
z(IndBlack)=[];

% dw=5;
% Max_smo=smooth(Max,dw);
%Max_smo2=smooth(Max,9);
dTi=1; %re-sampling time
Ti=[0:dTi:max(Time)];
%size of the moving average in seconds
Tw=30;
Tsmo=min(find(Ti>=Tw));
%Making threshold for Max from randomized control
Maxrandi = interp1(Time,Maxrand,Ti);
Maxrand_smo=smooth(Maxrandi,Tsmo);
Sig=std(Maxrand);
Th=mean(Maxrand)+3*Sig;
Th2=mean(Maxrand)+2*Sig;
%Threshold for r
Thr=0.1; %in �m
Thr2=0.4; %in �m
%ThSi=0.10;
%Interpolate the data to dt=1sec to smooth always with the same time
%intervall
Maxi = interp1(Time,Max,Ti);
Max_smo=smooth(Maxi,Tsmo);



xi=interp1(Time,x,Ti);
yi=interp1(Time,y,Ti);
zi=interp1(Time,z,Ti);
%set the zero for r vector
TZero=50;%index for making the zero
xz=x-mean(xi(1:TZero));
yz=y-mean(yi(1:TZero));
zz=z-mean(zi(1:TZero));
r=sqrt(xz.^2+yz.^2+zz.^2);
%r(IndBlack)=[];


Twr=15;
% Tsmor=min(find(Ti>=Twr));%size of the time window for averaging
% r_smo=smooth(ri,Tsmor);
% dt=8;%in seconds
% dti=min(find(Ti>=dt));%find the corresponding index
% for i=1:length(ri)-dti
%    Si(i)=std(ri(i:i+dti)); 
% end
%  Thlength=2.*dt./dTi+1;%how many contiguous points over threshold
%  FTind_r=[];
% for j=1:length(Si)-Thlength+1
%     L=sum(Si(j:j+ Thlength-1)>ThSi);
%    if L>=Thlength
%       FTind_r=[FTind_r;j]; 
%    end  
% end
% FTind_r=min(FTind_r);
% %FT_r=Ti(FTind_r+dti);
% FT_r=Ti(FTind_r);

%Find when r is over the Threshold.
 %Thlength=Twr./dTi+2;%how many contiguous points over threshold
%  Thlength=2;
%  FTind_r=[];
% for j=1:length(ri)-Thlength+1
%     L=sum(r_smo(j:j+ Thlength-1)>Thr);
%    if L>=Thlength
%       FTind_r=[FTind_r;j]; 
%    end  
% end
% FTind_r=min(find(ri>Thr));
% %FTind_r=min(FTind_r);
% FT_r=Ti(FTind_r);

%Interpolate Max_smo to obtain a better estimate of FT_max
%Tif=[0:0.25:max(Time)];
%yi = interp1(Ti,Max_smo,Tif);
FTind_max=min(find(Max_smo<Th));
FT_max=Ti(FTind_max);

figure;
subplot(2,1,1);
plot(Time,Max,'.');hold on;
plot(Ti,Max_smo,'-');
plot(Time,Maxrand,'r.');plot(Ti,Maxrand_smo,'r-');
plot(Time,Time.*0+Th,'-r');
%plot(Time,Time.*0+Th2,'-m');
plot(FT_max,Max_smo(FTind_max),'*g');hold off;
xlabel('t [s]');ylabel('Max.');
subplot(2,1,2);
plot(Time,r,'.');hold on;
plot(Time,r,'-');hold on;
%plot(Ti,ri,'-');
plot(Time,Time.*0+Thr,'-','Color',[0.5,0.5,0.5]);
plot(Time,Time.*0+Thr2,'-r');
%plot(FT_r,ri(FTind_r),'*g');
xlabel('t [s]');ylabel('Relative dist. [�m]');
set(gca,'YLim',[-0.3 2]);set(gcf,'Color',[1,1,1])


