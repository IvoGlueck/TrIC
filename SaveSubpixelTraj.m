function [SubpixelTraj] = SaveSubpixelTraj (FramesOfTrack, SubpixelXCoordinates, SubpixelYCoordinates)

SubpixelTraj = zeros(length(FramesOfTrack), 3);

SubpixelTraj(:,1) = FramesOfTrack;
SubpixelTraj(:,2) = SubpixelXCoordinates;
SubpixelTraj(:,3) = SubpixelYCoordinates;


