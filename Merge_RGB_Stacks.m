function Merged_RGB_stack = Merge_RGB_Stacks (Stack1, Stack2, min_x_index, max_x_index, min_y_index, max_y_index, frame_init, number_of_frames)
%function Merged_RGB_Stack_Exporter (PathMovie1, PathMovie2, Saving_destination, MergedMovieName) 
    
%rounds subpixel indices
max_x_index = round(max_x_index);
min_x_index = round(min_x_index);
max_y_index = round(max_y_index);
min_y_index = round(min_y_index);

%info = imfinfo(PathMovie1);
W = max_x_index - min_x_index + 1;
H = max_y_index - min_y_index + 1;
%H = info(1).Height;
%W = info(1).Width;
L = number_of_frames;
%L=length(info); %number of frames of the movie 
Merged_RGB_stack = (zeros(H,W,3,L)); %preallocates the stack

disp('Channels are merged');
%Activate if merged stack should be exported
%outputFileName = strcat(Saving_destination, '\', MergedMovieName); %contains the name of the outputfile

p = ProgressBar(L);
for i=1:L
    FrameChannel1 = Stack1(min_y_index:max_y_index, min_x_index:max_x_index, frame_init+i-1);
    %FrameChannel1 = imread(PathMovie1, 'Index', i); %reads in one frame of the movie at a time
    FrameChannel1adj = imadjust(FrameChannel1);
    
    FrameChannel2 = Stack2(min_y_index:max_y_index, min_x_index:max_x_index, frame_init+i-1);
    %FrameChannel2 = imread(PathMovie2, 'Index', i); %reads in one frame of the movie at a time
    FrameChannel2adj = imadjust(FrameChannel2);
    
    MergedFrame = imfuse (FrameChannel1adj, FrameChannel2adj); %contains the merged frame 
    %MergedFrame = imfuse (FrameChannel1, FrameChannel2); %contains the merged frame 
    MergedFramedouble = im2double(MergedFrame);
    Merged_RGB_stack(:,:,:,i) = MergedFramedouble; %saves the corrected frames in an array
    %imwrite(MergedFrame, outputFileName);

p.progress;
end
p.stop;

%Activate if merged stack should be exported
% %outputFileName = strcat(Saving_destination, '\', MergedMovieName); %contains the name of the outputfile
% 
% for K=1:length(stack(1, 1, 1, :)) %iterates over the stack containing the corrected frames
%     %imwrite(stack(:, :, :, K), outputFileName, 'WriteMode', 'append'); %writes all corrected frames into a single tiff file and saves it in the current folder 
%     imwrite(stack(:, :, :, K), outputFileName, 'tif', 'WriteMode', 'append',  'Compression','none'); %writes all corrected frames into a single tiff file and saves it in the current folder 
% end
% 
% 'Merge converted into RGB'
