function Check_loading

%% checks if movies + transformation structure are loaded

% returns error message if neither 3D nor 2D movies are loaded
if usersvalues.twoD == 1 
   
    if isfield (movies,'TwoD_movie_tracked') == 0
       msgbox('Please load movies before starting the analysis', 'Error','error')
       return 
    end
    
end    

if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1
   
    if isfield (movies, 'non_proj_movie_tracked') == 0 
       msgbox('Please load movies before starting the analysis', 'Error','error');
       return
    end
   
end

% returns error message if XY shift correction has been chosen and no transfer structure has been loaded
if usersvalues.XYcorrection == 1 
   
    if isfield (Filenames, 'transf') == 0  
       msgbox('Please load a transformation structure before starting the analysis', 'Error','error');
       return
    end

end