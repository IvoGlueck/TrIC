function [vi]= instspeed(t,x,y,z,pixelxy,pixelz)

%Inputs: 
%t: vector with entry for each step of the track containing absolute time since the beginning of the track [s]
%x: subpixel accurate X coordinates of the track for each frame 
%y: subpixel accurate Y coordinates of the track for each frame 
%z: Z coordinates with subplane accuracy (results of fit)
%pixelxy: pixel size x y entered in the GUI [�m]
%pixelz: distance between Z slices entered in the GUI [�m]

%Outputs:
%vi: vector containing the instantaneous velocity considering 3D movement for each frame of the track [�m/s]
%for first frame vi=0

L=length(x); %number of frames of the track
x=x.*pixelxy;
y=y.*pixelxy;
z=z.*pixelz;
%the subpixel localization values are multiplied by the pixel size
%=> coordinates in �m
%"." indicates that each entry in the vector is multiplied by the pixel
%size

dx = x(2:L)-x(1:L-1);
%distances the particle moved in x between the frames in �m
dy = y(2:L)-y(1:L-1);
%distances the particle moved in y between the frames in �m
dz = z(2:L)-z(1:L-1);
%distances the particle moved in z between the frames in �m
ds = sqrt(dx.^2 + dy.^2 +dz.^2);
%"ds" are the distances the particle moved in 3D between the frames (Satz
%des Pythagoras)

%Aurelies stuff:
% for i = 1:L-1
%     s(i) = sum(ds(1:i));
% end
% s=[0,s];
% ds = s(2:L)-s(1:L-1);

dt = t(2)-t(1);
%interval time is calculated from the entries in "t". One could also say
%"dt=t(2)" since this equals the interval time 
vi = ds./dt;
%each distance stored in "ds" is divided by interval time => "vi" is a
%vector containing the speed between the frames 
vi=[0;vi];
%inserts a "0" as the first entry in the vector "vi" is missing the first entry
%before => velocity for first frame is 0

disp ('Velocity calculated');