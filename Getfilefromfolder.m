function [Filepath] = Getfilefromfolder (folderpath, fileending)
%Saves the paths to all movies needed for the "New 3D" analysis 
%You only need to select the path to the folder containing the movies you
%want to analyse
%Please save all movies in one folder and include " tracked projected", 
%" untracked projected", " tracked not projected" and " untracked not projected" 
%in the respective file names   

%Inputs: no inputs

%Outputs: 
%Trackedmovie: Path to the tracked projected movie
%Untrackedmovie: Path to the untracked projected movie
%TrackedmovieZstack: Path to the tracked not projected movie
%UntrackedmovieZstack: Path to the untracked not projected movie

%Ivo M. Gl�ck

%MovieFolder = uigetdir('X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Experiments\Analysis experiments','Select the folder containing the movies to analyse');
%MovieFolder = uigetdir('X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Experiments\Analysis experiments','Select the folder containing the movies to analyse');
%X:\Data\Foamy Virus Lindemann Aurelie 2009\Ivo-Foamy\Experiments\Analysis experiments\Reproduction old data\10_11_30_P10_Track_No9\Analysis on cropped movies\3 frames movies
%MovieFolder contains the path to the folder selected by the user
allFiles = dir(folderpath);
%allFiles is a structure containing info (as fields) about all files in the folder 
allNames = { allFiles.name };
%saves the names of all files in the folder in a cell array


% 1. Save path to tracked projected movie 
SearchFile = strfind(allNames, fileending);
%cell array with one entry for all entries in "allNames" searches all entries 
%in "all names" for the string specified and stores the position at which the 
%string starts as a number; if the string is empty it saves an empty "[]"  
Index = find(~cellfun(@isempty, SearchFile));
%finds the index of the non-empty entry in "SearchTrackedProjected" which corresponds to the index of the file name in "allNames"  
FileName = allNames(Index);
%Saves the name of the file containing the tracked projected movie 


Filepath=char(strcat(folderpath, '\',FileName));
%Saves the path to the file
