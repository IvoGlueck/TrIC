function [ projected_movie ] = CreateProjectedMovie( non_projected_movie, nPlanes, removal_status )
%does a maximum projection of the non-projected movie and returns the
%projected movie

% Projection routine if first stack and first frame of every stack are
% removed 
if removal_status == 1

    %Preallocates projected movie
    sz_y = length(non_projected_movie(:,1,1));
    sz_x = length(non_projected_movie(1,:,1));
    sz_z = (length(non_projected_movie(1,1,:)))/(nPlanes-1);
    projected_movie = zeros([sz_y sz_x sz_z],'uint16');

    %does maximum projection for every stack
    frame_projected_movie = 0;
    p = ProgressBar(length(non_projected_movie(1,1,:))-(nPlanes-2)); %progress bar
    for k = 1:(nPlanes-1):(length(non_projected_movie(1,1,:))-(nPlanes-2))
        current_stack = non_projected_movie(:,:,k:k+nPlanes-2);
        mip_current_stack = max(current_stack, [], 3);
        frame_projected_movie = frame_projected_movie + 1;
        projected_movie(:,:,frame_projected_movie) = mip_current_stack;
    p.progress;
    end
    p.stop;

else
    
    % Projection routine if first stack and first frame of every stack are
    % NOT removed
    
    %Preallocates projected movie
    sz_y = length(non_projected_movie(:,1,1));
    sz_x = length(non_projected_movie(1,:,1));
    sz_z = (length(non_projected_movie(1,1,:)))/(nPlanes);
    projected_movie = zeros([sz_y sz_x sz_z],'uint16');

    %does maximum projection for every stack
    frame_projected_movie = 0;
    p = ProgressBar(length(non_projected_movie(1,1,:))-(nPlanes-1)); %progress bar
    for k = 1:(nPlanes):(length(non_projected_movie(1,1,:))-(nPlanes-1))
        current_stack = non_projected_movie(:,:,k:k+nPlanes-1);
        mip_current_stack = max(current_stack, [], 3);
        frame_projected_movie = frame_projected_movie + 1;
        projected_movie(:,:,frame_projected_movie) = mip_current_stack;
    p.progress;
    end
    p.stop;
    
end