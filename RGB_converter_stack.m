function RGB_stack = RGB_converter_stack (stack, min_x_index, max_x_index, min_y_index, max_y_index, frame_init, number_of_frames)
%Saves a 16-bit grey scale stack as a RGB stack 

%Inputs:
%stack: the stack to convert as 16-bit grey scale 
%min_x_index: lowest index in X to export
%max_x_index: highest index in X to export
%min_y_index: lowest index in Y to export
%max_y_index: highest index in Y to export
%frame_init: the index of the first frame of the stack which should be
%exported
%number_of_frames: total number of frames to convert into RGB
%result_location: the path to the folder in which to save the RGB-converted movie
%moviename: the name of the file containing the RGB converted movie 

%rounds subpixel indices
max_x_index = round(max_x_index);
min_x_index = round(min_x_index);
max_y_index = round(max_y_index);
min_y_index = round(min_y_index);

%info = imfinfo(Stackname);
W = max_x_index - min_x_index + 1;
%W = info(1).Width;
H = max_y_index - min_y_index + 1;
%H = info(1).Height;
L = number_of_frames;
%L=length(info); %number of frames of the movie 
RGB_stack = zeros(H,W,3,L); %preallocates the stack

disp ('RGB conversion ongoing'); %this statement is shown in the command window

p = ProgressBar(L);
for i=1:L
    frame = stack (min_y_index:max_y_index, min_x_index:max_x_index, frame_init+i-1); 
    %frame = imread(Stackname, 'Index', i); %reads in one frame of the movie at a time
    %imagesc(frame);
    frame_adjusted = imadjust(frame); %adjusts the contrast
    %imshow(frame_adjusted);
    RGB_frame = RGB_converter (frame_adjusted);
    %imshow(RGB_frame);
    %RGB_frame2 = RGB_converter (frame); %activate if movie should be saved
    %with original contrast
    %imshow(RGB_frame2);
    RGB_stack(:,:,:,i) = RGB_frame; %saves the corrected frames in an array

 p.progress;
end
 p.stop;

 % Activate in case you want to save the RGB converted movie in the results
% folder 
% outputFileName = strcat(result_location, '\', moviename); %contains the name of the outputfile
% %outputFileName = strcat(result_location, '\', 'Tracked_projected_RGB_inverted.tif'); %contains the name of the outputfile
% %outputFileName = strcat(Stackname(1:length(Stackname)-4),'_RGB_inverted.tif'); %contains the name of the outputfile
% 
% for K=1:length(stack(1, 1, 1, :)) %iterates over the stack containing the corrected frames
%     imwrite(stack(:, :, :, K), outputFileName, 'tif', 'WriteMode', 'append',  'Compression','none'); %writes all corrected frames into a single tiff file and saves it in the current folder 
% end
% 
% 'Inverted RGB movie saved'