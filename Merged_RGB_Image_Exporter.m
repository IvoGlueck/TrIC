function [MergedImage] = Merged_RGB_Image_Exporter (PathImage1, PathImage2) 
    
    %Image1 is green
    %Image2 is magenta 
    
    
    
    Image1 = imread(PathImage1);
    
    Image2 = imread(PathImage2);
    
    MergedImage = imfuse(Image1, Image2); %MergedImage is RGB
    
    %imwrite(MergedImage, 'MergedImage.tif');
    
    %imshow(MergedImage);