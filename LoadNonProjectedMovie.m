function [movie] = LoadNonProjectedMovie (Nplanes, Rawmoviesnames, folderpath, removal_status)
%GENERAL DESCRIPTION:
%reads in a non-projected movie recorded at the Andor SD 
%concatenates files created by Andor Solis
%Removes frames belonging to first stack
%Removes first frame of every stack
    
%CAUTION: files have to be opened in the order the have to be
%concatenated (first to last)

%INPUTS:
%Nplanes: Number of planes per stack recorded at the Andor Spinning
%Disk/inserted into the LV software
%Nframes_recorded: total number of frames recorded 
%Rawmoviesnames: containes the names of the raw movie subfiles
%folderpath: contains the path to the folder in which subfiles are stored

%OUTPUT:
%movie: 3D array containing all frames of the non-projected movie; the third dimension is
%the frame; the frames belonging to the first stack are removed and the
%first frame of each stack is removed

% if only a single file is selected "Rawmovienames is a character string
% and needs to be converted into a cell array of strings
if ischar(Rawmoviesnames)
   Rawmoviesnames={Rawmoviesnames};   
end

%Obtains image info for all files
for i = 1:length(Rawmoviesnames)
    current_subfile = fullfile(folderpath, Rawmoviesnames{i});
    ImageInfo{i} = imfinfo(current_subfile);
end
Nframes_total = sum(cellfun(@length,ImageInfo));  

%Preallocates the array containing the movie
sz_y = ImageInfo{1}.Height;
sz_x = ImageInfo{1}.Width;
sz_z = Nframes_total;
movie = zeros([sz_y sz_x sz_z],'uint16');

%concatenates the selected files and loads them into one 3D array
frame_pointer = 0;
p = ProgressBar(length(Rawmoviesnames)); %progress bar
for i=1:length(Rawmoviesnames)
    current_file=fullfile(folderpath, Rawmoviesnames{i}); %Saves the path to the file
    Nframes=length(ImageInfo{i});
    TifLink = Tiff(current_file, 'r');
    for current_frame = 1:Nframes
        TifLink.setDirectory(current_frame);
        movie(:,:,frame_pointer+current_frame)=TifLink.read();
        %disp(frame_pointer + current_frame);
    end
    frame_pointer = frame_pointer + current_frame;
    TifLink.close();
    p.progress;
end
p.stop;

% if the user selected to remove the first stack + the first frame of every
% stack
if removal_status == 1

    %Removes first stack 
    pointer2 = 0;
    for i=1:Nplanes
        pointer2 = pointer2+1;
        if pointer2 <= Nplanes
            movie(:,:,1) = []; 
        end
    end

    %Removes first frame of every stack
    removalList = 1:Nplanes:(numel(movie(1,1,:)));
    movie(:,:,removalList) = [];

    %Removes incomplete stacks at end of movie ("Nplanes-1" because first
    %frame was removed)
    N_full_stacks = floor((numel(movie(1,1,:)))/(Nplanes-1));
    N_frames_remove_end = (numel(movie(1,1,:)) - (N_full_stacks*(Nplanes-1)));

else

    %Removes incomplete stacks at end of movie
    N_full_stacks = floor((numel(movie(1,1,:)))/(Nplanes));
    N_frames_remove_end = (numel(movie(1,1,:)) - (N_full_stacks*(Nplanes)));

end 

movie = movie(:,:,1:end-N_frames_remove_end);

%Things to be added:
%eventually read in only needed pixels; if frame value exceeds first file
%read it from the second