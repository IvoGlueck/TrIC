function [single_track_data] = LoadTrack(track_name, track_path)
%function [ current_track_data, full_path_track, track_name ] = LoadTrack( track_path, track_name )

full_path_track = char(strcat(track_path, track_name));

single_track_data = load(full_path_track);

end

