function [out] = fullplotanalysis(Curdata, usersvalues)

%To plot all the results in a same new figure.
%To be called in TRAC


t=Curdata.time;
%usersvalues.pixxy = str2double(get(handles.pixxy,'String'));
%reads out the pixel size in xy entered in the GUI, turns it into a double
%and saves it in the variable "uservalues.pixxy"

%usersvalues.pixZ = str2double(get(handles.Zdist,'String'));
%reads out the distance between the Z slices entered in the GUI

x=Curdata.xsub*usersvalues.pixxy; %Aurlie's solution: 0.1398; % positions in �m
y=Curdata.ysub*usersvalues.pixxy; %Aurlie's solution: 0.1398; 
z=Curdata.zsub*usersvalues.pixZ; %Aurlie's solution: 0.3

Max=Curdata.maxcc;
Maxpos1=Curdata.ccposx;
Maxpos2=Curdata.ccposy;
Int2=Curdata.intGcorrected;
Int1=Curdata.intRcorrected;
vi=Curdata.speed*usersvalues.pixxy; %Aurelie's solution: 0.1398;  % speed in �m/s


figure;
set(gcf,'Position',[75 205 1075 445]);
% subplot(5,1,1);
% plot(t,x-x(1)); hold on; plot(t,y-y(1),'k');plot(t,z-z(1),'g');
% %plot(t(Indact),x(Indact)-x(1),'xr'); plot(t(Indact),y(Indact)-y(1),'xr');plot(t(Indact),z(Indact)-z(1),'xr');
% title('x,y,z trajectories');
% xlabel('t (s)');
% ylabel('�m');

subplot(2,2,1); plot(t(1:end),vi);set(gca,'XLim',[0 t(end)]);title('Instantaneous speed')
xlabel('t (s)');
ylabel('�m/s');

subplot(2,2,2);plot(t,Max,'.');set(gca,'YLim',[0 (max(Max))+0.05],'XLim',[0 t(end)]);title('Cross correlation maximum');
%Aurelie's original: subplot(2,2,2);plot(t,Max,'.');set(gca,'YLim',[0 1],'XLim',[0 t(end)]);title('Cross correlation maximum');
xlabel('t (s)');

subplot(2,2,4);plot(t,Maxpos1,'.');hold on;plot(t,Maxpos2,'.r');
title('Maximum position');set(gca,'YLim',[-10 10],'XLim',[0 t(end)]); hold off;
xlabel('t (s)');

subplot(2,2,3);plot(t,Int1,'.r');hold on;plot(t,Int2,'+g');
title('Intensity (corrected)');set(gca,'YLim',[(min(min(Int1),min(Int2))) (max(max(Int1),max(Int2)))+30],'XLim',[0 t(end)]);hold off;
xlabel('t (s)');
set(gcf,'Color',[1,1,1]);

Ze=zeros(1,length(x))+1;
figure;
plot3(x,y,z,'r');
grid on; hold on;
plot3(x(1),y(1),z(1),'og','MarkerSize',10,'LineWidth',2);
plot3(x,y,0*Ze);
plot3((round(max(x))+2.5)*Ze,y,z);%Aurelie's solution: plot3(24*Ze,y,z);
plot3(x,(round(max(y))+2.5)*Ze,z);%Aurelie's solution: plot3(x,45*Ze,z);
title('Trajectory'); set (gca, 'XLim', [min(x)-1 max(x)+3], 'YLim', [min(y)-1 max(y)+3], 'ZLim', [0 max(z)+1]); 
xlabel('x (�m)');
ylabel('y (�m)');
zlabel('z (�m)');
daspect([1 1 1]);
set(gcf,'Color',[1,1,1])

out=1;