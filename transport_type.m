function transport_type (Track_TRAC3D)

inst_velocity=Track_TRAC3D(:,20);
Time=Track_TRAC3D(:,2);

Thr1=0.05; %velocity in �m/s; below -> passive transport
Thr2=0.1; % velocity in �m/s; above -> active transport 

figure;
plot (Time, inst_velocity,'.', 'linewidth', 2); hold on;
plot (Time, inst_velocity,'-', 'linewidth', 2);
plot (Time, Time.*0+Thr1,'-r', 'linewidth', 2);
plot (Time, Time.*0+Thr2,'-g', 'linewidth', 2);
xlabel ('t (s)', 'FontSize', 12,'FontWeight', 'bold'); ylabel ('instantaneous velocity (�m/s)', 'FontSize', 12,'FontWeight', 'bold');
set(gca,'FontSize', 12, 'FontWeight', 'bold')
plot(597,inst_velocity(136),'*g');
plot(580,inst_velocity(133),'*b');
