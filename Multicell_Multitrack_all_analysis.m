%% Runs entire analysis on multiple tracks of multiple cells 
function Multicell_Multitrack_all_analysis(hObject, eventdata, handles)
% hObject    handle to Multicell_Multitrack_all_analysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Loads global variables 

global batch_data_base
global usersvalues
global Filenames

%% Loops through selected cells 

selected_cells = fieldnames(batch_data_base);
number_of_cells = length(selected_cells(:,1));

for current_cell_number = 1:number_of_cells
    
    %% Reads in the variables "Filenames" and "usersvalues" for the current cell
    
    current_cell_number_str = num2str(current_cell_number);
    current_cell_field_name = strcat('Cell_', current_cell_number_str);
    current_cell_data = batch_data_base.(current_cell_field_name);
    Filenames = current_cell_data.Filenames;
    usersvalues = current_cell_data.usersvalues;
    
    %% Loads files of current cell
    
    Load_files_Callback(hObject, eventdata, handles, current_cell_number, number_of_cells);
    
    %% Runs multitrack analysis on current cell
    
    %Multitrack_all_analysis_Callback(hObject, eventdata, handles, current_cell_number, number_of_cells);
    Multitrack_all_analysis(hObject, eventdata, handles, current_cell_number, number_of_cells); 
    
end

%% Completion message + clearing of variables from workspace 

disp('Multicell Multitrack analysis completed');
clear;