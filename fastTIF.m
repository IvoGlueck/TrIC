classdef fastTIF
    % class 'fastTIF': creates Array of IFD for specified TIF-file in order
    % to achieve shorter access times for frame reading
    %   constructor: determines byte-order of TIF and creates IFD-Array,
    %   saved under 'TIF_IFD'
    %   getSingleTIFframe: reads single frame from TIF_IFD
   
    properties %public class properties
        TIF_IFD
        TIF_filepath
    end
    
    properties (SetAccess = private, GetAccess = private) %private class properties
        byteorder
        fileid
    end
    
    
    methods
        
        function TIFobj = fastTIF(filepath) %class constructor
            
            %open file to determine byteorder ('big-indian' or'little-indian')
            byteorderfile = fopen(filepath, 'r');
            bytestate = fread(byteorderfile, 1, 'uint8');
            TIFobj.byteorder = bytestate;
            file_info = imfinfo(filepath);
            filelength = length(file_info);
            fclose(byteorderfile);           
            
            file_id = fopen(filepath, 'r', TIFobj.byteorder); %reopen TIF-stack with correct byteorder
           	TIFobj.fileid = file_id;
            %find IFD's:            
            nextIFD = 4; %starting position
            k = 1; %current position in IFDArray
            IFDArray = zeros(filelength, 4);
            TagArray = zeros(12,4);
            while (nextIFD ~= 0)
                if (nextIFD == 4) %read 1st IFD of file
                    fseek(file_id, 4, 'bof');
                    nextIFD = fread(file_id, 1, 'uint32');
                end
                fseek(file_id, nextIFD - ftell(file_id), 'cof');
                fIFDstart = fread(file_id, 1, 'uint16');
                fIFDdata = fread(file_id, fIFDstart * 6, 'uint16'); %position nextIFD + 2 (deshalb bei 16bit gleich als n�chstes ohne fseek)
                %fseek(fileid, ((fIFDstart * 12) + (nextIFD + 2)), 'bof');
                %find next IFD
                nextIFD = fread(file_id, 1, 'uint32'); %pos: (fIFDstart * 12) + nextIFD + 2
                for i = 1:((size(fIFDdata)/6))
                    TagArray(i,1) = fIFDdata(((i-1)*6)+1);
                    TagArray(i,2) = fIFDdata(((i-1)*6)+2);
                    TagArray(i,3) = fIFDdata(((i-1)*6)+3) + fIFDdata(((i-1)*6)+4);
                    if TIFobj.byteorder == 'b'
                        TagArray(i,4) = fIFDdata(((i-1)*6)+5) * 65536 + fIFDdata(((i-1)*6)+6);
                    else
                        TagArray(i,4) = fIFDdata(((i-1)*6)+5) + 65536 * fIFDdata(((i-1)*6)+6);
                    end
                end
                IFDArray(k, 1) = TagArray(TagArray(:,1) == 256, 4); %width
                IFDArray(k, 2) = TagArray(TagArray(:,1) == 257, 4); %height
                IFDArray(k, 3) = TagArray(TagArray(:,1) == 279, 4); %counter
                IFDArray(k, 4) = TagArray(TagArray(:,1) == 273, 4); %offset
                k = k + 1; %go to next line
            end
            TIFobj.TIF_IFD = IFDArray;
            TIFobj.TIF_filepath = filepath;
        end
        
        function delete(TIFobj) %deletes class instance and closes TIF-file
            fclose(TIFobj.fileid);
        end
        
        function imgdata = getSingleTIFframe(TIFobj, Frame)
            file_id = TIFobj.fileid;
            IFDArray = TIFobj.TIF_IFD;
            IFDdata = IFDArray(Frame, :);

            if TIFobj.byteorder == 'l'
                fseek(file_id, IFDdata(4), 'bof'); %read position of first strip (at least, I guess that's the meaning of this value...)
                imgstart = fread(file_id, 1, 'uint32');
            else
                imgstart = IFDdata(4);
            end
            fseek(file_id, imgstart, 'bof');
            imgdata = fread(file_id, (IFDdata(1) * IFDdata(2)), 'uint16=>uint32'); %read image data
            imgdata = reshape(imgdata, IFDdata(1), IFDdata(2)); %transform array of strips into 2D-matrix, determined by length and width of frame image
            imgdata = transpose(imgdata);
        end
        
        
        function TIFobj = set.byteorder(TIFobj, bytestate)
            if (bytestate == 77);
                TIFobj.byteorder = 'b'; %big-indian
            else
                TIFobj.byteorder = 'l'; %little-indian
            end
        end
        function byte_order = get.byteorder(TIFobj)
            byte_order = TIFobj.byteorder;
        end
        
        function TIFobj = set.fileid(TIFobj, file_id)
            TIFobj.fileid = file_id;
        end
        function file_id = get.fileid(TIFobj)
            file_id = TIFobj.fileid;
        end        
        
        function TIFobj = set.TIF_IFD(TIFobj, value)
            TIFobj.TIF_IFD = value;
        end
        function IFD = get.TIF_IFD(TIFobj)
            IFD = TIFobj.TIF_IFD;
        end
        
        %function TIFobj = set.TIF_filepath(TIFobj, value)
        %    TIFobj.TIF_filepath = value;
        %end
        %function filepath = get.TIF_filepath(TIFobj)
        %    filepath = TIFobj.TIF_filepath;
        %end
    end  
end