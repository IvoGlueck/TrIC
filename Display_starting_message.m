function Display_starting_message

%% Loads global variables 

global usersvalues

%% starting message

if strcmp (usersvalues.analysis_type, 'Single track') == 1 

    if usersvalues.twoD == 1 
        disp('2D Single track analysis started');
    end

    if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1 
        disp('3D Single track analysis started');
    end

end

if strcmp (usersvalues.analysis_type, 'Multitrack') == 1 
    
     if usersvalues.twoD == 1 
        disp('2D Multitrack analysis started');
    end

    if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1 
        disp('3D Multitrack analysis started');
    end
    
end

if strcmp (usersvalues.analysis_type, 'Multicell Multitrack') == 1 
    
     if usersvalues.twoD == 1 
        disp('2D Multicell Multitrack analysis started');
    end

    if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1 
        disp('3D Multicell Multitrack analysis started');
    end
    
end

