function [transf_structure] = LoadTransfStructure(TransfFile_name, TransfFile_path)

CompletePathTransfFile = char(strcat(TransfFile_path, TransfFile_name));

transf=load(CompletePathTransfFile);
fn=fieldnames(transf);
transf_structure=transf.(fn{1});

end

