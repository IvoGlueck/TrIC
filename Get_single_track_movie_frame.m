function single_track_movie_frame = Get_single_track_movie_frame (movie, frame_x_coordinate, frame_y_coordinate, frame, pixel_boundary)
    
    single_track_movie_frame = movie(frame_y_coordinate-pixel_boundary:frame_y_coordinate+pixel_boundary,frame_x_coordinate-pixel_boundary:frame_x_coordinate+pixel_boundary, frame);