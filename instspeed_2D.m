function [vi]= instspeed_2D(t,x,y,pixelxy)

%Inputs: 
%t: Array with entry for each step of the track containing absolute time since the beginning of the track
%x: subpixel accurate X coordinates of the track for each frame 
%y: subpixel accurate Y coordinates of the track for each frame 
%pixelxy:Pixel size x y entered in the GUI

%Outputs:
%"vi" is vector containing the instantaneous velocity ONLY considering 2D movement for each frame of the track 
%for first frame vi=0

%Aurelie's comments:
%figures out the instantaneous speed along the trajectory
% give t in seconds and x,y,z in pixels to have ��m/s
%pixelxy: pixel size in �m

%t: vector containing the time since the beginning of the track for each
%frame of the track
%x + y: X and Y coordinates of the track for each frame with subpixel
%accuracy

L=length(x);
%number of frames of the track is stored in L
x=x.*pixelxy;
y=y.*pixelxy;
%the subpixel localization values are multiplied by the pixel size
%=> coordinates in �m
%"." indicates that each entrie in the vector is multiplied by the pixel
%size

dx = x(2:L)-x(1:L-1);
%distances the particle moved in x between the frames in �m
dy = y(2:L)-y(1:L-1);
%distances the particle moved in y between the frames in �m

ds = sqrt(dx.^2 + dy.^2);
%"ds" are the distances the particle moved in 2D between the frames (Satz
%des Pythagoras)

%Aurelie's comment:
% for i = 1:L-1
%     s(i) = sum(ds(1:i));
% end
% s=[0,s];
% ds = s(2:L)-s(1:L-1);
dt = t(2)-t(1);
%interval time is calculated from the entries in "t". One could also say
%"dt=t(2)" since this equals the interval time 
vi = ds./dt;
%each distance stored in "ds" is divided by interval time => "vi" is a
%vector containing the speed between the frames 
vi=[0;vi];
%inserts a "0" as the first entry in the vector "vi" as it has one entry
%too less before => velocity for first frame is 0

disp ('Velocity calculated');