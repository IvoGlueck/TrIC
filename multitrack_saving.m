%% saves results of current track

Results_file_name=char(strcat('Results_Track_', track_name, '.mat')); %contains the name of the outputfile 
Results_folder_name=char(strcat('Results_Track_', track_name)); 
Parent_data_path=char(strcat(usersvalues.result_location, '\'));
mkdir ([Parent_data_path Results_folder_name]) 
Data_path = char(strcat(usersvalues.result_location, '\',Results_folder_name, '\', Results_file_name));
save(Data_path,'Curdata');