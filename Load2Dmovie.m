function [TwoDmovie] = Load2Dmovie (TwoDmovienames, folderpath)

% if only a single file is selected "TwoDmovienames is a character string
% and needs to be converted into a cell array of strings
if ischar(TwoDmovienames)
   TwoDmovienames={TwoDmovienames};   
end
    
%Calculates number of frames recorded 
Nframes_total = 0;
for subfile = 1:length(TwoDmovienames)
    current_subfile_path = char(strcat(folderpath, TwoDmovienames(1,subfile)));
    info_current_subfile = imfinfo(current_subfile_path);
    Nplanes_current_subfile = length(info_current_subfile);
    Nframes_total = Nframes_total + Nplanes_current_subfile;
end        

%Preallocates the array containing the movie
first_file_path = char(strcat(folderpath, TwoDmovienames(1,1))); %Saves the path to the first selected file
info_first_file = imfinfo(first_file_path);
sz_y = info_first_file(1).Height;
sz_x = info_first_file(1).Width;
sz_z = Nframes_total;
TwoDmovie = zeros([sz_y sz_x sz_z],'uint16');

%concatenates the selected files and loads them into one 3D array
frame_pointer = 0;
p = ProgressBar(length(TwoDmovienames)); %progress bar
for current_file=1:length(TwoDmovienames)
    current_file_path=char(strcat(folderpath, TwoDmovienames(current_file))); %Saves the path to the file
    info_current_file = imfinfo(current_file_path);
    Nframes=length(info_current_file);
    for current_frame = 1:Nframes
        TwoDmovie(:,:,frame_pointer+current_frame)=imread(current_file_path, 'Index', current_frame);
        %disp(frame_pointer + current_frame);
    end
    frame_pointer = (find(TwoDmovie(1,2,:)==0, 1, 'first'))-1;
p.progress;
end
p.stop;
