function varargout = TRAC(varargin)
% varargout (function with variable/any number of outputs) 
% varargin (variable with variable/any number of inputs)
% 
% TRAC M-file for TRAC.fig
%      TRAC, by itself, creates a new TRAC or raises the existing
%      singleton*. Singleton says that only one TRAC window can be open at
%      each time. If the TRAC.m code is executed twice the open TRAC window
%      will be activated (brought to front) but no second window will be
%      opened. 
%
%      H = TRAC returns the handle to a new TRAC or the handle to
%      the existing singleton*.
%
%      TRAC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRAC.M with the given input arguments.
%
%      TRAC('Property','Value',...) creates a new TRAC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before TRAC_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to TRAC_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TRAC

% Last Modified by GUIDE v2.5 04-Jun-2018 16:46:29

% below starts the initialization of the GUI.

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1; 
gui_State = struct('gui_Name',       mfilename, ... %contains the name of the current mfile so here TRAC
                   'gui_Singleton',  gui_Singleton, ... %is an option (0 or 1) that allows only one or several instance of your Gui to be started at the same time.
                   'gui_OpeningFcn', @TRAC_OpeningFcn, ...
                   'gui_OutputFcn',  @TRAC_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
               %gui_State is a structure containing 6 fields
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

%the above code initializes the GUI and is automatically inserted if a new
%GUI is created 

warning('off','all');
dbstop if error;

% --- Executes just before TRAC is made visible.
function TRAC_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to TRAC (see VARARGIN)

% Sets default folder path in uservalues
global usersvalues
usersvalues.defaultPath = '\\10.153.116.2\Fablab 2\Data\Foamy Virus Lindemann Aurelie 2009';

% Choose default command line output for TRAC
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes TRAC wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%labels the axes of the "XY Tracking with fit" plot
set(get(handles.XY_axes,'XLabel'),'String','relative X position (�m)'); 
set(get(handles.XY_axes,'YLabel'),'String','relative Y position (�m)'); 

%labels the axes of the "Z Tracking with fit" plot
set(get(handles.Z_axes,'XLabel'),'String','relative time (s)');
set(get(handles.Z_axes,'YLabel'),'String','relative Z position (�m)');

%sets the labels of the axes of the "Intensity" plot 
set(get(handles.intensity_axes,'XLabel'),'String','relative time (s)');
set(get(handles.intensity_axes,'YLabel'),'String','background-corrected intensity');

%sets the labels of the axes of the upper "Cross-correlation colocalization analysis" plot 
set(get(handles.CCmax_axes,'XLabel'),'String','relative time (s)');
set(get(handles.CCmax_axes,'YLabel'),'String', sprintf ('max. value of ICC \n of tracked particles'));

%sets the labels of the axes of the lower "Cross-correlation colocalization analysis" plot 
set(get(handles.CCmax_coordinates_axes,'XLabel'),'String','relative time (s)');
set(get(handles.CCmax_coordinates_axes,'YLabel'),'String', sprintf ('coordinates of max. value \n of ICC of tracked particles'));

%sets the labels of the axes of the "Instantaneous velocity" plot 
set(get(handles.velocity_axes,'XLabel'),'String','relative time (s)', 'FontSize', 10);
set(get(handles.velocity_axes,'YLabel'),'String', sprintf ('instantaneous velocity \n of tracked particles (�m/s)'), 'FontSize', 10);

%removes the tick labels of the "Particle image data" plot
axes(handles.particle_tracked_axes); set(gca, 'YTickLabel', [], 'XTickLabel', []);
axes(handles.particle_untracked_axes); set(gca, 'YTickLabel', [], 'XTickLabel', []);
axes(handles.particle_merged_axes); set(gca, 'YTickLabel', [], 'XTickLabel', []);


%%=========================================================================
%                       User inputs into GUI
%%=========================================================================

% --- Executes when selected object is changed in ana_type.
function ana_type_SelectionChangedFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in ana_type 
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

twoD_Callback(handles.twoD, eventdata, handles);
old3D_Callback(handles.old3D, eventdata, handles);
new3D_Callback(handles.new3D, eventdata, handles);

%% Reads out the status of the "2D" radio button in the GUI
% --- Executes on button press in twoD.
function twoD_Callback(hObject, eventdata, handles)
% hObject    handle to twoD (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of twoD

global usersvalues

usersvalues.twoD = get(hObject,'Value');

if usersvalues.twoD == 1 
   
   set(handles.PFS, 'Value', 0);
   set(handles.PFS, 'Enable', 'off');
   set(handles.text1, 'Enable', 'off');
   set(handles.Zslices, 'Enable', 'off');
   set(handles.text2, 'Enable', 'off');
   set(handles.Zdist, 'Enable', 'off');
   children = get(handles.Zpanel,'Children'); set(children(strcmpi ( get (children,'Type'),'UIControl')),'enable','off');
   set(handles.XYcorrection, 'Enable', 'on');
    
end

%% Reads out the status of the "Old 3D" radio button in the GUI
% --- Executes on button press in old3D.
function old3D_Callback(hObject, eventdata, handles)
% hObject    handle to old3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of old3D

global usersvalues

usersvalues.old_threeD = get(hObject,'Value');

if usersvalues.old_threeD == 1 
    
   set(handles.PFS, 'Value', 1);
   set(handles.PFS, 'Enable', 'on');
   set(handles.text1, 'Enable', 'on');
   set(handles.Zslices, 'Enable', 'on');
   set(handles.text2, 'Enable', 'on');
   set(handles.Zdist, 'Enable', 'on');
   children = get(handles.Zpanel,'Children'); set(children(strcmpi ( get (children,'Type'),'UIControl')),'enable','on');
   set(handles.Zdist, 'Enable', 'on');
   set(handles.XYcorrection, 'Enable', 'off'); %3D analysis currently not possible without correct for XY shift
   
end

%% Reads out the status of the "New 3D" radio button in the GUI
% --- Executes on button press in new3D.
function new3D_Callback(hObject, eventdata, handles)
% hObject    handle to new3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of new3D

global usersvalues

usersvalues.new_threeD = get(hObject,'Value');

if usersvalues.new_threeD == 1 
    
   set(handles.PFS, 'Value', 1);
   set(handles.PFS, 'Enable', 'on');
   set(handles.text1, 'Enable', 'on');
   set(handles.Zslices, 'Enable', 'on');
   set(handles.text2, 'Enable', 'on');
   set(handles.Zdist, 'Enable', 'on');
   children = get(handles.Zpanel,'Children'); set(children(strcmpi ( get (children,'Type'),'UIControl')),'enable','on'); 
   set(handles.XYcorrection, 'Enable', 'off'); %3D analysis currently not possible without correct for XY shift
   
end

%% Reads out the status of the "Incl. correction of XY shift" check box in the GUI
% --- Executes on button press in XYcorrection.
function XYcorrection_Callback(hObject, eventdata, handles)
% hObject    handle to XYcorrection (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of XYcorrection

%% Reads in the selection of the analysis type from the dropdown menu in the "Type of analysis" button group
% --- Executes on selection change in analysis_type_dropdown.
function analysis_type_dropdown_Callback(hObject, eventdata, handles)
% hObject    handle to analysis_type_dropdown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns analysis_type_dropdown contents as cell array
%        contents{get(hObject,'Value')} returns selected item from analysis_type_dropdown

global usersvalues        

% Selection in Analysis types dropdown menu 
analysis_types = get(handles.analysis_type_dropdown,'Value');

switch analysis_types 
    case 1 % Single track analysis was chosen 
        usersvalues.analysis_type = 'Single track';
    case 2 % Multitrack analysis on single cell was chosen 
        usersvalues.analysis_type = 'Multitrack';
    case 3 % Multicell Multitrack analysis was chosen 
        usersvalues.analysis_type = 'Multicell Multitrack';
end     

%% Sets visibility of GUI buttons depending on chosen analysis type
% if either the Single track or the Multitrack analysis has been chosen ...
if strcmp (usersvalues.analysis_type, 'Single track') == 1 || strcmp (usersvalues.analysis_type, 'Multitrack') == 1
    
    % ... the "Select files for analysis" button is not shown in the GUI
    % (and the selection of the files is done when pressing the "Load files
    % for analysis" button
    set(handles.Select_files, 'visible', 'off');
    
    % ... the "Load files for analysis" button is shown in the GUI
    set(handles.Load_files, 'visible', 'on');
    
end

% if Multicell Multitrack analysis has been chosen ...
if strcmp (usersvalues.analysis_type, 'Multicell Multitrack') == 1
    
    % ... the "Select files for analysis" button is shown in the GUI
    set(handles.Select_files, 'visible', 'on');
    
    % ... and the "Load files for analysis" button is not shown in the GUI
    % (since loading is done automatically)
    set(handles.Load_files, 'visible', 'off');    
    
end
                 
%% Reads out the number of Z slices entered in the GUI
function Zslices_Callback(hObject, eventdata, handles)
% hObject    handle to Zslices (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Zslices as text
%        str2double(get(hObject,'String')) returns contents of Zslices as a double
global usersvalues

usersvalues.Nz = str2num(get(hObject,'String'));

%% Reads out the Z distance entered in the GUI
function Zdist_Callback(hObject, eventdata, handles)
% hObject    handle to Zdist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Zdist as text
%        str2double(get(hObject,'String')) returns contents of Zdist as a double
global usersvalues

usersvalues.pixZ = str2num(get(hObject,'String'));

%% Reads out the interval time entered in the GUI 
function inttime_Callback(hObject, eventdata, handles)
% hObject    handle to inttime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of inttime as text
%        str2double(get(hObject,'String')) returns contents of inttime as a double
global usersvalues

usersvalues.dt = str2num(get(hObject,'String'));

%% Reads out the status of the "With PFS" check box in the GUI
% --- Executes on button press in PFS.
function PFS_Callback(hObject, eventdata, handles)
% hObject    handle to PFS (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of PFS

%% Reads out the tracked channel entered in the GUI 
% --- Executes on selection change in trackchannel.
function trackchannel_Callback(hObject, eventdata, handles)
% hObject    handle to trackchannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns trackchannel contents as cell array
%        contents{get(hObject,'Value')} returns selected item from trackchannel
global usersvalues

usersvalues.trackchannel = get(hObject,'Value');

%% Reads out the XY pixel size entered in the GUI
function pixxy_Callback(hObject, eventdata, handles)
% hObject    handle to pixxy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pixxy as text
%        str2double(get(hObject,'String')) returns contents of pixxy as a double
global usersvalues
usersvalues.pixxy = str2double(get(hObject,'String'));





%%
%%=========================================================================
%                       Program functions (buttons)
%%=========================================================================
%%




%% Calculates and plots only XY tracking ("Run XY tracking" button)
% --- Executes on button press in xytracking.
function xytracking_Callback(hObject, eventdata, handles)
% hObject    handle to xytracking (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Curdata
global Filenames

set(handles.ResultsPrinter,'String', 'Only XY tracking performed') %every time only XY tracking is started by clicking the "Run XY tracking" button this is shown in the window in the upper right of the TRAC window

[Xsub,Ysub] = XYtrackingfit(Curdata.x,Curdata.y,Filenames.trackedmovie,Curdata.frames(1));

if Xsub==0  %if Xsub is 0 this means that analysis has been aborted during XY tracking; needed to stop "TRAC" function
    set(handles.ResultsPrinter,'String', 'Analysis has been canceled during XY tracking') %message is shown in the upper right of the TRAC window
    return %function is left 
end

Curdata.xsub=Xsub;
Curdata.ysub=Ysub;

plot(handles.XY_axes,Curdata.xsub,Curdata.ysub);
hold(handles.XY_axes,'on');plot(handles.XY_axes,Curdata.xsub(1),Curdata.ysub(1),'*r');hold(handles.XY_axes,'off');
%labels the axes of the "XY Tracking with fit plot"
set(get(handles.XY_axes,'XLabel'),'String','relative X position (�m)'); 
set(get(handles.XY_axes,'YLabel'),'String','relative Y position (�m)'); 





%% Calculates and plots only Z tracking ("Run Z tracking" button)
% --- Executes on button press in ztracking.
function ztracking_Callback(hObject, eventdata, handles)
% hObject    handle to ztracking (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Curdata
global Filenames
global usersvalues

usersvalues.Nz=str2double(get(handles.Zslices,'String'));

%if isempty (Filenames.trackedmovieZstack)==1

[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the tracked movie NOT PROJECTED');
Filenames.trackedmovieZstack=char(strcat(PathName,FileName));
[Z,Zsub,Index_corrected] = TDtrackingfit(Curdata.xsub,Curdata.ysub,Filenames.trackedmovieZstack,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end));

%else
 %   [Z,Zsub,Index_corrected] = TDtrackingfit(Curdata.xsub,Curdata.ysub,Filenames.trackedmovieZstack,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end));
%end

Curdata.zsub=Zsub;
Curdata.z=Z;

usersvalues.dt=str2double(get(handles.inttime,'String'));
Curdata.time=(Curdata.frames-Curdata.frames(1)).*usersvalues.dt;

plot(handles.Z_axes,Curdata.time,Curdata.zsub); 
hold(handles.Z_axes,'on');plot(handles.Z_axes,Curdata.time(Index_corrected),Curdata.zsub(Index_corrected),'*r');hold(handles.Z_axes,'off');
set(get(handles.Z_axes,'XLabel'),'String','relative time (s)');
set(get(handles.Z_axes,'YLabel'),'String','relative Z position (�m)');
%labels the axes of the "Z Tracking with fit" plot


usersvalues.pixxy=str2double(get(handles.pixxy,'String'));
usersvalues.pixZ=str2double(get(handles.pixz,'String'));
Curdata.speed= instspeed(Curdata.time,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.pixxy,usersvalues.pixZ);





%% Calculates and plots only cross-correlation ("Run colocalization analysis" button)
% --- Executes on button press in CCanalysis.
function CCanalysis_Callback(hObject, eventdata, handles)
% hObject    handle to CCanalysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
parpool;
global Curdata
global Filenames
global usersvalues

usersvalues.twoD=get(handles.only2D,'Value');
usersvalues.new3D=get(handles.new3D,'Value');

[FileName,PathName,FilterIndex] = uigetfile('.mat','Select the transformation structure resulting from calibration');
Filenames.transf=char(strcat(PathName,FileName));
transf=load(Filenames.transf);
    fn=fieldnames(transf); %if .mat file
    transf=transf.(fn{1});
    
if usersvalues.twoD==1
    [FileName,PathName,FilterIndex] = uigetfile('.tif','Select the tracked movie');
Filenames.trackedmovie=char(strcat(PathName,FileName));

[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the NON-tracked movie');
Filenames.untrackedmovie=char(strcat(PathName,FileName));

[Max,Pos] = CContraj2D_trans(Filenames.trackedmovie,Filenames.untrackedmovie,Curdata.xsub,Curdata.ysub,Curdata.frames(1),Curdata.frames(end),transf);

Curdata.maxcc=Max;
Curdata.ccposx=Pos(:,1);
Curdata.ccposy=Pos(:,2);
Curdata.z=zeros(length(Curdata.x),1);
Curdata.zsub=zeros(length(Curdata.x),1);
%Calculate speed
usersvalues.dt=str2double(get(handles.inttime,'String'));
usersvalues.pixxy=str2double(get(handles.pixxy,'String'));
Curdata.time=(Curdata.frames-Curdata.frames(1)).*usersvalues.dt;
Curdata.speed= instspeed_2D(Curdata.time,Curdata.xsub,Curdata.ysub,usersvalues.pixxy);
    
else
usersvalues.Nz=str2double(get(handles.Zslices,'String'));

[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the tracked movie NOT PROJECTED');
Filenames.trackedmovieZstack=char(strcat(PathName,FileName));

[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the NON-tracked movie NOT PROJECTED');
Filenames.untrackedmovieZstack=char(strcat(PathName,FileName));

 if usersvalues.new3D==1
    disp('new 3D CC analysis');
    [Max,Pos,Maxrand,indoutofZ] = CC3D_trans(Filenames.trackedmovieZstack,Filenames.untrackedmovieZstack,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end),transf);
    Curdata.maxcc=Max;
    Curdata.maxccrand=Maxrand;
    Curdata.ccposx=Pos(:,1);
    Curdata.ccposy=Pos(:,2);
    Curdata.ccposz=Pos(:,3);
    
        if  isempty(indoutofZ) 
            set(handles.ResultsPrinter,'String','');
        else
            nonfitZ=char();
            for h=1:length(indoutofZ)
            nonfitZ=strcat(nonfitZ, ' ', num2str(indoutofZ(h)), ' ', ',');
            end
        set(handles.ResultsPrinter,'String',horzcat('Z position of CCmax could not be fitted for frame(s):', nonfitZ));
        end    
        
 else
[Max,Pos] = CContraj3D_trans(Filenames.trackedmovieZstack,Filenames.untrackedmovieZstack,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.Nz,Curdata.frames(1),Curdata.frames(end),transf);

Curdata.maxcc=Max;
Curdata.ccposx=Pos(:,1);
Curdata.ccposy=Pos(:,2);
 end
end

%Ivo's stuff:
plot(handles.CCmax_coordinates_axes,Curdata.time,Curdata.ccposx,'.c', Curdata.time,Curdata.ccposy,'.m', Curdata.time,Curdata.ccposz,'.k'); ylim(handles.CCmax_coordinates_axes,[-10 10]);
legend ({'X', 'Y', 'Z'}, 'FontSize', 6, 'Location', 'NorthWest')
%Curdata.ccposx is a vector containing the subpixel accurate x coordinate of the maximum of the the cross-correlation between 
%the stacks of channel 1 and channel 2 
%contains one value for each stack
%plotted against the time since the beginning of the movie (only values
%between -10 and 10 are shown in the graph)
%plotted in the lower "Cross-correlation colocalization analysis" window
set(get(handles.CCmax_coordinates_axes,'XLabel'),'String','relative time (s)');
set(get(handles.CCmax_coordinates_axes,'YLabel'),'String', sprintf ('coordinates of max. of ICC \n of tracked particles'));
%sets the labels of the axes of the lower "Cross-correlation colocalization analysis" plot 


plot(handles.CCmax_axes,Curdata.time,Curdata.maxcc,'.');ylim(handles.CCmax_axes,[0 1]);
set(get(handles.CCmax_axes,'XLabel'),'String','relative time (s)');
set(get(handles.CCmax_axes,'YLabel'),'String', sprintf ('max. value of ICC \n of tracked particles'));
%sets the labels of the axes of the upper "Cross-correlation colocalization analysis" plot 

%Aurelie's stuff:
%plot(handles.CCmax_coordinates_axes,Curdata.time,Curdata.ccposx,'.');ylim(handles.CCmax_coordinates_axes,[-10 10]);
%hold(handles.CCmax_coordinates_axes,'on');plot(handles.CCmax_coordinates_axes,Curdata.time,Curdata.ccposy,'.r');hold(handles.CCmax_coordinates_axes,'off');
delete (gcp) %shuts down parallel pool gcp = get current parallel pool





%% Calculates and plots only intensity ("Measure intensity" button) 
% --- Executes on button press in Intensity.
function Intensity_Callback(hObject, eventdata, handles)
% hObject    handle to Intensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Curdata
global Filenames
global usersvalues
%creates the global variable "uservalues"; no values saved into here 



[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the tracked projected movie');
Filenames.trackedmovie=char(strcat(PathName,FileName));
%creates the field "trackedmovie" in the variable "Filenames" and saves the
%path to the tracked projected movie into it

[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the NON-tracked projected movie');
Filenames.untrackedmovie=char(strcat(PathName,FileName));
%creates the field "untrackedmovie" in the variable "Filenames" and saves the
%path to the non-tracked projected movie into it

[FileName,PathName,FilterIndex] = uigetfile('.mat','Select the transformation structure resulting from calibration');
Filenames.transf=char(strcat(PathName,FileName));
%creates the field "transf" in the variable "Filenames" and saves the
%path to the transformation matrix into it


transf=load(Filenames.transf);
    fn=fieldnames(transf); %if .mat file
    %saves the data stored in the transformation matrix in a cell array of
    %strings called "fn"
    transf=transf.(fn{1});
    %calls to first row in the cell array "fn" and stores it in the
    %variable "transf"
    
[Int1_max,Int1_cor,Int2_max,Int2_cor] = Intwithcal(Curdata.xsub,Curdata.ysub,Filenames.trackedmovie,Filenames.untrackedmovie,transf,Curdata.frames(1));
%calls the function "Intwithcal" and gives into it the x and y position of
%the tracked particles with subpixel accuracy, the tracked movie, the
%untracked movie, the transformation matrix and 
usersvalues.trackchannel=get(handles.trackchannel,'Value');
%checks which channel has been selected for the tracking 
if usersvalues.trackchannel==1
Curdata.intGmax=Int1_max';
Curdata.intGcorrected=Int1_cor(:,1);
Curdata.intGbackground=Int1_cor(:,2);
Curdata.intRmax=Int2_max';
Curdata.intRcorrected=Int2_cor(:,1);
Curdata.intRbackground=Int2_cor(:,2);
else
Curdata.intGmax=Int2_max';
Curdata.intGcorrected=Int2_cor(:,1);
Curdata.intGbackground=Int2_cor(:,2);
Curdata.intRmax=Int1_max';
Curdata.intRcorrected=Int1_cor(:,1);
Curdata.intRbackground=Int1_cor(:,2);
end


usersvalues.PFS=get(handles.PFS,'Value'); 
%check if "With PFS" is checked
if usersvalues.PFS == 1;
usersvalues.firstFrame=str2double(get(handles.firstframe,'String'));
usersvalues.trackedFrames=str2double(get(handles.trackedframes,'String'));

usersvalues.dt = timewithpsf(usersvalues.Nz, usersvalues.firstFrame, usersvalues.trackedFrames);

else
usersvalues.dt=str2double(get(handles.inttime,'String'));
%returns the interval time set by the user and saves it into a
%double-precision representation which is stored in uservalues.dt
end

Curdata.time=(Curdata.frames-Curdata.frames(1)).*usersvalues.dt;

plot(handles.intensity_axes,Curdata.time,Curdata.intGcorrected,'+g');
hold(handles.intensity_axes,'on');plot(handles.intensity_axes,Curdata.time,Curdata.intRcorrected,'.r'); hold(handles.intensity_axes,'off');

%sets the labels of the axes of the "Intensity" plot 
set(get(handles.intensity_axes,'XLabel'),'String','relative time (s)');
set(get(handles.intensity_axes,'YLabel'),'String','background-corrected intensity');





%% Generates the transformation matrix ("Generate transformation matrix" button)
% --- Executes on button press in Transfmatrix.
function Transfmatrix_Callback(hObject, eventdata, handles)
% hObject    handle to Transfmatrix (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% THIS FUNCTION IS WORKING CORRECTLY BUT IT SHOULD BE CLEANED UP AND
% REORGANIZED ON THE LONG TERM

global Filenames
global usersvalues 

[FileName_fixed,PathName_fixed] = uigetfile('.tif',...
    'Select disk pattern of fixed channel',...
    usersvalues.defaultPath);
if isequal(FileName_fixed,0)
   disp('No file selected')
   return
end
usersvalues.defaultPath = PathName_fixed;
Pathfixed=fullfile(PathName_fixed,FileName_fixed);

[FileName_shifted,PathName_shifted] = uigetfile('.tif',...
    'Select disk pattern of channel to be shifted',...
    usersvalues.defaultPath);
if isequal(FileName_shifted,0)
   disp('No file selected')
   return
end
usersvalues.defaultPath = PathName_shifted;
Pathshifted=fullfile(PathName_shifted,FileName_shifted);
PathAllDisks=PathName_fixed;

transf12 = calib4_mass_working(Pathfixed,Pathshifted);
usersvalues.transf = transf12;

[SaveName, SavePath] = uiputfile ('*.mat','Save transformation matrix', fullfile(usersvalues.defaultPath, 'transf12.mat'));
if isequal(SaveName,0) || isequal(SavePath,0)
   disp('Transformation matrix not saved. Operation cancelled.')
   return
end
usersvalues.defaultPath = SavePath;
FullSavePath = fullfile(SavePath, SaveName);
save(FullSavePath,'transf12');
Filenames.transf.name = SaveName;
Filenames.transf.path = SavePath;

disk_pattern_shifted_channel = imread(Pathshifted);
 
shifted_disk_pattern = correct_shift_stack (disk_pattern_shifted_channel, Filenames.transf, 1, 1);
  
shifted_disk_pattern_file_name = fullfile(PathName_fixed, strcat(FileName_fixed(1:end-4), '_shifted', '.tif'));
imwrite(shifted_disk_pattern, shifted_disk_pattern_file_name, 'tif', 'Compression','none');

I1=imread(Pathfixed);
I2=imread(shifted_disk_pattern_file_name);
AlignedMerge = imfuse(I1,I2,'falsecolor','Scaling','joint','ColorChannels',[2 1 0]);
figure('name', 'Merged channels (aligned)');
imshow(AlignedMerge)





%% Gets the paths to the data files needed for the analysis
% --- Executes on button press in Select_files.
function Select_files_Callback(hObject, eventdata, handles)
% hObject    handle to Select_files (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%Get path to:
%1. Movies 
%2. Timestamps 
%3. Track file 
%4. Transfer matrix

%% Clears global variables 

clearvars -global Curdata Filenames movies

%% Gets global variables

global Curdata
global Filenames
global usersvalues
global movies

%% Reads user inputs into GUI 

% "2D" radio button
usersvalues.twoD=get(handles.twoD,'Value');

% "Old 3D" radio button
usersvalues.old_threeD=get(handles.old3D,'Value');

% "New 3D" radio button
usersvalues.new_threeD=get(handles.new3D,'Value');

% "Incl. correction of XY shift" tickbox
usersvalues.XYcorrection=get(handles.XYcorrection,'Value');  

% Selection in Analysis types dropdown menu 
analysis_types = get(handles.analysis_type_dropdown,'Value');

switch analysis_types 
    case 1 % Single track analysis was chosen 
        usersvalues.analysis_type = 'Single track';
    case 2 % Multitrack analysis on single cell was chosen 
        usersvalues.analysis_type = 'Multitrack';
    case 3 % Multicell Multitrack analysis was chosen 
        usersvalues.analysis_type = 'Multicell Multitrack';
end          

% Number of Z slices
usersvalues.Nz=str2double(get(handles.Zslices,'String'));

% Interval time
usersvalues.dt=str2double(get(handles.inttime,'String'));

% PFS
usersvalues.PFS=get(handles.PFS,'Value');

% "Track in:" dropdown menu
usersvalues.trackchannel=get(handles.trackchannel,'Value');

% Distance between the Z planes
usersvalues.pixZ=str2double(get(handles.Zdist,'String'));

% Pixel size
usersvalues.pixxy=str2double(get(handles.pixxy,'String'));

%% Checks if necessary info has been entered in the GUI

if usersvalues.twoD == 1 && usersvalues.dt == 0
    
   % error message if 2D analysis has been selected and no interframe time
   % was given
   msgbox('Time information missing. Please enter the interval time between the frames.', 'Error','error');
   return 
    
end

if (usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1) && usersvalues.dt == 0 && usersvalues.PFS == 0
    
    % error message if neither "With PFS" is checked nor is an interval time given
    msgbox('Time information missing. Either enter the interval time between stacks or select "With PFS".', 'Error','error');
    return
    
end

%% Single and Multitrack_all_analysis analysis

if strcmp (usersvalues.analysis_type, 'Single track') == 1 || strcmp (usersvalues.analysis_type, 'Multitrack') == 1
    
    %% Gets paths to movie file(s) 
    
    % 2D movies
    if usersvalues.twoD == 1
                
        %Tracked 
        [TwoDmovienames_tracked,folderpath_tracked] = uigetfile('.tif',...
            'Select movie file(s) belonging to the TRACKED channel (if multiple files are selected MIND THE ORDER)',...
            'MultiSelect', 'on',...
            usersvalues.defaultPath);
              
        if isfloat(TwoDmovienames_tracked) 
            msgbox('No file for the tracked movie selected', 'Error','error');
            return
        end
        usersvalues.defaultPath = folderpath_tracked;
        Filenames.TwoD_movie_tracked.name = TwoDmovienames_tracked;
        Filenames.TwoD_movie_tracked.path = folderpath_tracked;

        %Untracked
        [TwoDmovienames_untracked,folderpath_untracked] = uigetfile('.tif',...
            'Select movie file(s) belonging to the UNTRACKED channel (if multiple files are selected MIND THE ORDER)',...
            'MultiSelect', 'on',...
            usersvalues.defaultPath);    

        if isfloat(TwoDmovienames_untracked) 
            msgbox('No file for the untracked movie selected', 'Error','error');
            return
        end
        usersvalues.defaultPath = folderpath_untracked;
        Filenames.TwoD_movie_untracked.name = TwoDmovienames_untracked;
        Filenames.TwoD_movie_untracked.path = folderpath_untracked;        

    end
    
    
    % Z stack movies
    if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1
                
        %Tracked
        [ThreeDmoviesnames_tracked,folderpath_tracked] = uigetfile('.tif',...
            'Select movie file(s) belonging to the TRACKED channel (if multiple files are selected MIND THE ORDER)',...
            'MultiSelect', 'on',...
            usersvalues.defaultPath);
        
        if isfloat(ThreeDmoviesnames_tracked) 
            msgbox('No file for the tracked movie selected', 'Error','error');
            return
        end
        usersvalues.defaultPath = folderpath_tracked;
        Filenames.non_proj_movie_tracked.name = ThreeDmoviesnames_tracked;
        Filenames.non_proj_movie_tracked.path = folderpath_tracked;

        %Untracked
        [ThreeDmoviesnames_untracked,folderpath_untracked] = uigetfile('.tif',...
            'Select movie file(s) belonging to the UNTRACKED channel (if multiple files are selected MIND THE ORDER)',...
            'MultiSelect', 'on',...
            usersvalues.defaultPath);
    
        if isfloat(ThreeDmoviesnames_untracked) 
            msgbox('No file for the untracked movie selected', 'Error','error');
            return
        end
        usersvalues.defaultPath = folderpath_untracked;
        Filenames.non_proj_movie_untracked.name = ThreeDmoviesnames_untracked;
        Filenames.non_proj_movie_untracked.path = folderpath_untracked;
        
    end 

    %% Gets path to timestamps of PFS
    
    if usersvalues.PFS == 1
    
        [PFS_timings_name,PFS_timings_path] = uigetfile('.txt',...
            'Select timestamps of PFS',...
            usersvalues.defaultPath);

        if isfloat(PFS_timings_name) 
            msgbox('No file containing PFS timings selected', 'Error','error');
            return
        end
        usersvalues.defaultPath = PFS_timings_path;
        Filenames.PFS_timings.name = PFS_timings_name;
        Filenames.PFS_timings.path = PFS_timings_path;

    end
    
    %% Gets path to file containing the tracks 
    
    if strcmp (usersvalues.analysis_type, 'Single track') == 1
        
        [track_name,track_path] = uigetfile('.txt',...
            'Select raw tracked data',...
            usersvalues.defaultPath);
        
        if isfloat(track_name) 
            msgbox('No file containing the raw tracked data selected', 'Error','error');
            return
        end
        usersvalues.defaultPath = track_path;
        Filenames.Track.name = track_name;
        Filenames.Track.path = track_path;
                
    end
    
    if strcmp (usersvalues.analysis_type, 'Multitrack') == 1
        
        [multitrack_file_name,multitrack_file_path] = uigetfile('.xml',...
            'Select multitrack file generated by TrackMate',...
            usersvalues.defaultPath);
        
        if isfloat(multitrack_file_name) 
            msgbox('No file containing the raw tracked data selected', 'Error','error');
            return
        end
        usersvalues.defaultPath = multitrack_file_path;
        Filenames.Track.name = multitrack_file_name;
        Filenames.Track.path = multitrack_file_path;
        
    end
    
    %% Gets path to transformation structure
    
    %if XY correction is selected
    if usersvalues.XYcorrection == 1 

        % Transformation structure
        [FileNameTransfFile,PathTransfFile] = uigetfile('.mat',...
            'Select the transformation structure resulting from calibration',...
            usersvalues.defaultPath);
        
        if isfloat(FileNameTransfFile) 
            msgbox('No file containing the transformation strucutre selected', 'Error','error');
            return
        end
        usersvalues.defaultPath = PathTransfFile;
        Filenames.transf.name = FileNameTransfFile;
        Filenames.transf.path = PathTransfFile;
        %pause(5);

    end   
    
    %% Gets path to result folder
    
    %select folder in which results should be saved 
    usersvalues.result_location=uigetdir (usersvalues.defaultPath,'Please select the folder for results');
    
    if isfield (usersvalues,'result_location') == 0 
       msgbox('No directory for the results selected', 'Error','error');
       return
    end
    
end

%% Multicell Multitrack_all_analysis

if strcmp (usersvalues.analysis_type, 'Multicell Multitrack') == 1

    global batch_data_base
        
    cancel_indicator = 0;
    
    counter = 0;
    
    while cancel_indicator == 0

        %% Gets paths to movie file(s) 

        % 2D movies
        if usersvalues.twoD == 1

            %Tracked 
            [TwoDmovienames_tracked,folderpath_tracked] = uigetfile('.tif',...
                'Select movie file(s) belonging to the TRACKED channel (if multiple files are selected MIND THE ORDER)',...
                'MultiSelect', 'on',...
                usersvalues.defaultPath);

            if isfloat(TwoDmovienames_tracked) 
                msgbox('File loading has been finished', 'Error','error');
                cancel_indicator = 1;
                return
            end        
            usersvalues.defaultPath = folderpath_tracked;
            Filenames.TwoD_movie_tracked.name = TwoDmovienames_tracked;
            Filenames.TwoD_movie_tracked.path = folderpath_tracked;


            %Untracked
            [TwoDmovienames_untracked,folderpath_untracked] = uigetfile('.tif',...
            'Select movie file(s) belonging to the UNTRACKED channel (if multiple files are selected MIND THE ORDER)',...
            'MultiSelect', 'on',...
            usersvalues.defaultPath);    

            if isfloat(TwoDmovienames_untracked) 
                msgbox('File loading has been finished', 'Error','error');
                cancel_indicator = 1;
                return
            end
            usersvalues.defaultPath = folderpath_untracked;
            Filenames.TwoD_movie_untracked.name = TwoDmovienames_untracked;
            Filenames.TwoD_movie_untracked.path = folderpath_untracked;        

        end


        % Z stack movies
        if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1

            %Tracked
            [ThreeDmoviesnames_tracked,folderpath_tracked] = uigetfile('.tif',...
                'Select movie file(s) belonging to the TRACKED channel (if multiple files are selected MIND THE ORDER)' ,...
                'MultiSelect', 'on',...
                usersvalues.defaultPath);

            if isfloat(ThreeDmoviesnames_tracked) 
                msgbox('File loading has been finished', 'Error','error');
                cancel_indicator = 1;
                return
            end
            usersvalues.defaultPath = folderpath_tracked;
            Filenames.non_proj_movie_tracked.name = ThreeDmoviesnames_tracked;
            Filenames.non_proj_movie_tracked.path = folderpath_tracked;


            %Untracked
            [ThreeDmoviesnames_untracked,folderpath_untracked] = uigetfile('.tif',...
                'Select movie file(s) belonging to the UNTRACKED channel (if multiple files are selected MIND THE ORDER)',...
                'MultiSelect', 'on',...
                usersvalues.defaultPath);

            if isfloat(ThreeDmoviesnames_untracked) 
                msgbox('File loading has been finished', 'Error','error');
                cancel_indicator = 1;
                return
            end
            usersvalues.defaultPath = folderpath_untracked;
            Filenames.non_proj_movie_untracked.name = ThreeDmoviesnames_untracked;
            Filenames.non_proj_movie_untracked.path = folderpath_untracked;

        end 

        %% Gets path to timestamps of PFS

        if usersvalues.PFS == 1

            [PFS_timings_name,PFS_timings_path] = uigetfile('.txt',...
                'Select timestamps of PFS',...
                usersvalues.defaultPath);

            if isfloat(PFS_timings_name) 
                msgbox('File loading has been finished', 'Error','error');
                cancel_indicator = 1;
                return
            end
            usersvalues.defaultPath = PFS_timings_path;
            Filenames.PFS_timings.name = PFS_timings_name;
            Filenames.PFS_timings.path = PFS_timings_path;
            pause(1);

        end

        %% Gets path to file containing the tracks 

        % THE FOLLOWING CAN BE DELETED
        if strcmp (usersvalues.analysis_type, 'Single track') == 1

            [track_name,track_path] = uigetfile('.txt',...
                'Select raw tracked data',...
                usersvalues.defaultPath);

            if isfloat(track_name) 
                msgbox('File loading has been finished', 'Error','error');
                cancel_indicator = 1;
                return
            end
            usersvalues.defaultPath = track_path;
            Filenames.Track.name = track_name;
            Filenames.Track.path = track_path;

        end

        % MULTITRACK CAN BE DELETED
        if strcmp (usersvalues.analysis_type, 'Multitrack') == 1 || strcmp (usersvalues.analysis_type, 'Multicell Multitrack') == 1

            [multitrack_file_name,multitrack_file_path] = uigetfile('.xml',...
                'Select multitrack file generated by TrackMate',...
                usersvalues.defaultPath);

            if isfloat(multitrack_file_name) 
                msgbox('File loading has been finished', 'Error','error');
                cancel_indicator = 1;
                return
            end
            usersvalues.defaultPath = multitrack_file_path;
            Filenames.Track.name = multitrack_file_name;
            Filenames.Track.path = multitrack_file_path;
            pause(1);
            
        end

        %% Gets path to transformation structure

        %if XY correction is selected
        if usersvalues.XYcorrection == 1 

            % Transformation structure
            [FileNameTransfFile,PathTransfFile] = uigetfile('.mat',...
                'Select the transformation structure resulting from calibration',...
                usersvalues.defaultPath);

            if isfloat(FileNameTransfFile) 
                msgbox('File loading has been finished', 'Error','error');
                cancel_indicator = 1;
                return
            end
            usersvalues.defaultPath = PathTransfFile;
            Filenames.transf.name = FileNameTransfFile;
            Filenames.transf.path = PathTransfFile;

        end   

        %% Gets path to result folder

        %select folder in which results should be saved 
        usersvalues.result_location=uigetdir (usersvalues.defaultPath,'Please select the folder for results');

        if isfield (usersvalues,'result_location') == 0 
           msgbox('File loading has been finished', 'Error','error');
           cancel_indicator = 1;
           return
        end
        
        %% Counts number of loaded cells 

         counter = counter + 1;

        %% Saves the data belonging to one track 
    
    %     first_row = batch_data_base(1,:);
    %     first_free_entry = find(cellfun(@isempty,first_row),1)
    %     %first_free_entry = find(~first_row, 1);
    %     batch_data_base (1, first_free_entry) = Filenames;
    %     batch_data_base (2, first_free_entry) = usersvalues;
    % %     batch_data_base (1,(length(batch_data_base(1,:)+1))) = Filenames;
    % %     batch_data_base (2,(length(batch_data_base(1,:)+1))) = usersvalues;
          cell_number = num2str(counter);
          field_name = strcat('Cell_', cell_number);
          batch_data_base.(field_name).Filenames = Filenames;
          batch_data_base.(field_name).usersvalues = usersvalues;
        
    end
end 





%% Loads all files needed for the analysis of one cell ("Load files for analysis" button)
% --- Executes on button press in Load_files.
function Load_files_Callback(hObject, eventdata, handles, varargin)
% hObject    handle to Load_files (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Loads global variables

global Filenames
global usersvalues
global movies

%% Reads in user inputs into GUI 

usersvalues.twoD=handles.twoD.Value; %contains the status of the "2D" tickbox
usersvalues.old_threeD=get(handles.old3D,'Value'); %contains the status of the "Old 3D" tickbox
usersvalues.new_threeD=get(handles.new3D,'Value'); %contains the status of the "New 3D" tickbox
usersvalues.XYcorrection=get(handles.XYcorrection,'Value'); %contains the status of the "Incl. correction of XY shift" tickbox
usersvalues.first_stack_frame_removal = get(handles.stack_frame_removal,'Value'); %contains the status of the "Remove 1st stack + 1st frame of every stack" tickbox

%% Checks if user inputs make sense 

% THIS DOESN'T NEED TO BE CHECKED HERE BUT BEFORE STARTING THE ANALYSIS
% (LEAVE IT FOR NOW)
usersvalues.pixxy=str2double(get(handles.pixxy,'String')); % pixel size

% Error message pixel size
if usersvalues.pixxy == 0
   msgbox('Please enter the pixel size before loading the files.', 'Error','error');
   return
end

%Reads in number of Z slices if a 3D analysis has been chosen 
if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1 
    
    usersvalues.Nz=str2double(get(handles.Zslices,'String')); %Z slices

    %Error message if the number of Z slices has not been entered in the GUI
    if usersvalues.Nz == 0
       msgbox('Please enter the number of Z slices recorded per stack before loading the files for a 3D analysis.', 'Error','error');
       return
    end
    
    %distance between the Z planes
    usersvalues.pixZ=str2double(get(handles.Zdist,'String'));
    %error message
    if usersvalues.pixZ==0
       msgbox('Please enter the distance between Z planes before loading the files for a 3D analysis.', 'Error','error');
       return
    end
    
end

%% Checks what kind of analysis type was chosen by the user 

% Selection in Analysis types dropdown menu 
analysis_types = get(handles.analysis_type_dropdown,'Value');

switch analysis_types 
    case 1 % Single track analysis was chosen 
        usersvalues.analysis_type = 'Single track';
    case 2 % Multitrack analysis on single cell was chosen 
        usersvalues.analysis_type = 'Multitrack';
    case 3 % Multicell Multitrack analysis was chosen 
        usersvalues.analysis_type = 'Multicell Multitrack';
end          

% if either the Single track or the Multitrack analysis has been chosen ...
if strcmp (usersvalues.analysis_type, 'Single track') == 1 || strcmp (usersvalues.analysis_type, 'Multitrack') == 1

    % ... the files are selected here.
    Select_files_Callback(hObject, eventdata, handles)
    
end

%% Checks whether all files needed have been selected 

% Movies
    
     % 2D movies
    if usersvalues.twoD == 1
        
       if isfield (Filenames, 'TwoD_movie_tracked') == 0 || isfield (Filenames, 'TwoD_movie_untracked') == 0
           
           return
            
       end
       
    end
                    
    % Z stack movies
    if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1
         
       if isfield (Filenames, 'non_proj_movie_tracked') == 0 || isfield (Filenames, 'non_proj_movie_untracked') == 0
           
           return
            
       end
       
    end
    
    
% PFS time stamps 

    if usersvalues.PFS == 1
        
       if isfield (Filenames, 'PFS_timings') == 0
           
           return
           
       end
       
    end
    
    
% Tracks

    if isfield (Filenames, 'Track') == 0
           
        return
           
    end
    
    
% Transformation strucuture

    if usersvalues.XYcorrection == 1 
    
        if isfield (Filenames, 'transf') == 0
           
            return
           
        end

    end

    
% Results folder 

    if isfield (usersvalues,'result_location') == 0
        
        return
        
    end    

%% Sets info box

switch nargin

    case 3 % Loading starting message for Single track and Multitrack analysis 
        
        loading_start_msg = 'Loading of files ongoing ...';
        
        disp(loading_start_msg);
        set(handles.ResultsPrinter,'String', loading_start_msg);
        pause(1);       

    case 5 % Loading starting message for Multicell Multitrack analysis 
        
        number_current_cell = int2str(varargin{1});
    
        total_number_of_cells = int2str(varargin{2});
        
        loading_start_msg = strcat({'Loading of files of cell '}, number_current_cell, {' of '}, total_number_of_cells, {' ongoing ...'});
        
        disp(loading_start_msg);
        set(handles.ResultsPrinter,'String', loading_start_msg);
        pause(1);
        
end

%% Loads movies

%if 2D analysis is selected ...
if usersvalues.twoD == 1
    
    % ... 2D movies are loaded in the ...
    
    % ... tracked channel ...
    disp('Loading of movie in the tracked channel ongoing');
    movies.TwoD_movie_tracked = Load2Dmovie (Filenames.TwoD_movie_tracked.name, Filenames.TwoD_movie_tracked.path); 
    
    % ... and the untracked channel
    disp('Loading of movie in the untracked channel ongoing');
    movies.TwoD_movie_untracked = Load2Dmovie (Filenames.TwoD_movie_untracked.name, Filenames.TwoD_movie_untracked.path);

    % crops movies to same length if necessary
    if length(movies.TwoD_movie_tracked(1,1,:)) ~= length(movies.TwoD_movie_untracked(1,1,:))
       
        [movies.TwoD_movie_tracked, movies.TwoD_movie_untracked] = crop_movies_to_same_length (movies.TwoD_movie_tracked, movies.TwoD_movie_untracked);
    
    end

end 

%if a 3D analysis is selected ...
if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1 

    % the non-projected movies are loaded in the ...
    
    % ... tracked channel ...
    disp('Loading of non-projected movie in the tracked channel ongoing');
    movies.non_proj_movie_tracked = LoadNonProjectedMovie (usersvalues.Nz, Filenames.non_proj_movie_tracked.name, Filenames.non_proj_movie_tracked.path, usersvalues.first_stack_frame_removal);

    % ... and the untracked channel ...
    disp('Loading of non-projected movie in the untracked channel ongoing');
    movies.non_proj_movie_untracked = LoadNonProjectedMovie (usersvalues.Nz, Filenames.non_proj_movie_untracked.name, Filenames.non_proj_movie_untracked.path, usersvalues.first_stack_frame_removal);

    % crops movies to same length if necessary
    if length(movies.non_proj_movie_tracked(1,1,:)) ~= length(movies.non_proj_movie_untracked(1,1,:))
       
       [movies.non_proj_movie_tracked, movies.non_proj_movie_untracked] = crop_movies_to_same_length (movies.non_proj_movie_tracked, movies.non_proj_movie_untracked);
       
    end

    % ... and the projected movies are created 
    disp('Projection of movie in the tracked channel ongoing');
    movies.proj_movie_tracked = CreateProjectedMovie(movies.non_proj_movie_tracked, usersvalues.Nz, usersvalues.first_stack_frame_removal);

    disp('Projection of movie in the untracked channel ongoing');
    movies.proj_movie_untracked = CreateProjectedMovie(movies.non_proj_movie_untracked, usersvalues.Nz, usersvalues.first_stack_frame_removal);

end

disp('Movies loaded');

%% Loads time info

if  usersvalues.PFS == 1  
    
    %loads time stamps generated by the PFS 
    usersvalues.dt = timewithpfs(Filenames.PFS_timings.name, Filenames.PFS_timings.path, usersvalues.Nz, usersvalues.first_stack_frame_removal);
    
    %Inputs:
    %usersvalues.Nz: Number of Z planes inserted in the GUI
    
    %Outputs:
    %uservalues.dt: Time stamps [s] passed before each stack/each frame of the projected movie

else
    
    %reads out the interval time entered in the GUI
    usersvalues.dt=str2double(get(handles.inttime,'String'));
           
end

disp('Time info loaded');

%% Loads raw track data

if strcmp (usersvalues.analysis_type, 'Single track') == 1

    [single_track_data] = LoadTrack(Filenames.Track.name, Filenames.Track.path);

    usersvalues.track_data = single_track_data;

end

if strcmp (usersvalues.analysis_type, 'Multitrack') == 1 || strcmp (usersvalues.analysis_type, 'Multicell Multitrack') == 1

    [multiple_tracks] = load_multitrack_file(Filenames.Track.name, Filenames.Track.path);

    usersvalues.track_data = multiple_tracks;
    
end

disp('Raw track data loaded');

%% Loads transfer matrix

if usersvalues.XYcorrection == 1

    transf = LoadTransfStructure(Filenames.transf.name, Filenames.transf.path);

    usersvalues.transf = transf;

    disp('Transfer matrix loaded');
    
end

%% Sets info box

disp('Files loaded');
set(handles.ResultsPrinter,'String', 'Files loaded')    





%% Opens result files ("Open analysis" button) 
% --- Executes on button press in openanalysis.
function openanalysis_Callback(hObject, eventdata, handles)
% hObject    handle to openanalysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% loads global variables
global usersvalues

%% reads user inputs into GUI (ON THE LONG TERM THESE INFOS SHOULD BE SAVED AS META DATA IN THE TRIC TRACK FILE)

% "2D" radio button
usersvalues.twoD=get(handles.twoD,'Value');

% "Old 3D" radio button
usersvalues.old_threeD=get(handles.old3D,'Value');

% "New 3D" radio button
usersvalues.new_threeD=get(handles.new3D,'Value');

% pixel size
usersvalues.pixxy=str2double(get(handles.pixxy,'String'));
if usersvalues.pixxy == 0 %error message
   msgbox('Please enter the pixel size used for the TrIC track(s) to be loaded', 'Error','error');
   return
end

if usersvalues.new_threeD == 1 || usersvalues.old_threeD == 1

    % distance between the Z planes
    usersvalues.pixZ=str2double(get(handles.Zdist,'String'));
    if usersvalues.pixZ==0 % error message
       msgbox('Please enter the distance between the Z planes used for the TrIC track(s) to be loaded', 'Error','error');
       return
    end

end

%% gets the path to the TrIC track containing file

analysis_type_answer = questdlg('Did you select the anlysis type corresponding to the data you want load?','Selected analysis type','Yes','No','Yes');

switch analysis_type_answer
    
    case 'No'
          
        return 
    
    case 'Yes'
        
        % Get filepath and filenames of loading directory/folder
        [filename, filepath, cCheck] = uigetfile('.txt',...
            'Select track files',...
            'Multiselect', 'on',...
            usersvalues.defaultPath);

        assignin('base', 'filename', filename);
        assignin('base', 'filepath', filepath);
        assignin('base', 'cCheck', cCheck);

        % If user did not press cancel
        if (cCheck ~= 0)
            % Reset selection to first entry
            set(handles.listOpeningFiles, 'Value', 1);

            % Show selected file names in the listbox
            set(handles.listOpeningFiles, 'String', filename);
        end

        %saves the path to the selected files 
        usersvalues.defaultPath = filepath;
        usersvalues.trackpath = filepath;

        %writes path to the folder containing the results into window "Current pathname:"
         set(handles.currentpathname,'String',filepath);
     
end

% --- Executes on selection change in listOpeningFiles.
function listOpeningFiles_Callback(hObject, eventdata, handles)
% hObject    handle to listOpeningFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listOpeningFiles contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listOpeningFiles

global Curdata
global Filenames
global usersvalues

% Get string and value of results file
resultsfilename = get(handles.listOpeningFiles, 'String');
resultsfilevalue = get(handles.listOpeningFiles, 'Value');

% Display selected file
if (iscell(resultsfilename))
   fileName = resultsfilename(resultsfilevalue);
   %fprintf('Selected file: %s\n', fileName);
else
    fileName = resultsfilename;
    %fprintf('Selected file: %s\n', fileName);
end  
    Data_path = char(strcat(usersvalues.trackpath,fileName));
    set(handles.currentpathname,'String',usersvalues.trackpath);
    %writes path of selected file into window "Current pathname:"
    Filenames.FileNameOnly=fileName;
    set(handles.showtracknber,'String',Filenames.FileNameOnly);
    data = load(Data_path);

    
%% Reads in the TrIC track data

if usersvalues.new_threeD==1
    
    Curdata.frames=data(:,1);
    %saves the first column of "data" in the field "Curdata.frames"

    Curdata.time=data(:,2);
    %saves the second column of "data" in the field "Curdata.time"; time since
    %the movie was started 

    Curdata.x=data(:,3);
    %saves the third column of "data" in the field "Curdata.x"; x position
    %obtained by manual tracking (pixels)

    Curdata.y=data(:,4);
    %saves the fourth column of "data" in the field "Curdata.y"; y position
    %obtained by manual tracking (pixels)

    Curdata.z=data(:,5);
    %saves the fifth column of "data" in the field "Curdata.z"; z position
    %obtained by manual tracking (pixels) ????

    Curdata.xsub=data(:,6);
    %saves the sixth column of "data" in the field "Curdata.xsub"; x position
    %obtained by 2D gaussian fitting (subpixels accuracy)

    Curdata.ysub=data(:,7);
    %saves the seventh column of "data" in the field "Curdata.ysub"; y position
    %obtained by 2D gaussian fitting (subpixels accuracy)

    Curdata.zsub=data(:,8);
    %saves the eigth column of "data" in the field "Curdata.zsub"; z position
    %obtained by 2D gaussian fitting (subpixels accuracy)

    Curdata.maxcc=data(:,9);
    %saves the ninth column of "data" in the field "Curdata.maxcc";
    %maximal value of the cross-correlation (normalized, comprised between 0 and 1)

    Curdata.maxccrand=data(:,10);
    %saves the ninth column of "data" in the field "Curdata.maxccrand";
    %maximal value of the randomized cross-correlation 

    Curdata.ccposx=data(:,11);
    %saves the eleventh column of "data" in the field "Curdata.ccposx";
    %x position of the cross-correlation maximum (pixels, comprised between -10 and +10)

    Curdata.ccposy=data(:,12);
    %saves the twelvth column of "data" in the field "Curdata.ccposy";
    %y position of the cross-correlation maximum (pixels, comprised between -10 and +10)

    Curdata.ccposz=data(:,13);
    %saves the 13th column of "data" in the field "Curdata.ccposz";
    %z position of the cross-correlation maximum (pixels, comprised between -10 and +10)

    Curdata.intGmax=data(:,14);
    %saves the 14th column of "data" in the field "Curdata.intGmax";
    %maximal intensity in green channel

    Curdata.intGcorrected=data(:,15);
    %saves the 15th column of "data" in the field "Curdata.intGcorrected";
    %background corrected intensity in green channel

    Curdata.intGbackground=data(:,16);
    %saves the 16th column of "data" in the field "Curdata.intGbackground";
    %background intensity in green channel

    Curdata.intRmax=data(:,17);
    %saves the 17th column of "data" in the field "Curdata.intRmax";
    %maximal intensity in red channel

    Curdata.intRcorrected=data(:,18);
    %saves the 18th column of "data" in the field "Curdata.intRcorrected";
    %background corrected intensity in red channel

    Curdata.intRbackground=data(:,19);
    %saves the 19th column of "data" in the field "Curdata.intRbackground";
    %background intensity in red channel

    Curdata.speed=data(:,20);
    %saves the 20th column of "data" in the field "Curdata.speed";
    %instantaneous velocity (�m/s)
    
end

 if usersvalues.old_threeD==1
    
    Curdata.frames=data(:,1);
    Curdata.time=data(:,2);
    Curdata.x=data(:,3);
    Curdata.y=data(:,4);
    Curdata.z=data(:,5);
    Curdata.xsub=data(:,6);
    Curdata.ysub=data(:,7);
    Curdata.zsub=data(:,8);
    Curdata.maxcc=data(:,9);
    Curdata.ccposx=data(:,10);
    Curdata.ccposy=data(:,11);
    Curdata.intGmax=data(:,12);
    Curdata.intGcorrected=data(:,13);
    Curdata.intGbackground=data(:,14);
    Curdata.intRmax=data(:,15);
    Curdata.intRcorrected=data(:,16);
    Curdata.intRbackground=data(:,17);
    Curdata.speed=data(:,18);
    %the same as for "New3D" except that "randomized cross-correlation" and the 
    %"z position of the cross-correlation maximum (pixels, comprised between -10 and +10)
    %are not included" 
    
 end
 
 if usersvalues.twoD == 1 
     
    Curdata.frames=data(:,1);
    Curdata.time=data(:,2);
    Curdata.x=data(:,3);
    Curdata.y=data(:,4);
    Curdata.xsub=data(:,5);
    Curdata.ysub=data(:,6);
    Curdata.maxcc=data(:,7);
    Curdata.ccposx=data(:,8);
    Curdata.ccposy=data(:,9);
    Curdata.intGmax=data(:,10);
    Curdata.intGcorrected=data(:,11);
    Curdata.intGbackground=data(:,12);
    Curdata.intRmax=data(:,13);
    Curdata.intRcorrected=data(:,14);
    Curdata.intRbackground=data(:,15);
    Curdata.speed=data(:,16);
         
 end

%% Plots the XY data 
 
%plots subpixel accurate XY coordinates of track in the GUI in "XY tracking with fit"
plot(handles.XY_axes,Curdata.xsub*usersvalues.pixxy,Curdata.ysub*usersvalues.pixxy); 

%labels the axes of the "XY Tracking with fit"-plot
set(get(handles.XY_axes,'XLabel'),'String','relative X position (�m)'); 
set(get(handles.XY_axes,'YLabel'),'String','relative Y position (�m)');

%% Plots the Z data

% if a 3D annlysis has been chosen plot the Z position
if usersvalues.old_threeD ==1 || usersvalues.new_threeD == 1

    %plots Z coordinates in the "Z Tracking with fit" plot of the GUI and labels the axes
    plot(handles.Z_axes,Curdata.time,Curdata.zsub*usersvalues.pixZ); %plots the substack accurate Z coordinates in �m against the time since the beginning of the track in the window "Z tracking with fit" of the GUI

    %!!!Activate if corrected indices should be indicated!!!
    %hold(handles.Z_axes,'on');plot(handles.Z_axes,Curdata.time(Index_corrected),Curdata.zsub(Index_corrected),'*r');hold(handles.Z_axes,'off'); %Z coordinates that have been corrected are marked with a red star in the plot 
    set(get(handles.Z_axes,'XLabel'),'String','relative time (s)');
    set(get(handles.Z_axes,'YLabel'),'String','relative Z position (�m)');

end

%% Plots the Instantaneous velocity data 

   plot(handles.velocity_axes,Curdata.time,Curdata.speed); 
   legend(handles.velocity_axes, 'visible','off');
   %labels the axes of the "Instantaneous velocity" plot
   set(get(handles.velocity_axes,'XLabel'),'String','relative time (s)'); 
   set(get(handles.velocity_axes,'YLabel'),'String', sprintf ('instantaneous velocity \n of tracked particles (�m/s)'));

%% Plots the maxima of the cross-correlation

% plots the results in the upper "Cross-correlation colocalization analysis" plot
plot(handles.CCmax_axes,Curdata.time,Curdata.maxcc,'.');ylim(handles.CCmax_axes,[0 (max(Curdata.maxcc)+0.05)]);%Aurelie's original: plot(handles.CCmax_axes,Curdata.time,Curdata.maxcc,'.');ylim(handles.CCmax_axes,[0 1]); 
%Curdata.maxcc is a vector containing the maximum value the cross-correlation between 
%the stacks of tracked channel and the not tracked channel; contains one value for each stack
%plotted against the time since the beginning of the track 

%sets the labels of the axes of the upper "Cross-correlation colocalization analysis" plot 
set(get(handles.CCmax_axes,'XLabel'),'String','relative time (s)');
set(get(handles.CCmax_axes,'YLabel'),'String', sprintf ('max. value of ICC \n of tracked particle'));

%% Plots the coordinates of the maxima of the cross-correlation

if usersvalues.old_threeD == 1 || usersvalues.new_threeD == 1

    % plots the refined coordinates of the maximum of the cross-correlation in the lower "Cross-correlation colocalization analysis" window 
    plot(handles.CCmax_coordinates_axes,Curdata.time,Curdata.ccposx,'.c', Curdata.time,Curdata.ccposy,'.m', Curdata.time,Curdata.ccposz,'.k'); ylim(handles.CCmax_coordinates_axes,[-10 10]);
    legend ({'X', 'Y', 'Z'}, 'FontSize', 6, 'Location', 'NorthWest')
    %Curdata.ccposx is a vector containing the subpixel accurate x coordinate of the maximum of the the cross-correlation between 
    %the stacks of the tracked and untracked channel; contains one value for each stack
    %plotted against the time since the beginning of the track (only values between -10 and 10 are shown in the graph)

end

if usersvalues.twoD == 1 
    
    % plots the refined coordinates of the maximum of the cross-correlation in the lower "Cross-correlation colocalization analysis" window 
    plot(handles.CCmax_coordinates_axes,Curdata.time,Curdata.ccposx,'.c', Curdata.time,Curdata.ccposy,'.m'); ylim(handles.CCmax_coordinates_axes,[-10 10]);
    legend ({'X', 'Y'}, 'FontSize', 6, 'Location', 'NorthWest')
    %Curdata.ccposx is a vector containing the subpixel accurate x coordinate of the maximum of the the cross-correlation between 
    %the stacks of the tracked and untracked channel; contains one value for each stack
    %plotted against the time since the beginning of the track (only values between -10 and 10 are shown in the graph)
    
end

%sets the labels of the axes of the lower "Cross-correlation colocalization analysis" plot
set(get(handles.CCmax_coordinates_axes,'XLabel'),'String','relative time (s)');
set(get(handles.CCmax_coordinates_axes,'YLabel'),'String', sprintf ('coordinates of max. of ICC \n of tracked particle (pixels)'));

%% Plots the background-corrected intensity 

plot(handles.intensity_axes,Curdata.time,Curdata.intGcorrected,'+g');
%plots the background corrected intensity in the green channel against the
%time in the window "intensity"
%uses green "crosses" for plotting
hold(handles.intensity_axes,'on');plot(handles.intensity_axes,Curdata.time,Curdata.intRcorrected,'.r'); hold(handles.intensity_axes,'off');
%plots the background corrected intensity in the red channel against the
%time in the window "intensity" without overwriting the previous plot 
%uses red "dots" for plotting    

%sets the labels of the axes of the "Intensity" plot 
set(get(handles.intensity_axes,'XLabel'),'String','relative time (s)');
set(get(handles.intensity_axes,'YLabel'),'String','background-corrected intensity');





%% Calculates and plots only the instantaneous velocity ("Velocity" button)
% --- Executes on button press in velocity.
function velocity_Callback(hObject, eventdata, handles)
% hObject    handle to velocity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%Recalculate the velocity of one or of multiple EXISTING TRAC files
%The speed is in �m/s
%Those newly computed speeds are then saved AUTOMATICALLY, which means that
%they REPLACE the former values for speed.

global Curdata
global Filenames
global usersvalues


[RawFileNames,PathName] = uigetfile('*TRAC.txt','Select analysed data, multiple selection possible:','MultiSelect','on');

% %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% If multiple files are selected, Matlab creates a cell string array,
%whereas it creates a simple char array if only one file is selected. The
%following lines are here to solve this.
% %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

%if single file is selected the variable "RawFileNames" is a character array with the dimensions
%"1 x number of characters of file name"

%if multiple files are selected the variable "RawFileNames" is a cell
%string array with the dimensions of 1 x the number of files selected
%(columns)

[nbl,nbc] = size(RawFileNames);
%saves the dimensions of the array "RawFileNames" in the variables of type double "nbl"
%and "nbc" e.g. for the file name "TrackG1_TRAC Ivo - Kopie.txt" nbl=1 and nbc=28
[nbl1,nbc1] = size(RawFileNames(1));
%the variables "nbl1" and "nbc1" are created and the number of rows in the
%array is saved into it; here: 1 (not sure about that"

if ischar(RawFileNames)
    %if "RawFileNames" is a character array the if loop is entered
    StrBuffer='';
    %creates the variable "StrBuffer" of type "char" and saves "''" into it
    SingleFile=1;
    %creates the variable "SingleFile" of type "double" and saves "1" into it
    for m=1:nbc
        StrBuffer=strcat(StrBuffer,RawFileNames(1,m));
        %For-loop goes from 1 to nbc and saves the value of "RawFileNames"
        %into "StrBuffer"; "StrBuffer" contains file name of analysed data
    end
    nbc=1;
    %"nbc" is reset to "1"
    FileNames{1}=StrBuffer;
    %Cell array "FileNames" is created and and in the first row the value
    %of "StrBuffer" is saved
else
    %if "RawFileNames" is a not a character array the else option is entered
    SingleFile=0;
    %creates the variable "SingleFile" of type "double" and saves "0" into it
    FileNames=RawFileNames;
    %in the cell array "FileNames" the value of "RawFileNames" is saved
end

%Set the boundaries for the following loop
[nbl,nbc] = size(FileNames);
%this value is either "1 x 1" (if only single file was selected) or "1 x number of
%selected files" if multiple files were selected

for i = 1:nbc
    A=char(strcat(PathName,FileNames(i)));
    %'strcat' horizontally concatenates string => path incl file name to file to open   
    Filenames.rawtrack=A;
    Filenames.FileNameOnly=char(FileNames(i));
    %only the name of the file saved in 'FileNames' is saved in the
    %variable 'Filenames.FileNameOnly'
    data=load(A);
    %one file after the other saved in "FileNames" is loaded  
    
    
    
    Curdata.frames=data(:,1);
    Curdata.time=data(:,2);
    Curdata.x=data(:,3);
    Curdata.y=data(:,4);
    Curdata.z=data(:,5);
    Curdata.xsub=data(:,6);
    Curdata.ysub=data(:,7);
    Curdata.zsub=data(:,8);
    Curdata.maxcc=data(:,9);
    Curdata.ccposx=data(:,10);
    Curdata.ccposy=data(:,11);
    Curdata.intGmax=data(:,12);
    Curdata.intGcorrected=data(:,13);
    Curdata.intGbackground=data(:,14);
    Curdata.intRmax=data(:,15);
    Curdata.intRcorrected=data(:,16);
    Curdata.intRbackground=data(:,17);
    Curdata.speed=data(:,18);
    %and the data are saved in the variable "Curdata"
    
    usersvalues.twoD = get(handles.only2D,'Value');
    %checks whether the "only 2D" box is ticked
    usersvalues.pixxy = str2double(get(handles.pixxy,'String'));
    %reads out the value entered in the field 'pixel size xy' and saves it as
    %a double precision in the variable uservalues 
    usersvalues.pixZ = str2double(get(handles.Zdist,'String'));
    %reads out the value entered in the field 'distance between Z slices' and saves it as
    %a double precision in the variable uservalues 
    
    if usersvalues.twoD==1
        % usersvalues.dt=str2double(get(handles.inttime,'String'));
        % Curdata.time=(Curdata.frames-Curdata.frames(1)).*usersvalues.dt;
        Curdata.speed= instspeed_2D(Curdata.time,Curdata.xsub,Curdata.ysub,usersvalues.pixxy);
        set(handles.ResultsPrinter,'String',horzcat('2D Velocity measured for track ',Filenames.FileNameOnly));
        % horzcat: horizontally concatenates arrays
        %field 'ResultsPrinter' is a the top left of the GUI, shows the
        %track analysed
    else
        
        Curdata.speed= instspeed(Curdata.time,Curdata.xsub,Curdata.ysub,Curdata.zsub,usersvalues.pixxy,usersvalues.pixZ);
        set(handles.ResultsPrinter,'String',horzcat('3D Velocity measured for track ',Filenames.FileNameOnly));
    end
    
    finaldata=[Curdata.frames Curdata.time Curdata.x Curdata.y Curdata.z ...
        Curdata.xsub Curdata.ysub Curdata.zsub ...
        Curdata.maxcc Curdata.ccposx Curdata.ccposy ...
        Curdata.intGmax Curdata.intGcorrected Curdata.intGbackground ...
        Curdata.intRmax Curdata.intRcorrected Curdata.intRbackground ...
        Curdata.speed];

    dlmwrite(A,finaldata,'delimiter','\t','precision',6);
    %writes the matrix 'finaldata' in the file specified under path 'A' 
    %(meaning it overwrites the values saved there before using tabs as delimiters and
    %six significant digits as the precision 
end
%Reduces the pathname to the two last directories
%Finds the '\' in PathName
SlashPosition = strfind(PathName, '\');
%Split the string and keep the part after the second '\' from the right
PathNameShkd=PathName(SlashPosition(length(SlashPosition)-2)+1 :end);
%Supresses '\' at the end of the name
PathNameShkd=PathNameShkd(1 :end-1);

%prints message in the 'ResultsPrinter' field in the GUI
if SingleFile==0
    set(handles.ResultsPrinter,'String',horzcat('New speeds saved for all selected tracks in ',PathNameShkd));
else
    set(handles.ResultsPrinter,'String',horzcat('New speeds saved for this track only: ',Filenames.FileNameOnly));
end





%% Runs the selected analysis
% --- Executes on button press in Run_analysis.
function Run_analysis_Callback(hObject, eventdata, handles)
% hObject    handle to Run_analysis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Loads global variables

global Curdata
global Filenames
global usersvalues
global movies

%% Runs analysis
if strcmp (usersvalues.analysis_type, 'Single track') == 1
    
   %Single_track_all_analysis(single_track_data, hObject, eventdata, handles, varargin) 
   Single_track_all_analysis(usersvalues.track_data, hObject, eventdata, handles) 
   
end

if strcmp (usersvalues.analysis_type, 'Multitrack') == 1
    
    %Multitrack_all_analysis(hObject, eventdata, handles, varargin)
    %Multitrack_all_analysis(hObject, eventdata, handles, varargin{1}, varargin{2})
    Multitrack_all_analysis(hObject, eventdata, handles)
    
end

if strcmp (usersvalues.analysis_type, 'Multicell Multitrack') == 1
    
    Multicell_Multitrack_all_analysis(hObject, eventdata, handles)
    
end





%% Plots results of analysis ("show full plot" button) 

% --- Executes on button press in fullplot.
function fullplot_Callback(hObject, eventdata, handles)
% hObject    handle to fullplot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Curdata
global Filenames
global usersvalues

% usersvalues.dt=str2double(get(handles.inttime,'String'));
% Curdata.time=Curdata.frames.*usersvalues.dt;
[out] = fullplotanalysis(Curdata, usersvalues);
%creates a Matlab Figure 1 containing a plot of  
% a) the instantaneous speed in �m/s against the the absolute time since 
%    the beginning of the track in seconds  
% b) the Cross correlation maximum against the absolute time since the 
%    beginning of the track in seconds   
% c) the background corrected intensity in the green and in the red channel
%    against the the absolute time since the beginning of 
%    the track in seconds   
% d) The subpixel accurate x (blue) and y (red) coordinates of the cross 
%    correlation maximum against the absolute time since the beginning of 
%    the track in seconds  

%creates a Matlab Figure 2 containing a 3D plot of the virus trace including projections onto the three axes and 
%indication of the starting position by a green circle 





%% Exports movie of currently loaded track ("Export movie" button)

% --- Executes on button press in movie.
function movie_Callback(hObject, eventdata, handles)
% hObject    handle to movie (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%% Loads global variables

global Curdata
global Filenames
global usersvalues
global movies

%% Checks whether movies (and transformation structure) are loaded

% if the user chose to correct for XY shift
if usersvalues.XYcorrection == 1

    question = questdlg('Are the corresponding movies and the transformation structure loaded?', 'Movies and tranformation structure','Yes','No','Yes');

    switch question 
        case 'No'
            return
        case 'Yes'
    end

else
    
    question = questdlg('Are the corresponding movies loaded?', 'Movies','Yes','No','Yes');
    
    switch question 
        case 'No'
            return
        case 'Yes'
    end

end

%% Saves saving directory and root name for movies

[FileName,PathName] = uiputfile('*.*', 'Choose saving directory and root name for movie',...
    usersvalues.defaultPath);
 usersvalues.trackname = FileName;
 usersvalues.result_location = PathName;
 
%% Sets size of movie window to export

%give boundaries of the window around the track in pixels 
pixel_boundary = 10;

% implement that the boundary is adjusted if the given one exceeds the
% frame dimension CHANGE
 
%% Starting message 
 
disp ('Export of movie in progress ...'); %this statement is shown in the command window

%% tracked channel 

disp('... Tracked Channel ...');

% 3D
if usersvalues.new_threeD == 1 || usersvalues.old_threeD == 1

    %converts the tracked projected movie into RGB (only part (pixel area and frames) for which tracking was performed) as inverted (black on white, contrast adjusted)  RGB stack
    RGB_stack_tracked = RGB_converter_stack (movies.proj_movie_tracked, round(min(Curdata.xsub))-pixel_boundary, round(max(Curdata.xsub))+pixel_boundary, round(min(Curdata.ysub))-pixel_boundary, round(max(Curdata.ysub))+pixel_boundary, Curdata.frames(1), length(Curdata.frames));
    
    %saves the name of the movie file created 
    moviename_tracked = strcat(usersvalues.trackname(1:length(usersvalues.trackname)-4), '_tracked_projected_RGB_inverted'); 

end

% 2D
if usersvalues.twoD == 1
   
    %converts the tracked projected movie into RGB (only part (pixel area and frames) for which tracking was performed) as inverted (black on white, contrast adjusted)  RGB stack
    RGB_stack_tracked = RGB_converter_stack (movies.TwoD_movie_tracked, round(min(Curdata.xsub))-pixel_boundary, round(max(Curdata.xsub))+pixel_boundary, round(min(Curdata.ysub))-pixel_boundary, round(max(Curdata.ysub))+pixel_boundary, Curdata.frames(1), length(Curdata.frames));
    
    %saves the name of the movie file created 
    moviename_tracked = strcat(usersvalues.trackname(1:length(usersvalues.trackname)-4), '_tracked_RGB_inverted'); 
    
end

%creates a movie in which the track is encircled; last three numbers specify the color of the circle
CreateMovie(RGB_stack_tracked, Curdata.xsub, Curdata.ysub, round(min(Curdata.xsub))-pixel_boundary, round(min(Curdata.ysub))-pixel_boundary, usersvalues.result_location, moviename_tracked, 255, 0, 0); 

%% untracked channel 

disp('... Untracked Channel ...');

% if the user chose to correct for XY shift
if usersvalues.XYcorrection == 1

     %Calculates the coordinates of the track in the UNtracked channel out of the coordinates in the tracked channel
     [XsubNotTracked, YsubNotTracked] = CoordinateTransformer (Curdata.xsub, Curdata.ysub, Filenames.transf); 
 
end    


% 3D
if usersvalues.new_threeD == 1 || usersvalues.old_threeD == 1

    %converts the untracked projected movie into RGB (only part (pixel area and frames) for which tracking was performed) as inverted (black on white, contrast adjusted) RGB stack
    RGB_stack_untracked = RGB_converter_stack (movies.proj_movie_untracked, round(min(XsubNotTracked))-pixel_boundary, round(max(XsubNotTracked))+pixel_boundary, round(min(YsubNotTracked))-pixel_boundary, round(max(YsubNotTracked))+pixel_boundary, Curdata.frames(1), length(Curdata.frames)); 
    
end

% 2D
if usersvalues.twoD == 1
    
    %converts the untracked projected movie into RGB (only part (pixel area and frames) for which tracking was performed) as inverted (black on white, contrast adjusted) RGB stack
    RGB_stack_untracked = RGB_converter_stack (movies.TwoD_movie_untracked, round(min(Curdata.xsub))-pixel_boundary, round(max(Curdata.xsub))+pixel_boundary, round(min(Curdata.ysub))-pixel_boundary, round(max(Curdata.ysub))+pixel_boundary, Curdata.frames(1), length(Curdata.frames));   
    
end


% 3D
if usersvalues.new_threeD == 1 || usersvalues.old_threeD == 1

    %saves the name of the movie file created
    moviename_untracked = strcat(usersvalues.trackname(1:length(usersvalues.trackname)-4), '_untracked_projected_RGB_inverted'); %Saves the name of the RGB converted inverted avi-movie

end

% 2D
if usersvalues.twoD
  
    %saves the name of the movie file created
    moviename_untracked = strcat(usersvalues.trackname(1:length(usersvalues.trackname)-4), '_untracked_RGB_inverted'); %Saves the name of the RGB converted inverted avi-movie
    
end

% if the user chose to correct for XY shift use the transformed coordinates
if usersvalues.XYcorrection == 1

    %creates a movie in which the track is encircled; last three numbers specify the color of the circle 
    CreateMovie(RGB_stack_untracked, XsubNotTracked, YsubNotTracked, min(round(XsubNotTracked))-pixel_boundary, min(round(YsubNotTracked))-pixel_boundary, usersvalues.result_location, moviename_untracked, 255, 0, 0); 
            
else % if the user doesn't want to correct for XY shift 
    
    %creates a movie in which the track is encircled; last three numbers specify the color of the circle 
    CreateMovie(RGB_stack_untracked, Curdata.xsub, Curdata.ysub, round(min(Curdata.xsub))-pixel_boundary, round(min(Curdata.ysub))-pixel_boundary, usersvalues.result_location, moviename_untracked, 255, 0, 0); 
    
end

%% merged channel  
disp('... Merge ...');

if usersvalues.new_threeD == 1 || usersvalues.old_threeD == 1 

    if usersvalues.XYcorrection == 1

        %shifts the UNtracked movie to match with the tracked movie (only for the frames of the current track) (resulting movie is in 16-bit greyscale)
        proj_movie_untracked_shifted = correct_shift_stack (movies.proj_movie_untracked, Filenames.transf, Curdata.frames(1), length(Curdata.frames)); 

        %merges the tracked and the shifted UNtracked movie (input movies are in 16-bit grey scale, resulting movie is in RGB)
        merged_RGB_stack = Merge_RGB_Stacks (movies.proj_movie_tracked(:,:,(Curdata.frames(1):length(Curdata.frames))), proj_movie_untracked_shifted, round(min(Curdata.xsub))-pixel_boundary, round(max(Curdata.xsub))+pixel_boundary, round(min(Curdata.ysub))-pixel_boundary, round(max(Curdata.ysub))+pixel_boundary, 1, length(Curdata.frames));
        
        %saves the name of the movie file created
        moviename_merged_movie = strcat(usersvalues.trackname(1:length(usersvalues.trackname)-4), '_merge_XY_shift_corrected'); 

    else % this case (3D analysis without XY correction is currently not implemented)

        %merges the tracked and the UNtracked movie (only for the frames of the current track)(input movies are in 16-bit grey scale, resulting movie is in RGB)
        merged_RGB_stack = Merge_RGB_Stacks (movies.proj_movie_tracked, movies.proj_movie_untracked, round(min(Curdata.xsub))-pixel_boundary, round(max(Curdata.xsub))+pixel_boundary, round(min(Curdata.ysub))-pixel_boundary, round(max(Curdata.ysub))+pixel_boundary, Curdata.frames(1), length(Curdata.frames)); 
        
        %saves the name of the movie file created
        moviename_merged_movie = strcat(usersvalues.trackname(1:length(usersvalues.trackname)-4), '_merge'); 

    end

end

if usersvalues.twoD == 1

    %merges the tracked and the UNtracked 2D movie (only for the frames of the current track)(input movies are in 16-bit grey scale, resulting movie is in RGB)
    merged_RGB_stack = Merge_RGB_Stacks (movies.TwoD_movie_tracked, movies.TwoD_movie_untracked, round(min(Curdata.xsub))-pixel_boundary, round(max(Curdata.xsub))+pixel_boundary, round(min(Curdata.ysub))-pixel_boundary, round(max(Curdata.ysub))+pixel_boundary, Curdata.frames(1), length(Curdata.frames)); 
    
    %saves the name of the movie file created
    moviename_merged_movie = strcat(usersvalues.trackname(1:length(usersvalues.trackname)-4), '_merge'); 

end

%creates a movie in which the track is encircled; last three numbers specify the color of the circle
CreateMovie(merged_RGB_stack, round(Curdata.xsub), round(Curdata.ysub), round(min(Curdata.xsub))-pixel_boundary, round(min(Curdata.ysub))-pixel_boundary, usersvalues.result_location, moviename_merged_movie, 255, 255, 255);

%% completion message

disp('Movies saved');





% --- Executes on slider movement.
function Results_slider_Callback(hObject, eventdata, handles)
% hObject    handle to Results_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

%% Gets global variables 

global Curdata 
global usersvalues
global movies
global Filenames

%% Sets slider 

set(handles.Results_slider, 'Min', 1);
set(handles.Results_slider, 'Max', Curdata.frames(end));
set(handles.Results_slider, 'SliderStep', [(1/Curdata.frames(end)) (1/Curdata.frames(end))]);

current_frame = get(handles.Results_slider, 'Value');
current_frame = round(current_frame);
set(handles.Results_slider, 'Value', current_frame); 

% THIS IS NOT THE IDEAL LOCATION TO GENERATE THE CORRECTED MOVIE; SHOULD BE MOVED SOMEWHERE ELSE ON THE LONG TERM!   
% if shifted movie has not been created yet it is generated here 
if isfield (movies, 'proj_movie_untracked_shifted') == 0 
   %shifts the UNtracked movie to match with the tracked movie (only for the frames of the current track) (resulting movie is in 16-bit greyscale)
   movies.proj_movie_untracked_shifted = correct_shift_stack (movies.proj_movie_untracked, Filenames.transf, Curdata.frames(1), length(Curdata.frames));
end

%% Plots data including frame indicator

% XY plot
plot(handles.XY_axes,Curdata.xsub,Curdata.ysub);
hold(handles.XY_axes,'on');plot(handles.XY_axes,Curdata.xsub(current_frame),Curdata.ysub(current_frame), 'o', 'MarkerSize', 12, 'LineWidth', 2, 'MarkerEdgeColor','m'); legend('off'); hold(handles.XY_axes,'off');

% Labels the axes of the "XY Tracking with fit" plot
set(get(handles.XY_axes,'XLabel'),'String','relative X position (�m)'); 
set(get(handles.XY_axes,'YLabel'),'String','relative Y position (�m)');


% Z plot
plot(handles.Z_axes,Curdata.time,Curdata.zsub*usersvalues.pixZ);
hold(handles.Z_axes,'on');plot(handles.Z_axes,[Curdata.time(current_frame),Curdata.time(current_frame)],[0, ((max(Curdata.zsub*usersvalues.pixZ))+0.2)], 'm:', 'LineWidth', 2); hold(handles.Z_axes,'off');
%hold(handles.Z_axes,'on');plot(handles.Z_axes,[Curdata.time(current_frame),Curdata.time(current_frame)],[((min(Curdata.zsub*usersvalues.pixZ))-0.2), ((max(Curdata.zsub*usersvalues.pixZ))+0.2)], 'm:', 'LineWidth', 2); hold(handles.Z_axes,'off');

% Labels the axes of the "Z Tracking with fit" plot
set(get(handles.Z_axes,'XLabel'),'String','relative time (s)');
set(get(handles.Z_axes,'YLabel'),'String','relative Z position (�m)');


% Velocity plot
plot(handles.velocity_axes,Curdata.time,Curdata.speed);
hold(handles.velocity_axes,'on');plot(handles.velocity_axes,[Curdata.time(current_frame),Curdata.time(current_frame)],[((min(Curdata.speed))), ((max(Curdata.speed))+0.02)], 'm:', 'LineWidth', 2); legend('visible','off'); hold(handles.velocity_axes,'off');


% Labels the axes of the "Instantaneous velocity" plot
set(get(handles.velocity_axes,'XLabel'),'String','relative time (s)'); 
set(get(handles.velocity_axes,'YLabel'),'String', sprintf ('instantaneous velocity \n of tracked particles (�m/s)'));


% Cross-correlation maximum plot
plot(handles.CCmax_axes,Curdata.time,Curdata.maxcc,'.');ylim(handles.CCmax_axes,[0 (max(Curdata.maxcc)+0.05)]);
hold(handles.CCmax_axes,'on');plot(handles.CCmax_axes,[Curdata.time(current_frame),Curdata.time(current_frame)],[0, ((max(Curdata.maxcc))+0.02)], 'm:', 'LineWidth', 2); hold(handles.CCmax_axes,'off');
%hold(handles.CCmax_axes,'on');plot(handles.CCmax_axes,[Curdata.time(current_frame),Curdata.time(current_frame)],[((min(Curdata.maxcc))), ((max(Curdata.maxcc))+0.02)], 'm:', 'LineWidth', 2); hold(handles.CCmax_axes,'off');

% Labels the axes of the "Cross-correlation colocalization analysis maximum" plot 
set(get(handles.CCmax_axes,'XLabel'),'String','relative time (s)');
set(get(handles.CCmax_axes,'YLabel'),'String', sprintf ('max. value of ICC \n of tracked particle'));


% Cross-correlation maximum coordinates plot
plot(handles.CCmax_coordinates_axes,Curdata.time,Curdata.ccposx,'.c', Curdata.time,Curdata.ccposy,'.m', Curdata.time,Curdata.ccposz,'.k'); ylim(handles.CCmax_coordinates_axes,[-10 10]);
legend ({'X', 'Y', 'Z'}, 'FontSize', 6, 'Location', 'NorthWest');

% Gets the minimum and maximum values of the CC coordinates
min_ccposx = min(Curdata.ccposx);
min_ccposy = min(Curdata.ccposy);
min_ccposz = min(Curdata.ccposz);
min_coordinate = min([min_ccposx min_ccposy min_ccposz]);

max_ccposx = max(Curdata.ccposx);
max_ccposy = max(Curdata.ccposy);
max_ccposz = max(Curdata.ccposz);
max_coordinate  = max([max_ccposx max_ccposy max_ccposz]);

hold(handles.CCmax_coordinates_axes,'on');plot(handles.CCmax_coordinates_axes,[Curdata.time(current_frame),Curdata.time(current_frame)], [min_coordinate, max_coordinate], 'm:', 'LineWidth', 2); hold(handles.CCmax_coordinates_axes,'off');

% Labels the axes of the "Cross-correlation colocalization analysis maximum coordinates" plot
set(get(handles.CCmax_coordinates_axes,'XLabel'),'String','relative time (s)');
set(get(handles.CCmax_coordinates_axes,'YLabel'),'String', sprintf ('coordinates of max. of ICC \n of tracked particle (pixels)'));


% Intensity plot
plot(handles.intensity_axes,Curdata.time,Curdata.intGcorrected,'+g');
hold(handles.intensity_axes,'on');plot(handles.intensity_axes,Curdata.time,Curdata.intRcorrected,'.r'); hold(handles.intensity_axes,'off');

% Gets the minimum and maximum intensity values
min_intGcorrected = min(Curdata.intGcorrected);
min_intRcorrected = min(Curdata.intRcorrected);
min_intensity = min([min_intGcorrected min_intRcorrected]);

max_intGcorrected = max(Curdata.intGcorrected);
max_intRcorrected = max(Curdata.intRcorrected);
max_intensity = max([max_intGcorrected max_intRcorrected]);

hold(handles.intensity_axes,'on');plot(handles.intensity_axes,[Curdata.time(current_frame),Curdata.time(current_frame)],[((min_intensity)), ((max_intensity)+0.02)], 'm:', 'LineWidth', 2); hold(handles.intensity_axes,'off');

% Labels the axes of the "Intensity" plot 
set(get(handles.intensity_axes,'XLabel'),'String','relative time (s)');
set(get(handles.intensity_axes,'YLabel'),'String','background-corrected intensity');


% Particle tracked plot
particle_tracked_frame = Get_single_track_movie_frame (movies.proj_movie_tracked, round(Curdata.xsub(current_frame)), round(Curdata.ysub(current_frame)), current_frame, 10);
axes(handles.particle_tracked_axes); imagesc(particle_tracked_frame); colormap gray; set(gca, 'YTickLabel', [], 'XTickLabel', []);

% Particle untracked plot
%Calculates the coordinates of the track in the UNtracked channel out of the coordinates in the tracked channel
[XsubNotTracked, YsubNotTracked] = CoordinateTransformer (Curdata.xsub, Curdata.ysub, Filenames.transf); 
particle_untracked_frame = Get_single_track_movie_frame (movies.proj_movie_untracked, round(XsubNotTracked(current_frame)), round(YsubNotTracked(current_frame)), current_frame, 10);
axes(handles.particle_untracked_axes); imagesc(particle_untracked_frame); colormap gray; set(gca, 'YTickLabel', [], 'XTickLabel', []);

% Particle merged plot 
%particle_untracked_frame_unshifted = movies.proj_movie_untracked((round(YsubNotTracked)-10):(round(YsubNotTracked)+10), (round(XsubNotTracked)-10):(round(XsubNotTracked)+10));
particle_untracked_frame_shifted = movies.proj_movie_untracked_shifted((round(Curdata.ysub)-10):(round(Curdata.ysub)+10), (round(Curdata.xsub)-10):(round(Curdata.xsub)+10));
%particle_untracked_frame_shifted = correct_shift_frame (21,21, particle_untracked_frame_unshifted, Filenames.transf);
%particle_untracked_frame_shifted = correct_shift_frame (21,21, movies.proj_movie_untracked((round(XsubNotTracked)-10):(round(XsubNotTracked)+10),(round(YsubNotTracked)-10):(round(YsubNotTracked)+10)), Filenames.transf);
%particle_untracked_frame_shifted = correct_shift_frame (21,21, movies.proj_movie_untracked((round(Curdata.ysub)-10):(round(Curdata.ysub)+10),(round(Curdata.xsub)-10):(round(Curdata.xsub)+10)), Filenames.transf);
%particle_merged_frame = imfuse((imadjust(particle_tracked_frame)), imadjust(particle_untracked_frame_shifted));
particle_merged_frame = imfuse(imadjust(particle_tracked_frame), imadjust(particle_untracked_frame_shifted));
particle_merged_frame_double = im2double(particle_merged_frame);
particle_merged_frame_RGB (:,:,:) = particle_merged_frame_double;
axes(handles.particle_merged_axes); imshow(particle_merged_frame_RGB); set(gca, 'YTickLabel', [], 'XTickLabel', []);





%%



    








%
%%=========================================================================
%                      Additional GUI functions  
%%=========================================================================
%


% --- Outputs from this function are returned to the command line.
function varargout = TRAC_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes during object creation, after setting all properties.
function analysis_type_dropdown_CreateFcn(hObject, eventdata, handles)
% hObject    handle to analysis_type_dropdown (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function Zdist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Zdist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function Zslices_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Zslices (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function inttime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to inttime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function trackchannel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to trackchannel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function ana_type_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ana_type (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% --- Executes during object creation, after setting all properties.
function pixxy_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pixxy (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function listOpeningFiles_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listOpeningFiles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function Results_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Results_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

% --- Executes on button press in stack_frame_removal.
function stack_frame_removal_Callback(hObject, eventdata, handles)
% hObject    handle to stack_frame_removal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of stack_frame_removal


%% Things to put somewhere
%set(handles.currentpathname,'String',PathName);
%%
