function shifted_stack = correct_shift_stack (non_shifted_stack, Transfpath, frame_init, number_of_frames)

%Activate in case you want to save shifted movie (see also code below)
%function proj_movie_untracked_shifted = correct_shift_stack (Stackname, Transfpath, resultpath)
    
%'Calculation of the corrected movie ongoing'    

%[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the data you
%want to be shifted'); for future implementation of data loading

%info = imfinfo(Stackname);
H = length(non_shifted_stack(:,1,1));
%H = info(1).Height;
W = length(non_shifted_stack(1,:,1));
%W = info(1).Width;
L = number_of_frames;
%L=length(info); %number of frames of the movie 
shifted_stack = (zeros(H,W,L,'uint16')); %preallocates the stack

disp('Correction of untracked movie for shift ongoing');

p = ProgressBar(L);
for i=1:L
    frame = non_shifted_stack(:,:,frame_init+i-1);
    %frame = imread(Stackname, 'Index', i); %reads in one frame of the movie at a time
    corr_frame = correct_shift_frame (W, H, frame, Transfpath); %contains the corrected frame 
    shifted_stack(:,:,i) = corr_frame; %saves the corrected frames in an array
    
 p.progress;
end
 p.stop;

% Activate in case you want to save the corrected movie (see also beginning
% of function
% outputFileName = strcat(resultpath,'\Untracked_projected_shifted.tif'); %contains the name of the outputfile
% %outputFileName = strcat(Stackname(1:length(Stackname)-4),'_shifted.tif'); %contains the name of the outputfile
% 
% for K=1:length(stack(1, 1, :)) %iterates over the stack containing the corrected frames
%     imwrite(stack(:, :, K), outputFileName, 'tiff', 'WriteMode', 'append',  'Compression','none'); %writes all corrected frames into a single tiff file and saves it in the current folder 
% end
% 
% 'Corrected movie saved'
















%function correct_shift (Filename)

%[FileName,PathName,FilterIndex] = uigetfile('.tif','Select the data you want to be shifted');

%info = imfinfo(Filename);
%H = info.Width;
%W = info.Height;
%L=2; %number of frames of the movie 

%stack = uint16(zeros(H,W,L)); %preallocates the stack

%load('transf12.mat');

%for i=1:L
%stack (:,:,i) = imread(Filename); %I contains image to be shifted 
%single_frame (:,:) = stack(:,:,i);
%B2 = imtransform(single_frame,transf12,'XData',[1 info.Width], 'YData',[1 info.Height]);
%stack (:,:,i) = B2;
%end 

%load('transf12.mat');
%B2 = imtransform(stack,transf12,'XData',[1 info.Width], 'YData',[1 info.Height]);
%B2 = imtransform(stack,transf12);

%File=strcat(Filename(1:length(Filename)-4),'_shifted.tif');

%outputFileName = strcat(Filename(1:length(Filename)-4),'_shifted.tif');
%for K=1:length(stack(1, 1, :))
%imwrite(B2(:, :, K), outputFileName, 'tiff') %, 'WriteMode', 'append',  'Compression','none');
%end

%imwrite(B2,outputFileName,'tiff');