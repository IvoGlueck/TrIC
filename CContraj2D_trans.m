function [Max,Pos] = CContraj2D_trans(proj_movie_tracked,proj_movie_untracked,xpix,ypix,frame_init,frame_end,transf,XY_correction_status)

%Aurelie's comments:
%calculates the 2D cross-correlation between each frame of two movies
%restricted to the area around the tracked particle
%
% From TDtracking fit: Z and stack1
% Filename is the original movie (16bits) in the channel not used for
% TDtracking.
% xpix,ypix result of 2D tracking in pixels
%Aurelie Dupont

%Ivo's comments:
%Inputs:
%proj_movie_tracked: 3D array containing the tracked projected movie
%proj_movie_untracked: 3D array containing untracked projected movie
%xpix and ypix: coordinates with subpixel resolution
%frame_init: index of the frame of the projected movie for which tracking was started
%frame_end: index of the frame of the projected movie for which tracking
%was ended
%transf: structure containing info about the transformation matrix 

%Outputs:
% Max: array containing the value of the maximum of the cross-correlation between
% each frame of Filename1 and Filname2 (without taking 3D info into
% account)
% Pos: array containing the x and y coordinates of the maximum of the crosscorrelation for each frame  
%close all;



%info = imfinfo(Filename1);
%"info" is a structure with 37 fields each containing certain info about the 
%file containing the tracked projected movie

W = length(proj_movie_tracked(1,:,1));
%W = info.Width;
%saves the info in the field "Width" of the variable "info" in "W". Width of
%the image in pixels (for Aurelie's images with both channels on single camera =256)

H = length(proj_movie_tracked(:,1,1));
%H = info.Height;
%saves the info in the field "Height" of the variable "info" in "H". Height of
%the image in pixels (for Aurelie's images with both channels on single camera=512)

movie1 = uint16(zeros(H,W));
movie2 = uint16(zeros(H,W));
%creates a matrix of zeros which has the dimensions of the images taken
%"uint16" converts into unsigned integers of type "uint16" 

L = frame_end - frame_init +1;
%L=the number of frames of the projected movie for which tracking was
%performed; length of current track

dpix = 10; %half size of the area of interest, default 10

xpix=round(xpix);ypix=round(ypix); %rounds the subpixel x and y coordinates to the nearest integer

Pos=[]; %creates an empty array

% if analysis has been done incl. correction for XY shift 
if XY_correction_status == 1

    if length(transf.tdata) == 10
        %returns the length of the largest dimension of the array
        %"transf.tdata" CHECK AGAIN 
        D=3;
    elseif length(transf.tdata) == 6
        D=2;
    end

   [x2pix,y2pix] = Tran1to2(xpix,ypix,transf,D);
end
   
%Inputs:
%xpix and ypix: coordinates with subpixel resolution of track
%transf: structure containing info about the transformation matrix
%D=length of tranf.tdata CHECK AGAIN 

%Outputs:
%x2pix, y2pix: matrices containing coordinates of the track in the not tracked movie for every frame 
%(CHECK AGAIN)

disp('2D colocalization analysis ongoing');

%canceled= 0;
%"canceled is set to "1" if analysis has been canceled
%ColoZDwaitbar = waitbar(0, '1', 'Name', '2D cross-correlation colocalization analysis in progress ...', 'CreateCancelBtn', 'setappdata(gcbf,''Cancel'',1)'); 
p = ProgressBar(L);
for i= 1:L %"L" contains the number of tracked frames; for-loop runs through all tracked frames  
        
    if (xpix(i)-dpix<=0) | (ypix(i)-dpix<=0) | (xpix(i)+dpix>W) | (ypix(i)+dpix>H) %tracked channel
        Pos1=-20;Pos2=-20;Max(i)=0;
        %if the particle is closer to the edge of the image than half the
        %fitting window in x and y the above stated  values are returned
        %WHY WERE THESE VALUES CHOSEN? CHECK!!
    
    elseif XY_correction_status == 1 && ((x2pix(i)<=dpix) | (y2pix(i)<=dpix) | (x2pix(i)>W-dpix) | (y2pix(i)>H-dpix)) %untracked channel
            Pos1=-20;Pos2=-20;Max(i)=0;  
            %if the particle is closer to the edge of the image than half the
            %fitting window in x and y the above stated  values are returned
            %WHY WERE THESE VALUES CHOSEN?
    
    else
            
        movie1 = proj_movie_tracked ((ypix(i)-dpix):(ypix(i)+dpix), (xpix(i)-dpix):(xpix(i)+dpix), frame_init+i-1);
        %movie1 = proj_movie_tracked (:,:,frame_init+i-1); %reads in the frame of the tracked projected movie
        %movie1 = imread(Filename1,frame_init+i-1);
     
        if XY_correction_status == 1

           movie2 = proj_movie_untracked ((y2pix(i)-dpix):(y2pix(i)+dpix), (x2pix(i)-dpix):(x2pix(i)+dpix), frame_init+i-1);

        else        

            movie2 = proj_movie_untracked ((ypix(i)-dpix):(ypix(i)+dpix), (xpix(i)-dpix):(xpix(i)+dpix), frame_init+i-1); %reads in the frame of the untracked projected movie
            %movie2 = proj_movie_untracked (:,:,frame_init+i-1); %reads in the frame of the untracked projected movie
            %movie2 = imread(Filename2,frame_init+i-1);

        end

        %movie1 = zoom(movie1,xpix(i),ypix(i),dpix);     %THIS ZOOMING IS DONE WITHIN THE FUNCTION FOR THE NEW AND OLD CC3D ANALYSIS
        %Inputs:
        %movie1: for all frames of the tracked, projected movie the area of interest 
        %xpix: x coordinates with subpixel resolution
        %ypix: y coordinates with subpixel resolution
        %dpix: half size of the area of interest

        % if analysis has been done incl. correction for XY shift
        %if XY_correction_status == 1
           % the area of interest for the untracked movie is calculated around the shifted coordinates 
           %movie2 = zoom(movie2,x2pix(i),y2pix(i),dpix);   %THIS ZOOMING IS DONE WITHIN THE FUNCTION FOR THE NEW AND OLD CC3D ANALYSIS
        %else
           % the area of interest for the untracked movie is calculated around the same coordinates as in the tracked channel 
           %movie2 = zoom(movie2,xpix(i),ypix(i),dpix);
        %end

        %movie2: area of interest of the untracked projected movie for frame
        %(i)
        %x2pix: rounded x coordinates with subpixel resolution in the untracked movie
        %y2pix: rounded y coordinates with subpixel resolution in the untracked movie
        %dpix: half size of the area of interest

        CCC = CCi(movie1,movie2)./(length(movie1).^2);  %THE  FOLLOWING IS IDENTICAL TO OLD CC3D ANALYSIS
        %calculates the cross-correlation between the two corresponding frames
        %of the tracked and untracked projected movies 
        %CCC is a matrix with 2dpix+1-by-2dpix+1 dimensions devided by
        %(dpix+1)^2

        [Pos1,Pos2]=Gaussian2Di(CCC);
        %Pos1: x coordinate of maximum of Gaussian fit to cross correlation results
        %Pos2: y coordinate of maximum of Gaussian fit to cross correlation results

        Max(i)=max(max(CCC));
        %Max(i) contains the maximum value of the image cross-correlation for
        %each frame of the projected movies

        Pos1=Pos1-dpix-1;Pos2=Pos2-dpix-1;
        %scaling back from the the area of interest in x and y to the indices of the entire image
    
    end
    
    Pos=[Pos;Pos1,Pos2];
    %for each iteration Pos1 and Pos2 are saved in Pos below each other
    %the vector contains the x and y coordinates of the maximum of the fit
    %for each frame
    
%     waitbar(i/L, ColoZDwaitbar, [num2str((i/L)*100, 2) ' %'])
%     if getappdata(ColoZDwaitbar, 'Cancel') %if "Cancel" button has been pressed 
%        fprintf ('Analysis has been canceled during 2D cross-correlation colocalization analysis'); 
%        canceled = 1; %"canceled is set to "1" if analysis has been canceled 
%        delete(ColoZDwaitbar)
%        Max=100000; %if Max is 100000 this means that analysis has been canceled during Z tracking; needed to stop "TRAC" function
%        Pos=100000; %all variables returned need to exist; that's why a random value is assigned here -> never used somewhere else
%        return %function is left        
%     end
    p.progress;
    
end

p.stop;

%     if (canceled == 0) %variable showing whether analysis has been canceled  
%        fprintf ('2D cross-correlation colocalization analysis finished');
%        delete(ColoZDwaitbar)           
%     end

Max=Max'; %switches a row vetcor into a column vector 

%=================================================================
%=================================================================
function [X2,Y2] = Tran1to2(X1,Y1,transf,D)
%Inputs:
%X1 and Y1: coordinates with subpixel resolution of track
%transf: structure containing info about the transformation matrix
%D=length of tranf.tdata CHANGE AFTER UNDERSTANDING TRANSF

%Outputs:
%X2, Y2: matrices containing coordinates of the track in the not tracked movie for every frame 
%(CHECK AGAIN AFTER UNDERSTANDING TRANF)

%transform track to fit in channel 2
L=length(X1);
for i=1:L %number of iterations equals the number of frames of the projected movie
    if D==2
        A=[1,X1(i),Y1(i),X1(i).*Y1(i),X1(i).*X1(i),Y1(i).*Y1(i)];
        %A: vector with 6 entries; results of different operations on the
        %rounded subpixel x und y values
        %A(1): 1
        %A(2): rounded subpixel x value
        %A(3): rounded subpixel y value
        %A(4): product: rounded subpixel x value times rounded subpixel y value
        %A(5): rounded subpixel x value^2
        %A(6): rounded subpixel y value^2
       
    elseif D==3
        A=[1,X1(i),Y1(i),X1(i).*Y1(i),X1(i).^2,Y1(i).^2,Y1(i).*X1(i).^2,X1(i).*Y1(i).^2,X1(i).^3,Y1(i).^3];
        %A: vector with 10 entries; results of different operations on the
        %rounded subpixel x und y values
        %A(1): 1
        %A(2): rounded subpixel x value
        %A(3): rounded subpixel y value
        %A(4): product: rounded subpixel x value times rounded subpixel y value
        %A(5): rounded subpixel x value^2
        %A(6): rounded subpixel y value^2
        %A(7): rounded subpixel y value times rounded subpixel x value^2
        %A(8): rounded subpixel x value times rounded subpixel x value^2
        %A(9): rounded subpixel x value^3
        %A(10): rounded subpixel y value^3
    end
    u=A*transf.tdata; %A and tranf.tdata have the same dimensions and are multiplied
    X2(i)=u(1);Y2(i)=u(2); %these are the coordinates of the track in the not tracked movie (CHECK AGAIN AFTER UNDERSTANDING TRANF)
end
X2=X2';Y2=Y2'; % flips the vectors

%=================================================================
%=================================================================
function [CC] = CCi(frame1,frame2)

% cross-correlation of two images
%frame1 and frame2 are two images (16-bit gray scale values)

frame1=double(frame1);
frame2=double(frame2);

%scaling (WHY IS THIS DONE??)
frame1=frame1-mean(mean(frame1));
frame2=frame2-mean(mean(frame2));
%mean intensity is substracted from all pxiel values 

%Aurelie's comments:
% frame1=frame1./max(max(frame1));
% frame2=frame2./max(max(frame2));
STDF1=std(reshape(frame1,1,[]));
%reshape(frame1,1,[]) all entries of frame are written into one
%column, 21-by-21 array into 1-by-441 vector
%the standard deviation of all grey scale values is taken 
STDF2=std(reshape(frame2,1,[]));
frame1=frame1./STDF1; 
frame2=frame2./STDF2;
%the grey scale values from which the mean value over all pixels was
%substracted is devided by the standard deviation of all pixel values 

IM1=fft2(frame1);
IM2=fft2(frame2);
%calculates the fast fourier transform of the greyscale values 

 CC=abs(real(fftshift(ifft2(IM1.*conj(IM2))))); %DETAILS!!
%conj(X): returns the complex conjugate of the elements of Z
%ifft(X): calculates the inverse fourier transform of X
%fftshift: shifts the zero-frequency component to the center of the array
%real (X): returns the real part of the elements of the complex array X
%abs(X): returns the absolute value of the element in array X

        
%===============================================================
%===============================================================
%%===============================================================%%
function [CCfull] = windowCCi(frame1,frame2) %WHERE IS THIS FUNCTION CALLED??

% cross-correlation in a small window of size mxn
%

frame1=double(frame1);
frame2=double(frame2);

frame1=frame1-mean(mean(frame1));
frame2=frame2-mean(mean(frame2));
frame1=frame1./max(max(frame1));
frame2=frame2./max(max(frame2));

S=size(frame1);
m = 4;
n = 4;

mmax = S(1)./m;
nmax = S(2)./n;

CCfull=[];

for k = 1:mmax
    CCline=[];
    for j = 1:nmax
        IM1=fft2(frame1((k-1).*m+1:k.*m,(j-1).*n+1:j.*n));
        IM2=fft2(frame2((k-1).*m+1:k.*m,(j-1).*n+1:j.*n));
        
        CC=abs(real(fftshift(ifft2(IM1.*conj(IM2)))));
        
        CCline=[CCline CC];
    end
    CCfull = [CCfull;CCline];
end


%====================================================================
%====================================================================
function [ax,ay]=Gaussian2Di(frame)

%to fit a frame with a 2D gaussian
%returns [ax,ay] coordinates of the maximum of the fit 

movie=frame-mean(mean(frame));
%substractes the mean pixel intensity from the intensity of each pixel 
mm=max(max(movie(:,:))); % this line seems to be OPSOLETE
%returns the maximum value in movie (the maximum of all columns and then 
%the maximum of those entries the maximum value of movie)
[my mx]= find(movie(:,:)==mm,1); % this line seems to be OPSOLETE
%finds the indices/coordinates of the first maximum value in "movie" and
%stores it in the variables "mx" and "my", respectively

%[n,m]=size(movie(:,:));%assumes that I is a nxm matrix
% ws=10; %Half-window size
%  I=double(movie(my-ws:my+ws,mx-ws:mx+ws));
I=double(movie);
%converts the intensity values into double precision values
I=I-mean(mean(I));
%substracts the mean intensity from every intensity value in I
[n,m]=size(I);
%stores the dimensions of I in n and m
[X,Y]=meshgrid(1:n,1:m);%Aurelie's comment: your x-y coordinates
%creates two matrices:
%X: 2dpix+1 columns with each 2dpix+1 rows: the first column contains ones,
%the second twos and so on
%Y: 2dpix+1 columns with each 2dpix+1 rows: each column contains the
%numbers from 1 to 2dpix+1
x(:,1)=X(:); % x= first column %all entries of X below each other in the first column of x
x(:,2)=Y(:); % y= second column %all entries of Y below each other in the second column of x

f=I(:);          % your data f(x,y) (in column vector) %all values of I written below each other in the first and only column of f


mI=max(max(I(:,:)));
%stores the maximum entry of I in "mI"
[myf mxf]= find(I(:,:)==mI,1);
%finds the indices/coordinates of the first maximum value in "I" and
%stores it in the variables "mxf" and "myf", respectively

fun = @(c,x) c(1)+c(2)*exp(-((x(:,1)-c(3))/c(4)).^2-((x(:,2)-c(5))/c(6)).^2);
%creates an anonymous function "fun" with the input parameters "c" (a
%vector with 6 elements, meaning the function depends on 6 parameters c(1)-c(6)) and "x" 
%(which is the array created above containin the x-y coordinates of all pixels of your window)

%fun is the equation for a 2D gaussian function with the following inputs:
%c(1): offset of function/fit
%c(2): amplitude of function/fit
%c(3): x coordinate of the max of the function/fit 
%c(4): standard deviation in x spread of the function/fit
%c(5): y coordinate of the max of the function/fit 
%c(6): standard deviation in y spread of the function/fit


c0=[0 mI mxf 5 myf 5];%start-guess here
%vector with those entries as starting vector
%c0(1): the offset is initially assumed to be 0
%c0(2): the amplitude is initially assumed to equal the maximum value of the image
%c0(3): the x coordinate of the maximum is initially assumed to equal the x coordinate of
%       the maximum of the first maximum of the image
%c0(4): the standard deviation in x is initially assumed to be 5
%c0(5): the y coordinate of the maximum is initially assumed to equal the y coordinate of
%       the maximum of the first maximum of the image
%c0(6): the standard deviation in y is initially assumed to be 5

options=optimset('TolX',1e-3,'MaxIter',100,'MaxTime',.3,'Display','off');
%creates an optimization options structure "options" with 58 fields in which certain
%parameters for optimization can be set; general assignment: ('param1', value1, 'param2', value2, ...)
%Paramters set here:
%'TolX': Iterations end when last step is smaller than TolX
%'MaxIter': Maximum number of iterations allowed
%'MaxTime': most likely maximum time for optimization (this option is not
%given in the matlab documentation)
%'Display': 'off': doesn't show the results of the optimization 


%fit
[cc, a , b , c, d]=lsqcurvefit(fun,c0,x,f,[],[],options);
%the function "lsqcurvefit" does curve-fitting using the least-square
%method
%inputs:
%"fun" is the function you want to fit which is the function defined above
%(2D gaussian fit)
%"c0": starting value/coefficient for optimization 
%"x": matrix "x" defined above containing the coordinates of the image 
%"f": vector containing the measured data (pixel values); output data to be matched by the
%function
%"options": options structure "options" with 58 fields in which certain
%parameters for optimization can be set (see above) 

%outputs are:
%"cc": coefficients best fitting the data; see vector c above
%"c": value that describes the exit condition; 
%"d": structure that contains information about the optimization

d;
%Aurelie's comments:
% ax=cc(3)+my-(ws+2)
% ay=cc(5)+mx-(ws+2)
ax=cc(3);ay=cc(5);
%"cc(3): x coordinate of max of fit
%"cc(5)": y coordinate of max of fit

%Aurelie's comments:
% Ifit=fun(cc,x); %your fitted gaussian in vector
% Ifit=reshape(Ifit,[n m]);%gaussian reshaped as matrix
% cc=round(cc);


%to plot the fit in a window of 40x40 pixels

% figure
% imagesc([Ifit I])
% drawnow
% title(['Sigma: ' num2str(cc(4)) ' / ' num2str(cc(6))]);
% axis image
% axis off

%=============================================================
%=============================================================
     function [stackb]=zoom(stack,xpix,ypix,dpix) %THIS ZOOMING IS DONE WITHIN THE FUNCTION FORTHE NEW AND OLD CC3D ANALYSIS
              
         %to zoom around a particle, half-size of the box:dpix
         %stackb is an array with the dimensions 2dipx+1-by-2dipx+1 containing 
         %the gray scale values of the pixels within the region of interest/ 
         %the region of 2dipx around the pixel identified by the rounded subpixel x 
         %and y coordinates  of an image 
         S=size(stack);
         %returns the sizes of the image in each dimension
         %stack is a single image 
         xpix=round(xpix);
         ypix=round(ypix);
         %rounds the subpixel x and y coordinates to the next integer
         %xpix and ypix are the the subpixel coordinates of the particle
         %location in the frame "stack"
         if (xpix-dpix<=0) | (ypix-dpix<=0) | (xpix+dpix>S(2)) | (ypix+dpix>S(1))
             %if subpixel x and y coordinates are closer to the edge of the
             %movie than dipix stackb=0
             stackb=0;
             %for dpix=10 stackb is a 21-by-21 array 
         else
             stackb=stack(ypix-dpix:ypix+dpix,xpix-dpix:xpix+dpix);
             % the region of 2dipx around the rounded
             %subpixel coordinates is saved in stackb  
         end
        
         

