function [Xsub,Ysub] = XYtrackingfit(xpix,ypix,proj_movie_tracked,frame_init)
% Performs 2D tracking with sub-pixel resolution (2D gaussian fit)

% Inputs:
% xpix: Pixel accurate X coordinates of track
% ypix: Pixel accurate Y coordinates of track
% proj_movie_tracked: projected movie of the tracked channel as a 3D array; 16bits
% frame_init: first frame of the projected movie for which 2D manual tracking was performed; cannot be 0

% Outputs:
% Xsub,Ysub is the result of the fit: subpixel resolution; arrays
% containing value for each frame of the projected movie for which manual tracking
% was performed

% ER returns the positions where the fit did not succeed
%Aurelie Dupont
% close all;

dpix=4; % half size of the window (default 3 or 4)
%takes the pixel position identified by manual tracking and
%looks at a region of +/- 4 pixels in x and +/- 4 pixels in y => entire
%area is 9*9 pixels in size

%info = imfinfo(Filename);
%"info" is a structure with 37 fields each containing certain info about
%the file containing the tracked projected movie

W = length(proj_movie_tracked(1,:,1));
%W = info.Width;
%saves the info in the field "Width" of the variable "info" in "W". Width of
%the image in pixels; equals X (for Aurelie's measurements with single camera for both channels = 256) 

H = length(proj_movie_tracked(:,1,1));
%H = info.Height;
%saves the info in the field "Height" of the variable "info" in "H". Height of
%the image in pixels, equals Y (for Aurelie's measurements with single camera for both channels = 512)

L = length(xpix);
%saves the number of tracked x coordinates in "L" = the number of tracked
%frames 


%stack = uint16(zeros(2*dpix+1,2*dpix+1));
%%"uint16" converts into unsigned integers of type "uint16" 
%%"zeros(n,m)" returns an n by m matrix of zeros (as dpix=4 its a
%%9x9 matrix of zeros in this case); not sure why "uint16" conversion is necessary here 

xpix=round(xpix);ypix=round(ypix);
%rounds x and y values of manual tracking to the nearest integer; not sure
%why this is necessary as the results of the manual tracking are integers anyway

disp ('XY Tracking ongoing'); %this statement is shown in the command window 

PixRegx = double(zeros(1,2)); %creates a matrix with 1 row and 2 columns filling it with zeros
PixRegy = double(zeros(1,2));
Xsub = double(zeros(1,L)); %creates a matrix with 1 row and L columns filling it with zeros
Ysub = double(zeros(1,L));

p = ProgressBar(L);
    for i= 1:L  %"L" contains the number of tracked frames; for-loop runs through all tracked frames
    %parfor i= 1:L  %"L" contains the number of tracked frames; for-loop runs through all tracked frames  
      
    PixRegx=[xpix(i)-dpix,xpix(i)+dpix];
    %creates an array of type double with 2 elements: 
    %1. x coordinate of frame i - dpix, 
    %2. x coordinate of frame i + dpix
    %specifies a region around the pixel identified by manual tracking 
    PixRegy=[ypix(i)-dpix,ypix(i)+dpix];
    %does the same for y 
    
    stack = uint16(zeros(2*dpix+1,2*dpix+1));
    %"uint16" converts into unsigned integers of type "uint16" 
    %"zeros(n,m)" returns an n by m matrix of zeros (as dpix=4 its a
    %9x9 matrix of zeros in this case); not sure why "uint16" conversion is necessary here 

    
    if (xpix(i)<=dpix) | (ypix(i)<=dpix) | (xpix(i)>W-dpix) | (ypix(i)>H-dpix)
        %pipe "|" means "OR"
        %if the particle is closer to the edge of the image than half the
        %fitting window no fitting can be performed and the position found
        %through manual tracking is taken as the final x and y value
        Xsub(i)=xpix(i);
        Ysub(i)=ypix(i);
    else
        stack(:,:) = proj_movie_tracked (PixRegy(1):PixRegy(2), PixRegx(1):PixRegx(2), frame_init+i-1); 
        %stack(:,:) = imread(Filename,frame_init+i-1,'PixelRegion',{PixRegy,PixRegx});
        %"imread" reads in the color values of the pixels of the tracked projected movie
        %the TIFF-file loaded has a pixel depth of 16-bit and is in grey
        %scale. So the color of each pixel is assigned with a certain
        %number ranging from 0 (black) to 65 535 (white).
        
        %these values are written into the variable "stack" which is an
        %array of size 9x9
        %"Filename" contains the path to the tracked projected movie 
        %"frame_init+i-1" goes through all tracked frames 
      
        %"reads in only a region of each frame of the tracked projected movie; region is
        %specified by the boundaries set in "PixRegy" and "PixRegx" => 9x9 region around 
        %the pixel identified by manual tracking 
        
        %subpixel tracking by fitting with a 2D gaussian
        [ax,ay,flag]=Gaussian2D(stack,dpix+1,dpix+1);
        
        %Inputs: 
        %stack: array containing grey scale values of a 9x9 region around the pixel identified by 2D manual tracking
        %dpix+1: half the number of pixels considered in x direction around the pixel identified by 2D manual tracking + 1 = 5 (dpix+1)
        %dpix+1: half the number of pixels considered in y direction around the pixel identified by 2D manual tracking + 1 = 5 (dpix+1)

        %Outputs:
        %ax: x value of the maximum of the 2D gaussian fit
        %ay: y value of the maximum of the 2D gaussian fit
        %flag: value that describes exit condition of the fit

        
        
        if flag>0 %if fitting was successful 
            
            %if "flag" is <=0 this means that for some reason the fit is
            %not possible (see definition of lsqcurvefit exitflag)
            Xsub(i)=ax+xpix(i)-dpix-1;
            %"ax" contains the subpixel x coordinate within the 9x9 window 
            %this coordinate needs to be assigned to the corresponding
            %value in the entire image (coordinates in xpix)
            %xpix: relative X positions in pixels obtained by manual tracking; for Aurelie's movies with single camera for both channels: ranges from 0 to 256%relative X positions in pixels obtained by manual tracking; for Aurelie's movies with single camera for both channels: ranges from 0 to 256
            Ysub(i)=ay+ypix(i)-dpix-1;
            %"ay" contains the subpixel y coordinate within the 9x9 window 
            %this coordinate needs to be assigned to the corresponding
            %value in the entire image (coordinates in ypix)
            %ypix: relative Y positions in pixels obtained by manual tracking for Aurelie's movies with single camera for both channels: ranges from 0 to 512
        else
            %if fitting cannot be performed and the position found
            %through manual tracking is taken as the final x and y value
            Xsub(i)=xpix(i);
            Ysub(i)=ypix(i);
        end
        
    end      
       
p.progress;
    end   
p.stop;
   

Xsub=Xsub'; 
Ysub=Ysub';
%flips "Xsub"/"Ysub" from a matrix with the number of columns equal to 
%the number of tracked frames and each column containing a single entry into
%into a matrix with one column with one entry for each frame 

   ErrorDpix =4; %Correct points which are further than ErrorDpix
   %from the manual tracking
   dX=abs(Xsub-xpix);
   dY=abs(Ysub-ypix);
   %"Xsub" and "xpix" are both vectors, substraction: each vector element
   %is substracted from the corresponding in the other vector 
   ferx=find(dX>ErrorDpix);  
   fery=find(dY>ErrorDpix);
   %finds the indices for entries in vector "dX" which contain a value >ErrorDpix 
   Xsub(ferx)=xpix(ferx);
   Ysub(fery)=ypix(fery);
   %If "Xsub"/"Ysub" is further away than "ErrorDpix from the localization
   %identified by 2D manual tracking the localization identified by manual
   %tracking is saved in "Xsub"/"Ysub"

%Aurelie's comments: 
%        ERfit=find(ER<=0);
%        ERX=find(Xsub'==xpix);
%        ERY=find(Ysub'==ypix);

end
 
%=======================================================================
 function [ax,ay,flag]=Gaussian2D(frame,xmax,ymax)

%"frame" is the matrix containing the grayscale values of the 9x9 region
%around the pixel identified by manual tracking
%"ax" and "ay" are the subpixelaccurate coordinates of the maximum of the 2D gaussian fit of
%the data
%"flag": value that describes the exit condition of the 2D gaussian fit to
%the data 

%Aurelie's comments:
%to fit a frame with a 2D gaussian
%returns [ax,ay] coordinates of the maximum 
%close all;

I=double(frame);
%converts the grayscale values stored in "frame" into double precision values 
I=I-mean(mean(I));
%mean(I) returns the mean of the values along the first array dimension/the 
%mean of the values in the columns => array with 9 columns and 1 row  
%"mean(mean(I)))" returns the mean of mean values of I => single value
%remaining
%this single value is substracted from each value in the array I and the
%resulting values are stored in I

mm=max(max(I(:,:)));
%"max(I(:,:))" returns the maximum value out of each column in I
%"max(max(I(:,:)))" returns the maximum value of the entire matrix I

%Aurelies comment:
%[my mx]= find(I(:,:)==mm,1);
my=ymax;
%"my" contains half the size +1 of the pixel region around the pixel
%identified by manual tracking/dpix+1 
mx=xmax;
%"mx" contains half the size +1 of the pixel region around the pixel
%identified by manual tracking/dpix+1 
[n,m]=size(I);
%"m" and "n" contain the dimensions of the array "I"; in this case 9 and 9 
[X,Y]=meshgrid(1:n,1:m);%your x-y coordinates
%returns an array "X" with n (here 9) columns and n rows; the first column filled with ones, the second with twos and so on
%returns an array "Y" with m (here 9) rows and m columns; the first row filled with ones, the second with twos and so on
x(:,1)=X(:); % x= first column
%in the first column of x 9 ones, 9 twos and so on are written below each other (in total 81
%rows)
x(:,2)=Y(:); % y= second column
%in the second column of x the numbers from 1 to 9 are written below each
%other (in total 81 rows)

f=I(:);          % your data f(x,y) (in column vector)
%all columns of I are written below each other in the first column of f
%(one large vector with 81 entries) 

%Aurelie's comments:
%mI=max(max(I(:,:)));
%[myf mxf]= find(I(:,:)==mI,1);

fun = @(c,x) c(1)+c(2)*exp(-((x(:,1)-c(3))/c(4)).^2-((x(:,2)-c(5))/c(6)).^2);
%creates an anonymous function "fun" with the input parameters "c" (a
%vector with 6 elements, meaning the function depends on 6 parameters c(1)-c(6)) and "x" 
%(which is the array created above containin the x-y coordinates of all pixels of your window)
%fun is the equation for a 2D gaussian function 
c0=[-10 mm mx 3 my 3];%start-guess here

%vector with those entries as starting vector
%c(1): offset of function/fit WHY IS -10 SELECTED AS THE STARTING VALUE
%HERE?
%c(2): amplitude of function/fit  
%c(3): x coordinate of the max of the function/fit 
%c0(3): x coordinate of max in area of interst
%c(4): standard deviation in x/x spread of the function/fit
%c(5): y coordinate of the max of the function/fit 
%c0(5): y coordinate of max in area of interst
%c(6): standard deviation in y/y spread of the function/fit

%vector with those entries as starting vector WHY ARE THOSE STARTING VALUES SELECTED?
%c0(3): x coordinate of max of fit
%c0(5): y coordinate of max of fit
options=optimset('TolX',1e-3,'MaxIter',4000,'MaxTime',.5,'Display','off');
%creates an optimization options structure "options" with 58 fields in which certain
%parameters for optimization can be set; general assignment: ('param1', value1, 'param2', value2, ...)
%Paramters set here:
%'TolX': Iterations end when last step is smaller than TolX
%'MaxIter': Maximum number of iterations allowed
%'MaxTime': most likely maximum time for optimization (NOT SURE HERE)
%'Display': 'off': doesn't show the results of the optimization 


%Aurelie's comments:
%fit
% [x,resnorm,residual,exitflag]
[cc, a , b , c, d]=lsqcurvefit(fun,c0,x,f,[],[],options);
%the function "lsqcurvefit" does curve-fitting using the least-square
%method
%inputs:
%"fun" is the function you want to fit which is the function defined above
%"c0": starting value/coefficient for optimization 
%"x": matrix "x" defined above; input data for function
%"f": vector containing the measured data; output data to be matched by the
%function
%"options": options structure "options" with 58 fields in which certain
%parameters for optimization can be set (see above) 

%outputs are:
%"cc": coefficients best fitting the data; see vector c above
%"c": value that describes the exit condition; 
%"d": structure that contains information about the optimization

d;
cc;
flag=c;
%value that describes the exit condition is stored in "flag"

ax=cc(3);
%"cc(3): x coordinate of max of fit
ay=cc(5);
%"cc(5)": y coordinate of max of fit

%Ifit=fun(cc,x); %your fitted gaussian in vector
%Ifit=reshape(Ifit,[n m]);%gaussian reshaped as matrix
%cc=round(cc);

% figure
% imagesc([Ifit I])
% drawnow
% title(['Sigma: ' num2str(cc(4)) ' / ' num2str(cc(6))]);
% axis image
% axis off
% pause;
 end
%=============================================================
%=============================================================
     function [stackb]=zoom(stack,xpix,ypix,dpix) %DON'T KNOW WHEN THIS FUNCTION IS CALLED
         
         %to zoom around a particle, half-size of the box:dpix
         S=size(stack);
         xpix=round(xpix);
         ypix=round(ypix);
         if (xpix-dpix<=0) | (ypix-dpix<=0)| (xpix+dpix>S(2)) | (ypix+dpix>S(1))
             stackb=0;
         else
             stackb=stack(ypix-dpix:ypix+dpix,xpix-dpix:xpix+dpix);
         end
     end  
         
%   